jQuery(function($) {
	$('#date-picker').datepicker({
		autoclose : true,
	    format: 'yyyy-mm-dd',
		//dateFormat : 'yy-mm-dd'
	});
	$('#date-picker2').datepicker({
		autoclose : true,
	    format: 'yyyy-mm-dd',
		//dateFormat : 'yy-mm-dd'
	});
	$("#datepicker-mm-yy").datepicker( {
	    format: "mm-yy",
	    viewMode: "months", 
	    minViewMode: "months"
	});
	
});

$(function() {

    $("#example1").dataTable({
        "bPaginate": false,
        "bFilter": false,
    });
    $("#example2").dataTable({
    	"bPaginate": false,
        "bFilter": false,
    });
    $('#example3').dataTable({
        "bPaginate": false,
        "bLengthChange": false,
        "bFilter": false,
        "bSort": true,
        "bInfo": true,
        "bAutoWidth": false
    });
});

console.log(document.URL);


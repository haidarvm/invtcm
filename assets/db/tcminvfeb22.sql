--
-- PostgreSQL database dump
--

-- Dumped from database version 14.2
-- Dumped by pg_dump version 14.2

-- Started on 2022-02-11 14:46:08 WIB

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 846 (class 1247 OID 16531)
-- Name: qty_unit; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE public.qty_unit AS ENUM (
    'kg',
    'pcs'
);


ALTER TYPE public.qty_unit OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 209 (class 1259 OID 16535)
-- Name: tcminv_item; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tcminv_item (
    item_id integer NOT NULL,
    item_name character varying(255),
    item_code character varying(255),
    qty integer,
    qty_unit_id smallint,
    item_desc character varying(255),
    create_at timestamp with time zone DEFAULT now(),
    update_at timestamp with time zone DEFAULT statement_timestamp(),
    user_id integer,
    category_id integer
);


ALTER TABLE public.tcminv_item OWNER TO postgres;

--
-- TOC entry 210 (class 1259 OID 16542)
-- Name: tcm_item_item_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tcm_item_item_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tcm_item_item_id_seq OWNER TO postgres;

--
-- TOC entry 4473 (class 0 OID 0)
-- Dependencies: 210
-- Name: tcm_item_item_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tcm_item_item_id_seq OWNED BY public.tcminv_item.item_id;


--
-- TOC entry 211 (class 1259 OID 16543)
-- Name: tcminv_qty_unit; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tcminv_qty_unit (
    qty_unit_id integer NOT NULL,
    qty_unit character varying(255),
    description text
);


ALTER TABLE public.tcminv_qty_unit OWNER TO postgres;

--
-- TOC entry 212 (class 1259 OID 16548)
-- Name: tcm_qty_unit_qty_unit_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tcm_qty_unit_qty_unit_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tcm_qty_unit_qty_unit_id_seq OWNER TO postgres;

--
-- TOC entry 4474 (class 0 OID 0)
-- Dependencies: 212
-- Name: tcm_qty_unit_qty_unit_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tcm_qty_unit_qty_unit_id_seq OWNED BY public.tcminv_qty_unit.qty_unit_id;


--
-- TOC entry 213 (class 1259 OID 16549)
-- Name: tcminv_category; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tcminv_category (
    category_id integer NOT NULL,
    category_name character varying(255),
    create_at timestamp with time zone DEFAULT now(),
    category_desc text,
    qty_unit_id smallint
);


ALTER TABLE public.tcminv_category OWNER TO postgres;

--
-- TOC entry 214 (class 1259 OID 16555)
-- Name: tcminv_category_category_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tcminv_category_category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tcminv_category_category_id_seq OWNER TO postgres;

--
-- TOC entry 4475 (class 0 OID 0)
-- Dependencies: 214
-- Name: tcminv_category_category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tcminv_category_category_id_seq OWNED BY public.tcminv_category.category_id;


--
-- TOC entry 215 (class 1259 OID 16556)
-- Name: tcminv_contractor; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tcminv_contractor (
    contractor_id integer NOT NULL,
    contractor_name character varying(255),
    contractor_desc text,
    create_at timestamp with time zone DEFAULT now()
);


ALTER TABLE public.tcminv_contractor OWNER TO postgres;

--
-- TOC entry 216 (class 1259 OID 16562)
-- Name: tcminv_contractor_contractor_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tcminv_contractor_contractor_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tcminv_contractor_contractor_id_seq OWNER TO postgres;

--
-- TOC entry 4476 (class 0 OID 0)
-- Dependencies: 216
-- Name: tcminv_contractor_contractor_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tcminv_contractor_contractor_id_seq OWNED BY public.tcminv_contractor.contractor_id;


--
-- TOC entry 217 (class 1259 OID 16563)
-- Name: tcminv_user_level; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tcminv_user_level (
    level_id integer NOT NULL,
    level character varying(255)
);


ALTER TABLE public.tcminv_user_level OWNER TO postgres;

--
-- TOC entry 218 (class 1259 OID 16566)
-- Name: tcminv_level_level_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tcminv_level_level_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tcminv_level_level_id_seq OWNER TO postgres;

--
-- TOC entry 4477 (class 0 OID 0)
-- Dependencies: 218
-- Name: tcminv_level_level_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tcminv_level_level_id_seq OWNED BY public.tcminv_user_level.level_id;


--
-- TOC entry 219 (class 1259 OID 16567)
-- Name: tcminv_purchase_order; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tcminv_purchase_order (
    po_id integer NOT NULL,
    po_name character varying(255),
    po_number bigint,
    po_value character varying(255),
    supplier_id integer,
    create_at timestamp with time zone DEFAULT now()
);


ALTER TABLE public.tcminv_purchase_order OWNER TO postgres;

--
-- TOC entry 220 (class 1259 OID 16573)
-- Name: tcminv_purchase_order_po_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tcminv_purchase_order_po_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tcminv_purchase_order_po_id_seq OWNER TO postgres;

--
-- TOC entry 4478 (class 0 OID 0)
-- Dependencies: 220
-- Name: tcminv_purchase_order_po_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tcminv_purchase_order_po_id_seq OWNED BY public.tcminv_purchase_order.po_id;


--
-- TOC entry 221 (class 1259 OID 16574)
-- Name: tcminv_stock_in; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tcminv_stock_in (
    stock_in_id integer NOT NULL,
    qty_in real NOT NULL,
    item_id integer NOT NULL,
    date_in date NOT NULL,
    create_at timestamp with time zone DEFAULT now(),
    surat_izin_id smallint NOT NULL,
    user_id smallint,
    update_at timestamp with time zone DEFAULT statement_timestamp(),
    unit_cost numeric,
    supplier_id smallint
);


ALTER TABLE public.tcminv_stock_in OWNER TO postgres;

--
-- TOC entry 222 (class 1259 OID 16581)
-- Name: tcminv_stock_out; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tcminv_stock_out (
    stock_out_id integer NOT NULL,
    item_id integer NOT NULL,
    surat_izin_id smallint NOT NULL,
    date_out date NOT NULL,
    qty_out real NOT NULL,
    create_at timestamp with time zone DEFAULT now() NOT NULL,
    update_at timestamp with time zone DEFAULT statement_timestamp(),
    user_id smallint,
    contractor_id smallint
);


ALTER TABLE public.tcminv_stock_out OWNER TO postgres;

--
-- TOC entry 223 (class 1259 OID 16586)
-- Name: tcminv_stock_out_stock_out_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tcminv_stock_out_stock_out_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tcminv_stock_out_stock_out_id_seq OWNER TO postgres;

--
-- TOC entry 4479 (class 0 OID 0)
-- Dependencies: 223
-- Name: tcminv_stock_out_stock_out_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tcminv_stock_out_stock_out_id_seq OWNED BY public.tcminv_stock_out.stock_out_id;


--
-- TOC entry 224 (class 1259 OID 16587)
-- Name: tcminv_stock_return; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tcminv_stock_return (
    stock_out_id integer DEFAULT nextval('public.tcminv_stock_out_stock_out_id_seq'::regclass) NOT NULL,
    item_id integer NOT NULL,
    surat_izin_id smallint NOT NULL,
    date_return date NOT NULL,
    qty_return real NOT NULL,
    create_at timestamp(6) with time zone DEFAULT now() NOT NULL,
    update_at timestamp(6) with time zone DEFAULT statement_timestamp(),
    user_id smallint,
    contractor_id smallint
);


ALTER TABLE public.tcminv_stock_return OWNER TO postgres;

--
-- TOC entry 225 (class 1259 OID 16593)
-- Name: tcminv_stock_stock_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tcminv_stock_stock_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tcminv_stock_stock_id_seq OWNER TO postgres;

--
-- TOC entry 4480 (class 0 OID 0)
-- Dependencies: 225
-- Name: tcminv_stock_stock_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tcminv_stock_stock_id_seq OWNED BY public.tcminv_stock_in.stock_in_id;


--
-- TOC entry 226 (class 1259 OID 16594)
-- Name: tcminv_supplier; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tcminv_supplier (
    supplier_id integer NOT NULL,
    supplier_name character varying(255),
    supplier_details text,
    create_at timestamp with time zone DEFAULT now()
);


ALTER TABLE public.tcminv_supplier OWNER TO postgres;

--
-- TOC entry 227 (class 1259 OID 16600)
-- Name: tcminv_supplier_supplier_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tcminv_supplier_supplier_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tcminv_supplier_supplier_id_seq OWNER TO postgres;

--
-- TOC entry 4481 (class 0 OID 0)
-- Dependencies: 227
-- Name: tcminv_supplier_supplier_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tcminv_supplier_supplier_id_seq OWNED BY public.tcminv_supplier.supplier_id;


--
-- TOC entry 228 (class 1259 OID 16601)
-- Name: tcminv_surat_izin; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tcminv_surat_izin (
    surat_izin_id integer NOT NULL,
    surat_izin character varying(255),
    surat_izin_desc text,
    create_at timestamp with time zone DEFAULT now(),
    date_min date,
    date_max date,
    user_id smallint,
    update_at timestamp with time zone DEFAULT statement_timestamp()
);


ALTER TABLE public.tcminv_surat_izin OWNER TO postgres;

--
-- TOC entry 229 (class 1259 OID 16608)
-- Name: tcminv_surat_izin_surat_izin_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tcminv_surat_izin_surat_izin_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tcminv_surat_izin_surat_izin_id_seq OWNER TO postgres;

--
-- TOC entry 4482 (class 0 OID 0)
-- Dependencies: 229
-- Name: tcminv_surat_izin_surat_izin_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tcminv_surat_izin_surat_izin_id_seq OWNED BY public.tcminv_surat_izin.surat_izin_id;


--
-- TOC entry 230 (class 1259 OID 16609)
-- Name: tcminv_user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tcminv_user (
    user_id integer NOT NULL,
    username character varying(255),
    fullname character varying(500),
    email character varying(255),
    address character varying(255),
    phone character varying(255),
    phone2 character varying(255),
    contractor_id integer,
    create_at timestamp with time zone DEFAULT now(),
    update_at timestamp with time zone DEFAULT statement_timestamp(),
    level_id smallint,
    password text
);


ALTER TABLE public.tcminv_user OWNER TO postgres;

--
-- TOC entry 231 (class 1259 OID 16616)
-- Name: tcminv_user_log; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tcminv_user_log (
    log_id bigint NOT NULL,
    user_id integer,
    login timestamp with time zone,
    logout timestamp with time zone
);


ALTER TABLE public.tcminv_user_log OWNER TO postgres;

--
-- TOC entry 232 (class 1259 OID 16619)
-- Name: tcminv_user_log_log_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tcminv_user_log_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tcminv_user_log_log_id_seq OWNER TO postgres;

--
-- TOC entry 4483 (class 0 OID 0)
-- Dependencies: 232
-- Name: tcminv_user_log_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tcminv_user_log_log_id_seq OWNED BY public.tcminv_user_log.log_id;


--
-- TOC entry 233 (class 1259 OID 16620)
-- Name: tcminv_user_user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tcminv_user_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tcminv_user_user_id_seq OWNER TO postgres;

--
-- TOC entry 4484 (class 0 OID 0)
-- Dependencies: 233
-- Name: tcminv_user_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tcminv_user_user_id_seq OWNED BY public.tcminv_user.user_id;


--
-- TOC entry 4239 (class 2604 OID 16621)
-- Name: tcminv_category category_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tcminv_category ALTER COLUMN category_id SET DEFAULT nextval('public.tcminv_category_category_id_seq'::regclass);


--
-- TOC entry 4241 (class 2604 OID 16622)
-- Name: tcminv_contractor contractor_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tcminv_contractor ALTER COLUMN contractor_id SET DEFAULT nextval('public.tcminv_contractor_contractor_id_seq'::regclass);


--
-- TOC entry 4236 (class 2604 OID 16623)
-- Name: tcminv_item item_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tcminv_item ALTER COLUMN item_id SET DEFAULT nextval('public.tcm_item_item_id_seq'::regclass);


--
-- TOC entry 4244 (class 2604 OID 16624)
-- Name: tcminv_purchase_order po_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tcminv_purchase_order ALTER COLUMN po_id SET DEFAULT nextval('public.tcminv_purchase_order_po_id_seq'::regclass);


--
-- TOC entry 4237 (class 2604 OID 16625)
-- Name: tcminv_qty_unit qty_unit_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tcminv_qty_unit ALTER COLUMN qty_unit_id SET DEFAULT nextval('public.tcm_qty_unit_qty_unit_id_seq'::regclass);


--
-- TOC entry 4247 (class 2604 OID 16626)
-- Name: tcminv_stock_in stock_in_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tcminv_stock_in ALTER COLUMN stock_in_id SET DEFAULT nextval('public.tcminv_stock_stock_id_seq'::regclass);


--
-- TOC entry 4250 (class 2604 OID 16627)
-- Name: tcminv_stock_out stock_out_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tcminv_stock_out ALTER COLUMN stock_out_id SET DEFAULT nextval('public.tcminv_stock_out_stock_out_id_seq'::regclass);


--
-- TOC entry 4255 (class 2604 OID 16628)
-- Name: tcminv_supplier supplier_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tcminv_supplier ALTER COLUMN supplier_id SET DEFAULT nextval('public.tcminv_supplier_supplier_id_seq'::regclass);


--
-- TOC entry 4258 (class 2604 OID 16629)
-- Name: tcminv_surat_izin surat_izin_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tcminv_surat_izin ALTER COLUMN surat_izin_id SET DEFAULT nextval('public.tcminv_surat_izin_surat_izin_id_seq'::regclass);


--
-- TOC entry 4261 (class 2604 OID 16630)
-- Name: tcminv_user user_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tcminv_user ALTER COLUMN user_id SET DEFAULT nextval('public.tcminv_user_user_id_seq'::regclass);


--
-- TOC entry 4242 (class 2604 OID 16631)
-- Name: tcminv_user_level level_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tcminv_user_level ALTER COLUMN level_id SET DEFAULT nextval('public.tcminv_level_level_id_seq'::regclass);


--
-- TOC entry 4262 (class 2604 OID 16632)
-- Name: tcminv_user_log log_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tcminv_user_log ALTER COLUMN log_id SET DEFAULT nextval('public.tcminv_user_log_log_id_seq'::regclass);


--
-- TOC entry 4447 (class 0 OID 16549)
-- Dependencies: 213
-- Data for Name: tcminv_category; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tcminv_category (category_id, category_name, create_at, category_desc, qty_unit_id) FROM stdin;
6	dinamit	2014-06-16 15:31:11.097022+07	bahan peledak	\N
5	detonator	2014-06-16 15:30:43.741295+07	alat pemicu	\N
9	pc	2014-06-19 20:09:03.736856+07	pc component	\N
\.


--
-- TOC entry 4449 (class 0 OID 16556)
-- Dependencies: 215
-- Data for Name: tcminv_contractor; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tcminv_contractor (contractor_id, contractor_name, contractor_desc, create_at) FROM stdin;
1	PAMA	PAMA contractor	2014-06-16 19:29:37.53725+07
2	BAS	BAS	2014-06-16 19:29:37.53725+07
\.


--
-- TOC entry 4443 (class 0 OID 16535)
-- Dependencies: 209
-- Data for Name: tcminv_item; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tcminv_item (item_id, item_name, item_code, qty, qty_unit_id, item_desc, create_at, update_at, user_id, category_id) FROM stdin;
8	Connectadet 12 M 25 MS	S5010062	26	2	masuk 72	2014-06-04 14:34:15.378958+07	\N	\N	6
7	Anzomex	S5010013	99	1	72 masuks	2014-06-04 14:34:00.634844+07	\N	\N	6
18	power supply	123123	\N	2	psu 350W	2014-06-19 20:14:12.500643+07	2014-06-19 20:14:12.500643+07	\N	9
19	intel core i5-4300U	srz13	\N	2	intel processor	2014-06-19 20:15:41.09404+07	2014-06-19 20:15:41.09404+07	\N	9
3	POWERGEL MAGNUM 50mm X 400mm	S5060011	34	1	\N	2014-06-04 14:30:46.745681+07	\N	\N	6
1	AMMONIUM NITRATE	S5010031	55	1	\N	2014-06-04 14:29:34.432644+07	\N	\N	6
5	EXEL CONNECTADETE 6 MD 12	S5010016	54	2	\N	2014-06-04 14:32:39.381951+07	\N	\N	5
9	DYNADET C2 INSTANT 4M	S5010063	5	1	\N	2014-06-04 14:34:49.276431+07	\N	\N	5
4	SUPER POWER 0.5 Kg	S5060019	23	1	\N	2014-06-04 14:31:14.382921+07	\N	\N	6
2	EMULSIFIER CLARIANT	S5020015	2	1	\N	2014-06-04 14:30:31.421065+07	\N	\N	5
10	Electric Detonator	S5010063	21	2	\N	2014-06-04 14:35:30.590601+07	\N	\N	6
6	RAYDET DTH 9 M 500 MS	S5010041	51	2	\N	2014-06-04 14:33:19.426326+07	\N	\N	5
\.


--
-- TOC entry 4453 (class 0 OID 16567)
-- Dependencies: 219
-- Data for Name: tcminv_purchase_order; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tcminv_purchase_order (po_id, po_name, po_number, po_value, supplier_id, create_at) FROM stdin;
\.


--
-- TOC entry 4445 (class 0 OID 16543)
-- Dependencies: 211
-- Data for Name: tcminv_qty_unit; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tcminv_qty_unit (qty_unit_id, qty_unit, description) FROM stdin;
2	pcs	piece
1	kg	kilogram
\.


--
-- TOC entry 4455 (class 0 OID 16574)
-- Dependencies: 221
-- Data for Name: tcminv_stock_in; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tcminv_stock_in (stock_in_id, qty_in, item_id, date_in, create_at, surat_izin_id, user_id, update_at, unit_cost, supplier_id) FROM stdin;
67	2	1	2022-02-11	2014-06-19 18:58:37.494036+07	2	\N	2014-06-19 18:58:37.494036+07	\N	\N
76	2	1	2022-02-11	2014-06-19 19:14:00.820246+07	2	\N	2014-06-19 19:14:00.820246+07	\N	\N
77	1	2	2022-02-11	2014-06-19 19:14:14.103169+07	2	\N	2014-06-19 19:14:14.103169+07	\N	\N
78	1	3	2022-02-11	2014-06-19 19:14:30.630237+07	2	\N	2014-06-19 19:14:30.630237+07	\N	\N
79	0	18	2022-02-11	2014-06-19 20:14:12.508047+07	1	\N	2014-06-19 20:14:12.508047+07	\N	\N
80	0	18	2022-02-11	2014-06-19 20:14:12.539589+07	2	\N	2014-06-19 20:14:12.539589+07	\N	\N
81	0	19	2022-02-11	2014-06-19 20:15:41.205538+07	1	\N	2014-06-19 20:15:41.205538+07	\N	\N
82	0	19	2022-02-11	2014-06-19 20:15:41.371069+07	2	\N	2014-06-19 20:15:41.371069+07	\N	\N
83	1	18	2022-02-11	2014-06-19 20:19:29.106921+07	2	\N	2014-06-19 20:19:29.106921+07	\N	\N
84	1	19	2022-02-11	2014-06-19 20:19:29.218848+07	2	\N	2014-06-19 20:19:29.218848+07	\N	\N
85	2	18	2022-02-11	2014-06-19 21:00:29.216387+07	2	\N	2014-06-19 21:00:29.216387+07	\N	\N
86	3	19	2022-02-11	2014-06-19 21:00:29.341415+07	2	\N	2014-06-19 21:00:29.341415+07	\N	\N
37	656	1	2022-02-11	\N	2	\N	2014-06-18 22:30:19.678025+07	\N	\N
38	66	3	2022-02-11	\N	2	\N	2014-06-18 22:30:19.689133+07	\N	\N
39	35	5	2022-02-11	\N	2	\N	2014-06-18 22:30:19.700203+07	\N	\N
40	531	6	2022-02-11	\N	2	\N	2014-06-18 22:30:19.711302+07	\N	\N
41	343	9	2022-02-11	\N	2	\N	2014-06-18 22:30:19.722554+07	\N	\N
42	99	10	2022-02-11	\N	2	\N	2014-06-18 22:30:19.733497+07	\N	\N
43	123	2	2022-02-11	\N	2	\N	2014-06-18 22:30:19.744619+07	\N	\N
44	545	4	2022-02-11	\N	2	\N	2014-06-18 22:30:19.755689+07	\N	\N
45	91	8	2022-02-11	\N	2	\N	2014-06-18 22:30:19.767095+07	\N	\N
46	10	7	2022-02-11	\N	2	\N	2014-06-18 22:30:19.777926+07	\N	\N
27	1	1	2022-02-11	\N	1	\N	2014-06-18 22:30:19.563379+07	\N	\N
28	3	3	2022-02-11	\N	1	\N	2014-06-18 22:30:19.578398+07	\N	\N
47	1	5	2022-02-11	2014-06-19 11:44:18.077755+07	2	\N	2014-06-19 11:44:18.077755+07	\N	\N
48	4	8	2022-02-11	2014-06-19 11:44:18.108175+07	2	\N	2014-06-19 11:44:18.108175+07	\N	\N
49	11	6	2022-02-11	2014-06-19 11:44:18.118596+07	2	\N	2014-06-19 11:44:18.118596+07	\N	\N
50	12	3	2022-02-11	2014-06-19 11:44:18.130056+07	2	\N	2014-06-19 11:44:18.130056+07	\N	\N
51	13	4	2022-02-11	2014-06-19 11:44:18.141345+07	2	\N	2014-06-19 11:44:18.141345+07	\N	\N
52	14	7	2022-02-11	2014-06-19 11:44:18.152569+07	2	\N	2014-06-19 11:44:18.152569+07	\N	\N
53	15	10	2022-02-11	2014-06-19 11:44:18.163905+07	2	\N	2014-06-19 11:44:18.163905+07	\N	\N
54	16	1	2022-02-11	2014-06-19 11:44:18.175009+07	2	\N	2014-06-19 11:44:18.175009+07	\N	\N
55	17	9	2022-02-11	2014-06-19 11:44:18.185954+07	2	\N	2014-06-19 11:44:18.185954+07	\N	\N
56	18	2	2022-02-11	2014-06-19 11:44:18.197102+07	2	\N	2014-06-19 11:44:18.197102+07	\N	\N
57	1	5	2022-02-11	2014-06-19 11:47:32.190507+07	2	\N	2014-06-19 11:47:32.190507+07	\N	\N
58	1	8	2022-02-11	2014-06-19 11:47:32.303124+07	2	\N	2014-06-19 11:47:32.303124+07	\N	\N
59	1	5	2022-02-11	2014-06-19 11:54:40.321602+07	2	\N	2014-06-19 11:54:40.321602+07	\N	\N
60	1	8	2022-02-11	2014-06-19 11:54:40.425424+07	2	\N	2014-06-19 11:54:40.425424+07	\N	\N
61	1	5	2022-02-11	2014-06-19 11:55:53.228695+07	2	\N	2014-06-19 11:55:53.228695+07	\N	\N
62	1	5	2022-02-11	2014-06-19 11:56:32.607434+07	2	\N	2014-06-19 11:56:32.607434+07	\N	\N
63	3	8	2022-02-11	2014-06-19 12:11:03.436998+07	2	\N	2014-06-19 12:11:03.436998+07	\N	\N
64	1	5	2022-02-11	2014-06-19 12:11:44.127195+07	2	\N	2014-06-19 12:11:44.127195+07	\N	\N
65	2	5	2022-02-11	2014-06-19 18:13:31.420569+07	1	\N	2014-06-19 18:13:31.420569+07	\N	\N
66	2	5	2022-02-11	2014-06-19 18:13:43.313181+07	2	\N	2014-06-19 18:13:43.313181+07	\N	\N
68	2	1	2022-02-11	2014-06-19 18:58:44.684117+07	2	\N	2014-06-19 18:58:44.684117+07	\N	\N
69	4	1	2022-02-11	2014-06-19 18:58:49.252565+07	2	\N	2014-06-19 18:58:49.252565+07	\N	\N
70	1	2	2022-02-11	2014-06-19 19:03:47.082762+07	2	\N	2014-06-19 19:03:47.082762+07	\N	\N
71	2	3	2022-02-11	2014-06-19 19:03:47.336177+07	2	\N	2014-06-19 19:03:47.336177+07	\N	\N
72	2	4	2022-02-11	2014-06-19 19:03:47.558133+07	2	\N	2014-06-19 19:03:47.558133+07	\N	\N
73	2	5	2022-02-11	2014-06-19 19:03:47.602553+07	2	\N	2014-06-19 19:03:47.602553+07	\N	\N
74	1	7	2022-02-11	2014-06-19 19:03:47.624332+07	2	\N	2014-06-19 19:03:47.624332+07	\N	\N
75	1	10	2022-02-11	2014-06-19 19:03:47.646673+07	2	\N	2014-06-19 19:03:47.646673+07	\N	\N
87	5	18	2022-02-11	2014-06-23 22:07:33.806178+07	2	\N	2014-06-23 22:07:33.806178+07	\N	\N
88	5	19	2022-02-11	2014-06-23 22:07:34.116214+07	2	\N	2014-06-23 22:07:34.116214+07	\N	\N
\.


--
-- TOC entry 4456 (class 0 OID 16581)
-- Dependencies: 222
-- Data for Name: tcminv_stock_out; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tcminv_stock_out (stock_out_id, item_id, surat_izin_id, date_out, qty_out, create_at, update_at, user_id, contractor_id) FROM stdin;
40	1	2	2022-02-11	2	2014-06-19 18:14:25.699526+07	2014-06-19 18:14:25.699526+07	\N	\N
41	2	2	2022-02-11	2	2014-06-19 19:22:13.341279+07	2014-06-19 19:22:13.341279+07	\N	\N
42	1	2	2022-02-11	1	2014-06-19 19:42:15.245577+07	2014-06-19 19:42:15.245577+07	\N	\N
43	18	2	2022-02-11	1	2014-06-19 20:19:46.217117+07	2014-06-19 20:19:46.217117+07	\N	\N
44	19	2	2022-02-11	1	2014-06-19 20:19:46.328794+07	2014-06-19 20:19:46.328794+07	\N	\N
45	1	2	2022-02-11	1	2014-06-19 20:41:09.442935+07	2014-06-19 20:41:09.442935+07	\N	\N
46	1	2	2022-02-11	1	2014-06-19 20:41:18.771881+07	2014-06-19 20:41:18.771881+07	\N	\N
47	18	2	2022-02-11	1	2014-06-19 21:14:05.69649+07	2014-06-19 21:14:05.69649+07	\N	\N
48	19	2	2022-02-11	1	2014-06-19 21:14:05.809079+07	2014-06-19 21:14:05.809079+07	\N	\N
25	5	2	2022-02-11	1	2014-06-19 14:06:57.691315+07	2014-06-19 14:06:57.691315+07	\N	\N
26	8	2	2022-02-11	2	2014-06-19 14:06:57.808012+07	2014-06-19 14:06:57.808012+07	\N	\N
27	6	2	2022-02-11	3	2014-06-19 14:06:57.896842+07	2014-06-19 14:06:57.896842+07	\N	\N
28	3	2	2022-02-11	4	2014-06-19 14:06:58.029617+07	2014-06-19 14:06:58.029617+07	\N	\N
29	4	2	2022-02-11	5	2014-06-19 14:06:58.040563+07	2014-06-19 14:06:58.040563+07	\N	\N
30	7	2	2022-02-11	6	2014-06-19 14:06:58.051898+07	2014-06-19 14:06:58.051898+07	\N	\N
31	10	2	2022-02-11	7	2014-06-19 14:06:58.062024+07	2014-06-19 14:06:58.062024+07	\N	\N
32	1	2	2022-02-11	8	2014-06-19 14:06:58.073724+07	2014-06-19 14:06:58.073724+07	\N	\N
33	9	2	2022-02-11	9	2014-06-19 14:06:58.084799+07	2014-06-19 14:06:58.084799+07	\N	\N
34	2	2	2022-02-11	10	2014-06-19 14:06:58.096006+07	2014-06-19 14:06:58.096006+07	\N	\N
35	1	2	2022-02-11	1	2014-06-19 18:06:31.310221+07	2014-06-19 18:06:31.310221+07	\N	\N
36	1	2	2022-02-11	1	2014-06-19 18:06:42.408152+07	2014-06-19 18:06:42.408152+07	\N	\N
37	2	2	2022-02-11	11	2014-06-19 18:07:20.826429+07	2014-06-19 18:07:20.826429+07	\N	\N
38	3	2	2022-02-11	1	2014-06-19 18:07:25.709364+07	2014-06-19 18:07:25.709364+07	\N	\N
39	6	2	2022-02-11	9	2014-06-19 18:08:19.834543+07	2014-06-19 18:08:19.834543+07	\N	\N
49	18	2	2022-02-11	1	2014-06-23 22:05:47.687337+07	2014-06-23 22:05:47.687337+07	\N	\N
50	19	2	2022-02-11	2	2014-06-23 22:05:47.847529+07	2014-06-23 22:05:47.847529+07	\N	\N
51	18	2	2022-02-11	4	2014-06-23 22:07:48.858738+07	2014-06-23 22:07:48.858738+07	\N	\N
52	19	2	2022-02-11	4	2014-06-23 22:07:48.996219+07	2014-06-23 22:07:48.996219+07	\N	\N
53	1	2	2022-02-11	2	2014-06-24 12:44:27.492383+07	2014-06-24 12:44:27.492383+07	\N	\N
54	18	2	2022-02-11	1	2014-06-24 12:44:27.861439+07	2014-06-24 12:44:27.861439+07	\N	\N
55	19	2	2022-02-11	1	2014-06-24 12:44:27.869749+07	2014-06-24 12:44:27.869749+07	\N	\N
56	2	2	2022-02-11	1	2014-06-24 12:45:17.928812+07	2014-06-24 12:45:17.928812+07	\N	1
57	4	2	2022-02-11	5	2014-06-24 12:45:18.041516+07	2014-06-24 12:45:18.041516+07	\N	1
61	1	2	2022-02-11	2	2014-07-11 14:34:03.758432+07	2014-07-11 14:34:03.758432+07	\N	1
62	2	2	2022-02-11	10	2014-07-11 14:34:04.3577+07	2014-07-11 14:34:04.3577+07	\N	1
63	3	2	2022-02-11	5	2014-07-11 14:34:04.371824+07	2014-07-11 14:34:04.371824+07	\N	1
64	4	2	2022-02-11	6	2014-07-11 14:34:04.382956+07	2014-07-11 14:34:04.382956+07	\N	1
65	5	2	2022-02-11	12	2014-07-11 14:34:04.39411+07	2014-07-11 14:34:04.39411+07	\N	1
77	1	1	2022-02-11	1	2016-07-23 14:09:35.447697+07	2016-07-23 14:09:35.447697+07	\N	1
78	3	1	2022-02-11	2	2016-07-23 14:09:35.779657+07	2016-07-23 14:09:35.779657+07	\N	1
79	5	1	2022-02-11	3	2016-07-23 14:09:35.846024+07	2016-07-23 14:09:35.846024+07	\N	1
80	18	1	2022-02-11	4	2016-07-23 14:09:35.912969+07	2016-07-23 14:09:35.912969+07	\N	1
81	19	1	2022-02-11	5	2016-07-23 14:09:35.968072+07	2016-07-23 14:09:35.968072+07	\N	1
\.


--
-- TOC entry 4458 (class 0 OID 16587)
-- Dependencies: 224
-- Data for Name: tcminv_stock_return; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tcminv_stock_return (stock_out_id, item_id, surat_izin_id, date_return, qty_return, create_at, update_at, user_id, contractor_id) FROM stdin;
40	1	2	2014-06-19	2	2014-06-19 18:14:25.699526+07	2014-06-19 18:14:25.699526+07	\N	\N
41	2	2	2014-06-19	2	2014-06-19 19:22:13.341279+07	2014-06-19 19:22:13.341279+07	\N	\N
42	1	2	2014-06-19	1	2014-06-19 19:42:15.245577+07	2014-06-19 19:42:15.245577+07	\N	\N
43	18	2	2014-06-19	1	2014-06-19 20:19:46.217117+07	2014-06-19 20:19:46.217117+07	\N	\N
44	19	2	2014-06-19	1	2014-06-19 20:19:46.328794+07	2014-06-19 20:19:46.328794+07	\N	\N
45	1	2	2014-06-19	1	2014-06-19 20:41:09.442935+07	2014-06-19 20:41:09.442935+07	\N	\N
46	1	2	2014-06-19	1	2014-06-19 20:41:18.771881+07	2014-06-19 20:41:18.771881+07	\N	\N
47	18	2	2014-06-20	1	2014-06-19 21:14:05.69649+07	2014-06-19 21:14:05.69649+07	\N	\N
48	19	2	2014-06-20	1	2014-06-19 21:14:05.809079+07	2014-06-19 21:14:05.809079+07	\N	\N
25	5	2	2014-06-17	1	2014-06-19 14:06:57.691315+07	2014-06-19 14:06:57.691315+07	\N	\N
26	8	2	2014-06-17	2	2014-06-19 14:06:57.808012+07	2014-06-19 14:06:57.808012+07	\N	\N
27	6	2	2014-06-17	3	2014-06-19 14:06:57.896842+07	2014-06-19 14:06:57.896842+07	\N	\N
28	3	2	2014-06-17	4	2014-06-19 14:06:58.029617+07	2014-06-19 14:06:58.029617+07	\N	\N
29	4	2	2014-06-17	5	2014-06-19 14:06:58.040563+07	2014-06-19 14:06:58.040563+07	\N	\N
30	7	2	2014-06-17	6	2014-06-19 14:06:58.051898+07	2014-06-19 14:06:58.051898+07	\N	\N
31	10	2	2014-06-17	7	2014-06-19 14:06:58.062024+07	2014-06-19 14:06:58.062024+07	\N	\N
32	1	2	2014-06-17	8	2014-06-19 14:06:58.073724+07	2014-06-19 14:06:58.073724+07	\N	\N
33	9	2	2014-06-17	9	2014-06-19 14:06:58.084799+07	2014-06-19 14:06:58.084799+07	\N	\N
34	2	2	2014-06-17	10	2014-06-19 14:06:58.096006+07	2014-06-19 14:06:58.096006+07	\N	\N
35	1	2	2014-06-17	1	2014-06-19 18:06:31.310221+07	2014-06-19 18:06:31.310221+07	\N	\N
36	1	2	2014-06-17	1	2014-06-19 18:06:42.408152+07	2014-06-19 18:06:42.408152+07	\N	\N
37	2	2	2014-06-17	11	2014-06-19 18:07:20.826429+07	2014-06-19 18:07:20.826429+07	\N	\N
38	3	2	2014-06-17	1	2014-06-19 18:07:25.709364+07	2014-06-19 18:07:25.709364+07	\N	\N
39	6	2	2014-06-17	9	2014-06-19 18:08:19.834543+07	2014-06-19 18:08:19.834543+07	\N	\N
49	18	2	2014-06-23	1	2014-06-23 22:05:47.687337+07	2014-06-23 22:05:47.687337+07	\N	\N
50	19	2	2014-06-23	2	2014-06-23 22:05:47.847529+07	2014-06-23 22:05:47.847529+07	\N	\N
51	18	2	2014-06-22	4	2014-06-23 22:07:48.858738+07	2014-06-23 22:07:48.858738+07	\N	\N
52	19	2	2014-06-22	4	2014-06-23 22:07:48.996219+07	2014-06-23 22:07:48.996219+07	\N	\N
53	1	2	2014-06-24	2	2014-06-24 12:44:27.492383+07	2014-06-24 12:44:27.492383+07	\N	\N
54	18	2	2014-06-24	1	2014-06-24 12:44:27.861439+07	2014-06-24 12:44:27.861439+07	\N	\N
55	19	2	2014-06-24	1	2014-06-24 12:44:27.869749+07	2014-06-24 12:44:27.869749+07	\N	\N
56	2	2	2014-06-24	1	2014-06-24 12:45:17.928812+07	2014-06-24 12:45:17.928812+07	\N	1
57	4	2	2014-06-24	5	2014-06-24 12:45:18.041516+07	2014-06-24 12:45:18.041516+07	\N	1
59	1	2	2014-06-25	1	2014-06-25 15:07:14.288792+07	2014-06-25 15:07:14.288792+07	\N	1
60	2	2	2014-06-25	1	2014-06-25 15:23:13.188348+07	2014-06-25 15:23:13.188348+07	\N	1
71	1	2	2014-06-27	1	2014-07-14 10:48:50.981747+07	2014-07-14 10:48:50.981747+07	\N	1
72	2	2	2014-06-27	1	2014-07-14 10:48:51.064354+07	2014-07-14 10:48:51.064354+07	\N	1
73	3	2	2014-06-27	1	2014-07-14 10:48:51.174373+07	2014-07-14 10:48:51.174373+07	\N	1
74	4	2	2014-06-27	1	2014-07-14 10:48:51.184608+07	2014-07-14 10:48:51.184608+07	\N	1
75	5	2	2014-06-27	1	2014-07-14 10:48:51.195696+07	2014-07-14 10:48:51.195696+07	\N	1
76	6	2	2014-06-27	1	2014-07-14 10:48:51.207154+07	2014-07-14 10:48:51.207154+07	\N	1
82	8	1	2016-07-01	10	2016-07-23 14:11:47.355737+07	2016-07-23 14:11:47.355737+07	\N	1
83	8	1	2016-07-01	10	2016-07-23 14:11:47.391099+07	2016-07-23 14:11:47.391099+07	\N	2
84	8	1	2016-07-02	10	2016-07-23 14:11:47.402065+07	2016-07-23 14:11:47.402065+07	\N	1
85	8	1	2016-07-02	10	2016-07-23 14:11:47.413161+07	2016-07-23 14:11:47.413161+07	\N	2
86	8	1	2016-07-01	20	2016-07-23 14:12:05.95239+07	2016-07-23 14:12:05.95239+07	\N	1
87	8	1	2016-07-01	20	2016-07-23 14:12:05.991307+07	2016-07-23 14:12:05.991307+07	\N	2
88	8	1	2016-07-02	20	2016-07-23 14:12:06.002493+07	2016-07-23 14:12:06.002493+07	\N	1
89	8	1	2016-07-02	20	2016-07-23 14:12:06.013473+07	2016-07-23 14:12:06.013473+07	\N	2
\.


--
-- TOC entry 4460 (class 0 OID 16594)
-- Dependencies: 226
-- Data for Name: tcminv_supplier; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tcminv_supplier (supplier_id, supplier_name, supplier_details, create_at) FROM stdin;
\.


--
-- TOC entry 4462 (class 0 OID 16601)
-- Dependencies: 228
-- Data for Name: tcminv_surat_izin; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tcminv_surat_izin (surat_izin_id, surat_izin, surat_izin_desc, create_at, date_min, date_max, user_id, update_at) FROM stdin;
1	SI 7980	SI/7899/X/2013 Ex SI/2906/IV/2014	2014-06-16 17:43:57.706294+07	2022-01-01	2022-02-11	\N	2014-06-16 17:43:57.706294+07
2	SI 7972	Surat Izin Control	2014-06-16 18:00:02.370814+07	2022-01-01	2022-02-11	\N	2014-06-16 18:00:02.370814+07
\.


--
-- TOC entry 4464 (class 0 OID 16609)
-- Dependencies: 230
-- Data for Name: tcminv_user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tcminv_user (user_id, username, fullname, email, address, phone, phone2, contractor_id, create_at, update_at, level_id, password) FROM stdin;
3	admin	\N	admin	admin	\N	\N	1	2016-07-23 14:02:37.326271+07	2016-07-23 14:02:37.326271+07	1	21232f297a57a5a743894a0e4a801fc3
\.


--
-- TOC entry 4451 (class 0 OID 16563)
-- Dependencies: 217
-- Data for Name: tcminv_user_level; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tcminv_user_level (level_id, level) FROM stdin;
1	admin
\.


--
-- TOC entry 4465 (class 0 OID 16616)
-- Dependencies: 231
-- Data for Name: tcminv_user_log; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tcminv_user_log (log_id, user_id, login, logout) FROM stdin;
1	3	2016-07-23 14:06:02+07	\N
2	3	2022-02-11 14:32:57+07	\N
\.


--
-- TOC entry 4485 (class 0 OID 0)
-- Dependencies: 210
-- Name: tcm_item_item_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tcm_item_item_id_seq', 19, true);


--
-- TOC entry 4486 (class 0 OID 0)
-- Dependencies: 212
-- Name: tcm_qty_unit_qty_unit_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tcm_qty_unit_qty_unit_id_seq', 2, true);


--
-- TOC entry 4487 (class 0 OID 0)
-- Dependencies: 214
-- Name: tcminv_category_category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tcminv_category_category_id_seq', 9, true);


--
-- TOC entry 4488 (class 0 OID 0)
-- Dependencies: 216
-- Name: tcminv_contractor_contractor_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tcminv_contractor_contractor_id_seq', 2, true);


--
-- TOC entry 4489 (class 0 OID 0)
-- Dependencies: 218
-- Name: tcminv_level_level_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tcminv_level_level_id_seq', 1, false);


--
-- TOC entry 4490 (class 0 OID 0)
-- Dependencies: 220
-- Name: tcminv_purchase_order_po_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tcminv_purchase_order_po_id_seq', 1, false);


--
-- TOC entry 4491 (class 0 OID 0)
-- Dependencies: 223
-- Name: tcminv_stock_out_stock_out_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tcminv_stock_out_stock_out_id_seq', 89, true);


--
-- TOC entry 4492 (class 0 OID 0)
-- Dependencies: 225
-- Name: tcminv_stock_stock_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tcminv_stock_stock_id_seq', 88, true);


--
-- TOC entry 4493 (class 0 OID 0)
-- Dependencies: 227
-- Name: tcminv_supplier_supplier_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tcminv_supplier_supplier_id_seq', 1, false);


--
-- TOC entry 4494 (class 0 OID 0)
-- Dependencies: 229
-- Name: tcminv_surat_izin_surat_izin_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tcminv_surat_izin_surat_izin_id_seq', 2, true);


--
-- TOC entry 4495 (class 0 OID 0)
-- Dependencies: 232
-- Name: tcminv_user_log_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tcminv_user_log_log_id_seq', 2, true);


--
-- TOC entry 4496 (class 0 OID 0)
-- Dependencies: 233
-- Name: tcminv_user_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tcminv_user_user_id_seq', 3, true);


--
-- TOC entry 4264 (class 2606 OID 16634)
-- Name: tcminv_item tcm_item2_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tcminv_item
    ADD CONSTRAINT tcm_item2_pkey PRIMARY KEY (item_id);


--
-- TOC entry 4266 (class 2606 OID 16636)
-- Name: tcminv_qty_unit tcm_qty_unit_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tcminv_qty_unit
    ADD CONSTRAINT tcm_qty_unit_pkey PRIMARY KEY (qty_unit_id);


--
-- TOC entry 4268 (class 2606 OID 16638)
-- Name: tcminv_category tcminv_category_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tcminv_category
    ADD CONSTRAINT tcminv_category_pkey PRIMARY KEY (category_id);


--
-- TOC entry 4270 (class 2606 OID 16640)
-- Name: tcminv_contractor tcminv_contractor_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tcminv_contractor
    ADD CONSTRAINT tcminv_contractor_pkey PRIMARY KEY (contractor_id);


--
-- TOC entry 4272 (class 2606 OID 16642)
-- Name: tcminv_user_level tcminv_level_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tcminv_user_level
    ADD CONSTRAINT tcminv_level_pkey PRIMARY KEY (level_id);


--
-- TOC entry 4274 (class 2606 OID 16644)
-- Name: tcminv_purchase_order tcminv_purchase_order_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tcminv_purchase_order
    ADD CONSTRAINT tcminv_purchase_order_pkey PRIMARY KEY (po_id);


--
-- TOC entry 4280 (class 2606 OID 16646)
-- Name: tcminv_stock_return tcminv_stock_out_copy_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tcminv_stock_return
    ADD CONSTRAINT tcminv_stock_out_copy_pkey PRIMARY KEY (stock_out_id);


--
-- TOC entry 4278 (class 2606 OID 16648)
-- Name: tcminv_stock_out tcminv_stock_out_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tcminv_stock_out
    ADD CONSTRAINT tcminv_stock_out_pkey PRIMARY KEY (stock_out_id);


--
-- TOC entry 4276 (class 2606 OID 16650)
-- Name: tcminv_stock_in tcminv_stock_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tcminv_stock_in
    ADD CONSTRAINT tcminv_stock_pkey PRIMARY KEY (stock_in_id);


--
-- TOC entry 4282 (class 2606 OID 16652)
-- Name: tcminv_supplier tcminv_supplier_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tcminv_supplier
    ADD CONSTRAINT tcminv_supplier_pkey PRIMARY KEY (supplier_id);


--
-- TOC entry 4284 (class 2606 OID 16654)
-- Name: tcminv_surat_izin tcminv_surat_izin_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tcminv_surat_izin
    ADD CONSTRAINT tcminv_surat_izin_pkey PRIMARY KEY (surat_izin_id);


--
-- TOC entry 4288 (class 2606 OID 16656)
-- Name: tcminv_user_log tcminv_user_log_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tcminv_user_log
    ADD CONSTRAINT tcminv_user_log_pkey PRIMARY KEY (log_id);


--
-- TOC entry 4286 (class 2606 OID 16658)
-- Name: tcminv_user tcminv_user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tcminv_user
    ADD CONSTRAINT tcminv_user_pkey PRIMARY KEY (user_id);


--
-- TOC entry 4289 (class 2606 OID 16659)
-- Name: tcminv_item category; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tcminv_item
    ADD CONSTRAINT category FOREIGN KEY (category_id) REFERENCES public.tcminv_category(category_id);


--
-- TOC entry 4296 (class 2606 OID 16664)
-- Name: tcminv_stock_out contractor; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tcminv_stock_out
    ADD CONSTRAINT contractor FOREIGN KEY (contractor_id) REFERENCES public.tcminv_contractor(contractor_id);


--
-- TOC entry 4293 (class 2606 OID 16669)
-- Name: tcminv_stock_in item; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tcminv_stock_in
    ADD CONSTRAINT item FOREIGN KEY (item_id) REFERENCES public.tcminv_item(item_id);


--
-- TOC entry 4297 (class 2606 OID 16674)
-- Name: tcminv_stock_out item; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tcminv_stock_out
    ADD CONSTRAINT item FOREIGN KEY (item_id) REFERENCES public.tcminv_item(item_id);


--
-- TOC entry 4290 (class 2606 OID 16679)
-- Name: tcminv_item qty_unit; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tcminv_item
    ADD CONSTRAINT qty_unit FOREIGN KEY (qty_unit_id) REFERENCES public.tcminv_qty_unit(qty_unit_id);


--
-- TOC entry 4292 (class 2606 OID 16684)
-- Name: tcminv_category qty_unit; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tcminv_category
    ADD CONSTRAINT qty_unit FOREIGN KEY (qty_unit_id) REFERENCES public.tcminv_qty_unit(qty_unit_id);


--
-- TOC entry 4294 (class 2606 OID 16689)
-- Name: tcminv_stock_in supplier; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tcminv_stock_in
    ADD CONSTRAINT supplier FOREIGN KEY (supplier_id) REFERENCES public.tcminv_supplier(supplier_id);


--
-- TOC entry 4295 (class 2606 OID 16694)
-- Name: tcminv_stock_in surat_izin; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tcminv_stock_in
    ADD CONSTRAINT surat_izin FOREIGN KEY (surat_izin_id) REFERENCES public.tcminv_surat_izin(surat_izin_id);


--
-- TOC entry 4298 (class 2606 OID 16699)
-- Name: tcminv_stock_out surat_izin; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tcminv_stock_out
    ADD CONSTRAINT surat_izin FOREIGN KEY (surat_izin_id) REFERENCES public.tcminv_surat_izin(surat_izin_id);


--
-- TOC entry 4299 (class 2606 OID 16704)
-- Name: tcminv_stock_return tcminv_stock_return_copy_contractor_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tcminv_stock_return
    ADD CONSTRAINT tcminv_stock_return_copy_contractor_id_fkey FOREIGN KEY (contractor_id) REFERENCES public.tcminv_contractor(contractor_id);


--
-- TOC entry 4300 (class 2606 OID 16709)
-- Name: tcminv_stock_return tcminv_stock_return_copy_item_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tcminv_stock_return
    ADD CONSTRAINT tcminv_stock_return_copy_item_id_fkey FOREIGN KEY (item_id) REFERENCES public.tcminv_item(item_id);


--
-- TOC entry 4301 (class 2606 OID 16714)
-- Name: tcminv_stock_return tcminv_stock_return_copy_surat_izin_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tcminv_stock_return
    ADD CONSTRAINT tcminv_stock_return_copy_surat_izin_id_fkey FOREIGN KEY (surat_izin_id) REFERENCES public.tcminv_surat_izin(surat_izin_id);


--
-- TOC entry 4291 (class 2606 OID 16719)
-- Name: tcminv_item user; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tcminv_item
    ADD CONSTRAINT "user" FOREIGN KEY (user_id) REFERENCES public.tcminv_user(user_id);


--
-- TOC entry 4303 (class 2606 OID 16724)
-- Name: tcminv_user_log user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tcminv_user_log
    ADD CONSTRAINT user_id FOREIGN KEY (user_id) REFERENCES public.tcminv_user(user_id);


--
-- TOC entry 4302 (class 2606 OID 16729)
-- Name: tcminv_user user_level; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tcminv_user
    ADD CONSTRAINT user_level FOREIGN KEY (level_id) REFERENCES public.tcminv_user_level(level_id);


-- Completed on 2022-02-11 14:46:10 WIB

--
-- PostgreSQL database dump complete
--


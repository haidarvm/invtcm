CREATE TABLE "public"."tcm_item" (
"item_id" serial4 NOT NULL,
"name" varchar(255) COLLATE "default",
"item_code" varchar(255) COLLATE "default",
"surat_izin" int2,
"qty" int2,
"qty_unit_id" int2,
"description" varchar(255) COLLATE "default",
"create_at" timestamptz(6) DEFAULT now(),
"update_at" timestamptz(6),
"user_id" int4,
CONSTRAINT "tcm_item2_pkey" PRIMARY KEY ("item_id")
);


SELECT ti.item_id,ti.item_name,SUM(qty_in) as sum_qty_in, SUM(qty_in) - SUM(qty_in) qty_av, SUM(qty_out) as sum_qty_out, sti.surat_izin_id, si.surat_izin
FROM "tcminv_item"  ti
INNER JOIN "tcminv_stock_in"  sti ON sti."item_id" = ti."item_id" 
INNER JOIN "tcminv_surat_izin" si ON si."surat_izin_id" = sti."surat_izin_id"
LEFT JOIN tcminv_stock_out sto ON sto.item_id = ti.item_id
WHERE sti."surat_izin_id" = '2'  AND sto.surat_izin_id = '2'
GROUP BY ti.item_id, sti.surat_izin_id, sto.surat_izin_id, si.surat_izin	
ORDER BY ti.item_id;

 SELECT "tcminv_stock_in"."item_id", MIN("item_name"), MIN("item_code"), SUM(qty_in) as sum_qty_in, SUM(qty_out) as sum_qty_out, SUM(qty_in) - SUM(qty_in) qty_av, 
"tcminv_stock_in"."surat_izin_id", "tcminv_surat_izin"."surat_izin" 
FROM "tcminv_item" 
INNER JOIN "tcminv_stock_in" ON "tcminv_stock_in"."item_id" = "tcminv_item"."item_id" 
INNER JOIN "tcminv_surat_izin" ON "tcminv_surat_izin"."surat_izin_id" = "tcminv_stock_in"."surat_izin_id" 
LEFT JOIN "tcminv_stock_out" ON "tcminv_stock_out"."item_id" = "tcminv_stock_out"."item_id" 
WHERE "tcminv_stock_in"."surat_izin_id" = '2' 
GROUP BY "tcminv_stock_in"."item_id", "tcminv_stock_in"."surat_izin_id", "tcminv_stock_out"."surat_izin_id", "tcminv_surat_izin"."surat_izin";


SELECT item_id, surat_izin_id, SUM(qty_out) 
FROM tcminv_stock_out
GROUP BY item_id, surat_izin_id
ORDER BY item_id;

SELECT item_id, surat_izin_id, SUM(qty_in) 
FROM tcminv_stock_in
GROUP BY item_id, surat_izin_id
ORDER BY item_id;



SELECT sti.item_id, item_name , item_code, sti.surat_izin_id, surat_izin, sum_qty_in, sum_qty_out, sum_qty_in - sum_qty_out as qty_av,
qty_unit
FROM (
SELECT MIN(ti.item_name) as item_name, MAX(ti.item_code) as item_code, sti.item_id , 
MAX(si.surat_izin) as surat_izin, sti.surat_izin_id , SUM(qty_in) as sum_qty_in,
MAX(qty_unit) as qty_unit
FROM tcminv_stock_in sti
INNER JOIN tcminv_item ti ON sti.item_id = ti.item_id
INNER JOIN tcminv_qty_unit qu ON ti.qty_unit_id = qu.qty_unit_id
RIGHT JOIN tcminv_surat_izin si ON sti.surat_izin_id = si.surat_izin_id
GROUP BY sti.item_id, sti.surat_izin_id
) sti
LEFT JOIN (
SELECT item_id, surat_izin_id, SUM(qty_out)  as sum_qty_out
FROM tcminv_stock_out
GROUP BY item_id, surat_izin_id
) sto USING ( item_id, surat_izin_id)
ORDER BY item_id, surat_izin_id DESC;


SELECT MIN(ti.item_name) as item_name, MAX(ti.item_code) as item_code, sti.item_id , 
MAX(si.surat_izin) as surat_izin, sti.surat_izin_id , SUM(qty_in) as sum_qty_in,
MAX(qty_unit) as qty_unit
FROM tcminv_stock_in sti
RIGHT JOIN tcminv_item ti ON sti.item_id = ti.item_id
INNER JOIN tcminv_qty_unit qu ON ti.qty_unit_id = qu.qty_unit_id
RIGHT JOIN tcminv_surat_izin si ON sti.surat_izin_id = si.surat_izin_id
GROUP BY sti.item_id, sti.surat_izin_id

SELECT sti.item_id, MAX(sti.surat_izin_id), MAX(sti.qty_in)
-- ,SUM(qty_out)
-- SUM(qty_in) as sum_in, SUM(qty_out) as sum_out
FROM tcminv_stock_in sti
-- INNER JOIN tcminv_stock_out sto ON sti.item_id =  sto.item_id   AND  sto.surat_izin_id = sti.surat_izin_id
INNER JOIN tcminv_stock_out sto on sti.item_id = sto.item_id
GROUP BY sti.item_id, sti.surat_izin_id
ORDER BY sti.item_id;public

-- DELETE FROM tcminv_stock_in WHERE qty_in = 0


 SELECT * FROM "tcminv_surat_izin" WHERE "date_min" <= '2014-06-19' AND "date_max" >= '2014-06-19';



-- Daily Report
SELECT sti.item_id, date_in, SUM(qty_in), string_agg(  to_char(qty_in, '999'),  ', ' )  
FROM  tcminv_item ti
LEFT JOIN tcminv_stock_in sti ON sti.item_id = ti.item_id
GROUP BY sti.item_id, sti.date_in, sti.surat_izin_id
ORDER BY sti.item_id;

select  item_id,qty_in,
    coalesce(to_char(date_in,'YYYY-MM-DD'), '') as A
  from tcminv_stock_in ;

SELECT MIN(stock_in_id), MAX(date_in),item_id, to_char(qty_in, '999')
--string_agg(qty_in, ',')
FROM tcminv_stock_in 
GROUP BY item_id, qty_in;

select * FROM crosstab(
'SELECT item_id, qty_in, date_in 
FROM tcminv_stock_in
GROUP BY 1,2
ORDER BY 1
'
) as qty_in ('Item' text,  '2014-06-20' text, '2014-06-19' text )
;

select  extract(day from DATE '2014-06-20');

select extract(day from  date_in) from tcminv_stock_in;

SELECT item_id,
       SUM(CASE date_in  WHEN '2014-06-20' THEN qty_in ELSE 0 END) AS  "20",
       SUM(CASE date_in   WHEN '2014-06-19' THEN qty_in ELSE 0 END) AS "19"
FROM tcminv_stock_in
GROUP BY item_id
ORDER BY 1;

 SELECT item_id, 
SUM(CASE date_in WHEN '2014-06-17' THEN qty_in ELSE 0 END) AS "2014-06-17",
SUM(CASE date_in WHEN '2014-06-18' THEN qty_in ELSE 0 END) AS "2014-06-18",
SUM(CASE date_in WHEN '2014-06-19' THEN qty_in ELSE 0 END) AS "2014-06-19",
SUM(CASE date_in WHEN '2014-06-20' THEN qty_in ELSE 0 END) AS "2014-06-20" ,
SUM(CASE to_char(date_in,'YY-MM') WHEN '14-06' THEN qty_in ELSE 0 END) AS "total" 
FROM tcminv_stock_in GROUP BY item_id ORDER BY 1;



 SELECT date_in, 
SUM(CASE ti.category_id WHEN '6' THEN qty_in ELSE 0 END) AS "dinamit",
SUM(CASE ti.category_id WHEN '5' THEN qty_in ELSE 0 END) AS "detonator",
SUM(CASE ti.category_id WHEN '9' THEN qty_in ELSE 0 END) AS "pc" 
FROM tcminv_stock_in sti INNER JOIN tcminv_item ti ON ti.item_id = sti.item_id 
INNER JOIN tcminv_category tc ON tc.category_id = ti.category_id 
GROUP BY date_in ORDER BY date_in;


SELECT item_id, SUM(qty_in) , max(date_in) FROM tcminv_stock_in
GROUP BY item_id, date_in
ORDER BY item_id;

SELECT item_id, date_in, qty_in FROM tcminv_stock_in
ORDER BY date_in DESC;

SELECT date_in FROM tcminv_stock_in
GROUP BY date_in 
ORDER BY date_in DESC;

SELECT item_name, qty_in 
FROM  tcminv_item ti
LEFT JOIN tcminv_stock_in sti ON sti.item_id = ti.item_id

UPDATE tcminv_stock_out 
set date_out = '2014-06-17'
WHERE date_out = '1970-01-01'



SELECT MAX(sti.item_id) as item_id, MAX(qty_unit) as qty_unit , MAX(date_in),
SUM(CASE ti.category_id WHEN '6' THEN qty_in ELSE 0 END) AS "dinamit",
SUM(CASE ti.category_id WHEN '5' THEN qty_in ELSE 0 END) AS "detonator",
SUM(CASE ti.category_id WHEN '9' THEN qty_in ELSE 0 END) AS "pc" 
FROM tcminv_stock_in sti 
INNER JOIN tcminv_item ti ON sti.item_id = ti.item_id 
INNER JOIN tcminv_category tc on tc.category_id = ti.category_id
INNER JOIN tcminv_surat_izin si ON sti.surat_izin_id = si.surat_izin_id 
INNER JOIN tcminv_qty_unit qu ON ti.qty_unit_id = qu.qty_unit_id 
WHERE sti.surat_izin_id = 2 GROUP BY date_in 


SELECT MAX(sti.item_id) as item_id, MAX(qty_unit) as qty_unit , MAX(date_in) as date_in,
SUM(CASE ti.category_id WHEN '6' THEN qty_in ELSE 0 END) AS "dinamit_in",
SUM(CASE ti.category_id WHEN '5' THEN qty_in ELSE 0 END) AS "detonator_in",
SUM(CASE ti.category_id WHEN '9' THEN qty_in ELSE 0 END) AS "pc_in" 
FROM tcminv_stock_in sti 
INNER JOIN tcminv_item ti ON sti.item_id = ti.item_id 
INNER JOIN tcminv_category tc on tc.category_id = ti.category_id
INNER JOIN tcminv_surat_izin si ON sti.surat_izin_id = si.surat_izin_id 
INNER JOIN tcminv_qty_unit qu ON ti.qty_unit_id = qu.qty_unit_id 
WHERE sti.surat_izin_id = 2 AND date_in ='2014-06-19'


SELECT date_in,"dinamit_in", "dinamit_out"
FROM ( 
SELECT MAX(sti.item_id) as item_id, MAX(qty_unit) as qty_unit , MAX(date_in) as date_in,
SUM(CASE ti.category_id WHEN '6' THEN qty_in ELSE 0 END) AS "dinamit_in",
SUM(CASE ti.category_id WHEN '5' THEN qty_in ELSE 0 END) AS "detonator_in",
SUM(CASE ti.category_id WHEN '9' THEN qty_in ELSE 0 END) AS "pc_in" 
FROM tcminv_stock_in sti 
INNER JOIN tcminv_item ti ON sti.item_id = ti.item_id 
INNER JOIN tcminv_category tc on tc.category_id = ti.category_id
INNER JOIN tcminv_surat_izin si ON sti.surat_izin_id = si.surat_izin_id 
INNER JOIN tcminv_qty_unit qu ON ti.qty_unit_id = qu.qty_unit_id 
WHERE sti.surat_izin_id = 2 GROUP BY date_in  ) sti 
LEFT JOIN ( 
SELECT date_out , SUM(qty_out) as sum_qty_out , MAX(sto.item_id) as item_id,
SUM(CASE ti.category_id WHEN '6' THEN qty_out ELSE 0 END) AS "dinamit_out",
SUM(CASE ti.category_id WHEN '5' THEN qty_out ELSE 0 END) AS "detonator_out",
SUM(CASE ti.category_id WHEN '9' THEN qty_out ELSE 0 END) AS "pc_out" 
FROM tcminv_stock_out sto
INNER JOIN tcminv_item ti ON sto.item_id = ti.item_id 
INNER JOIN tcminv_category tc on tc.category_id = ti.category_id
WHERE surat_izin_id = 2 GROUP BY date_out ) sto 
ON (date_in = date_out )
LEFT JOIN ( 
SELECT date_return , MAX(rtn.item_id) as item_id , SUM(qty_return) as sum_qty_return ,
SUM(CASE ti.category_id WHEN '6' THEN qty_return ELSE 0 END) AS "dinamit_return",
SUM(CASE ti.category_id WHEN '5' THEN qty_return ELSE 0 END) AS "detonator_return",
SUM(CASE ti.category_id WHEN '9' THEN qty_return ELSE 0 END) AS "pc_return" 
FROM tcminv_stock_return  rtn
INNER JOIN tcminv_item ti ON rtn.item_id = ti.item_id 
INNER JOIN tcminv_category tc on tc.category_id = ti.category_id
WHERE surat_izin_id = 2 
GROUP BY date_return ) rtn
ON (date_in = date_return)
ORDER BY date_in DESC;

SELECT item_id, surat_izin_id, SUM(qty_return) as sum_qty_return FROM tcminv_stock_return WHERE surat_izin_id = 2 GROUP BY item_id, surat_izin_id


SELECT date_out FROM tcminv_stock_out GROUP BY date_out ORDER BY date_out ASC;

- qty_return

SELECT tso.item_id,MAX(item_name) as item_name, MAX(item_code) as item_code, MAX(qty_unit) as qty_unit, 
SUM(CASE date_out WHEN '2014-06-17' THEN qty_out  ELSE 0 END) AS "2014-06-17",
SUM(CASE date_out WHEN '2014-06-19' THEN qty_out ELSE 0 END) AS "2014-06-19",
SUM(CASE date_out WHEN '2014-06-20' THEN qty_out ELSE 0 END) AS "2014-06-20",
SUM(CASE date_out WHEN '2014-06-22' THEN qty_out ELSE 0 END) AS "2014-06-22",
SUM(CASE date_out WHEN '2014-06-23' THEN qty_out ELSE 0 END) AS "2014-06-23",
SUM(CASE date_out WHEN '2014-06-24' THEN qty_out ELSE 0 END) AS "2014-06-24",
SUM(CASE to_char(date_out, 'MM-YY') WHEN '06-14' THEN qty_out ELSE 0 END) AS "Total" 
FROM tcminv_stock_out tso INNER JOIN tcminv_item ti ON ti.item_id = tso.item_id 
INNER JOIN tcminv_qty_unit qu ON qu.qty_unit_id = ti.qty_unit_id 
-- LEFT JOIN tcminv_stock_return tsr ON tsr.item_id = tso.item_id
WHERE tso.surat_izin_id =2 
AND to_char(date_out, 'MM-YY') ='06-14'
-- AND tso.contractor_id ='1' 
 GROUP BY tso.item_id ORDER BY 1;


SELECT sto.item_id,  sum(qty_out) FROM  tcminv_stock_out sto
INNER JOIN tcminv_item ti ON ti.item_id = sto.item_id 
INNER JOIN tcminv_qty_unit qu ON qu.qty_unit_id = ti.qty_unit_id 
WHERE surat_izin_id =2 AND to_char(date_out, 'MM-YY') ='06-14' AND contractor_id ='1' 
AND sto.item_id= 1 AND date_out = '2014-06-17'
GROUP BY sto.item_id;


SELECT item_id, sum(qty_return) FROM  tcminv_stock_return
where item_id= 1 AND date_return = '2014-06-17'
GROUP BY item_id;




-- Query to get stock use
SELECT 
sto.item_id as item_id, item_name, 
COALESCE (sto."2014-06-17" - str."2014-06-17") as sum_use
FROM tcminv_item ti
INNER JOIN (
SELECT item_id,  
SUM(CASE date_out WHEN '2014-06-17' THEN qty_out  ELSE 0 END) AS "2014-06-17"
-- sum(qty_out)  as sum_qty_out 
FROM  tcminv_stock_out 
GROUP BY item_id
) as sto on (sto.item_id = ti.item_id)
LEFT JOIN (
SELECT item_id,
SUM(CASE date_return WHEN '2014-06-17' THEN qty_return  ELSE 0 END) AS "2014-06-17"
 FROM  tcminv_stock_return
GROUP BY item_id
) as str on (sto.item_id = str.item_id)



 SELECT tso.item_id,MAX(item_name) as item_name, MAX(item_code) as item_code, 
MAX(qty_unit) as qty_unit, 
SUM(CASE date_out WHEN '2014-06-01' THEN qty_out ELSE 0 END) AS "2014-06-01", SUM(CASE date_out WHEN '2014-06-02' THEN qty_out ELSE 0 END) AS "2014-06-02", SUM(CASE date_out WHEN '2014-06-03' THEN qty_out ELSE 0 END) AS "2014-06-03", SUM(CASE date_out WHEN '2014-06-04' THEN qty_out ELSE 0 END) AS "2014-06-04", SUM(CASE date_out WHEN '2014-06-05' THEN qty_out ELSE 0 END) AS "2014-06-05", SUM(CASE date_out WHEN '2014-06-06' THEN qty_out ELSE 0 END) AS "2014-06-06", SUM(CASE date_out WHEN '2014-06-07' THEN qty_out ELSE 0 END) AS "2014-06-07", SUM(CASE date_out WHEN '2014-06-08' THEN qty_out ELSE 0 END) AS "2014-06-08", SUM(CASE date_out WHEN '2014-06-09' THEN qty_out ELSE 0 END) AS "2014-06-09", SUM(CASE date_out WHEN '2014-06-10' THEN qty_out ELSE 0 END) AS "2014-06-10", SUM(CASE date_out WHEN '2014-06-11' THEN qty_out ELSE 0 END) AS "2014-06-11", SUM(CASE date_out WHEN '2014-06-12' THEN qty_out ELSE 0 END) AS "2014-06-12", SUM(CASE date_out WHEN '2014-06-13' THEN qty_out ELSE 0 END) AS "2014-06-13", SUM(CASE date_out WHEN '2014-06-14' THEN qty_out ELSE 0 END) AS "2014-06-14", SUM(CASE date_out WHEN '2014-06-15' THEN qty_out ELSE 0 END) AS "2014-06-15", SUM(CASE date_out WHEN '2014-06-16' THEN qty_out ELSE 0 END) AS "2014-06-16", SUM(CASE date_out WHEN '2014-06-17' THEN qty_out ELSE 0 END) AS "2014-06-17", SUM(CASE date_out WHEN '2014-06-18' THEN qty_out ELSE 0 END) AS "2014-06-18", SUM(CASE date_out WHEN '2014-06-19' THEN qty_out ELSE 0 END) AS "2014-06-19", SUM(CASE date_out WHEN '2014-06-20' THEN qty_out ELSE 0 END) AS "2014-06-20", SUM(CASE date_out WHEN '2014-06-21' THEN qty_out ELSE 0 END) AS "2014-06-21", SUM(CASE date_out WHEN '2014-06-22' THEN qty_out ELSE 0 END) AS "2014-06-22", SUM(CASE date_out WHEN '2014-06-23' THEN qty_out ELSE 0 END) AS "2014-06-23", SUM(CASE date_out WHEN '2014-06-24' THEN qty_out ELSE 0 END) AS "2014-06-24", SUM(CASE date_out WHEN '2014-06-25' THEN qty_out ELSE 0 END) AS "2014-06-25", SUM(CASE date_out WHEN '2014-06-26' THEN qty_out ELSE 0 END) AS "2014-06-26", SUM(CASE date_out WHEN '2014-06-27' THEN qty_out ELSE 0 END) AS "2014-06-27", SUM(CASE date_out WHEN '2014-06-28' THEN qty_out ELSE 0 END) AS "2014-06-28", SUM(CASE date_out WHEN '2014-06-29' THEN qty_out ELSE 0 END) AS "2014-06-29", SUM(CASE date_out WHEN '2014-06-30' THEN qty_out ELSE 0 END) AS "2014-06-30", SUM(CASE to_char(date_out, 'YY-MM') WHEN '14-06' THEN qty_out ELSE 0 END) AS "Total" FROM tcminv_stock_out tso INNER JOIN tcminv_item ti ON ti.item_id = tso.item_id INNER JOIN tcminv_qty_unit qu ON qu.qty_unit_id = ti.qty_unit_id 
WHERE surat_izin_id =2 AND to_char(date_out, 'MM-YY') ='06-14' AND contractor_id ='1' GROUP BY tso.item_id ORDER BY 1;

SELECT sto.item_id as item_id, MAX(item_name) as item_name, MAX(item_code) as item_code, MAX(qty_unit) as qty_unit, MAX(sto.surat_izin_id) as surat_izin_id, 
COALESCE (MAX(sto."2014-06-17") - MAX(str."2014-06-17")) as "2014-04-17" 
FROM tcminv_item ti INNER JOIN tcminv_qty_unit qu ON qu.qty_unit_id = ti.qty_unit_id 
INNER JOIN ( 
SELECT item_id, MAX(sto.surat_izin_id) as surat_izin_id, MAX(sto.contractor_id) as contractor_id, SUM(CASE date_out WHEN '2014-06-01' THEN qty_out ELSE 0 END) AS "2014-06-01",SUM(CASE date_out WHEN '2014-06-02' THEN qty_out ELSE 0 END) AS "2014-06-02",SUM(CASE date_out WHEN '2014-06-03' THEN qty_out ELSE 0 END) AS "2014-06-03",SUM(CASE date_out WHEN '2014-06-04' THEN qty_out ELSE 0 END) AS "2014-06-04",SUM(CASE date_out WHEN '2014-06-05' THEN qty_out ELSE 0 END) AS "2014-06-05",SUM(CASE date_out WHEN '2014-06-06' THEN qty_out ELSE 0 END) AS "2014-06-06",SUM(CASE date_out WHEN '2014-06-07' THEN qty_out ELSE 0 END) AS "2014-06-07",SUM(CASE date_out WHEN '2014-06-08' THEN qty_out ELSE 0 END) AS "2014-06-08",SUM(CASE date_out WHEN '2014-06-09' THEN qty_out ELSE 0 END) AS "2014-06-09",SUM(CASE date_out WHEN '2014-06-10' THEN qty_out ELSE 0 END) AS "2014-06-10",SUM(CASE date_out WHEN '2014-06-11' THEN qty_out ELSE 0 END) AS "2014-06-11",SUM(CASE date_out WHEN '2014-06-12' THEN qty_out ELSE 0 END) AS "2014-06-12",SUM(CASE date_out WHEN '2014-06-13' THEN qty_out ELSE 0 END) AS "2014-06-13",SUM(CASE date_out WHEN '2014-06-14' THEN qty_out ELSE 0 END) AS "2014-06-14",SUM(CASE date_out WHEN '2014-06-15' THEN qty_out ELSE 0 END) AS "2014-06-15",SUM(CASE date_out WHEN '2014-06-16' THEN qty_out ELSE 0 END) AS "2014-06-16",SUM(CASE date_out WHEN '2014-06-17' THEN qty_out ELSE 0 END) AS "2014-06-17",SUM(CASE date_out WHEN '2014-06-18' THEN qty_out ELSE 0 END) AS "2014-06-18",SUM(CASE date_out WHEN '2014-06-19' THEN qty_out ELSE 0 END) AS "2014-06-19",SUM(CASE date_out WHEN '2014-06-20' THEN qty_out ELSE 0 END) AS "2014-06-20",SUM(CASE date_out WHEN '2014-06-21' THEN qty_out ELSE 0 END) AS "2014-06-21",SUM(CASE date_out WHEN '2014-06-22' THEN qty_out ELSE 0 END) AS "2014-06-22",SUM(CASE date_out WHEN '2014-06-23' THEN qty_out ELSE 0 END) AS "2014-06-23",SUM(CASE date_out WHEN '2014-06-24' THEN qty_out ELSE 0 END) AS "2014-06-24",SUM(CASE date_out WHEN '2014-06-25' THEN qty_out ELSE 0 END) AS "2014-06-25",SUM(CASE date_out WHEN '2014-06-26' THEN qty_out ELSE 0 END) AS "2014-06-26",SUM(CASE date_out WHEN '2014-06-27' THEN qty_out ELSE 0 END) AS "2014-06-27",SUM(CASE date_out WHEN '2014-06-28' THEN qty_out ELSE 0 END) AS "2014-06-28",SUM(CASE date_out WHEN '2014-06-29' THEN qty_out ELSE 0 END) AS "2014-06-29",SUM(CASE date_out WHEN '2014-06-30' THEN qty_out ELSE 0 END) AS "2014-06-30" FROM tcminv_stock_out sto WHERE sto.surat_izin_id =2 AND to_char(date_out, 'MM-YY') ='06-14' AND sto.contractor_id ='1' GROUP BY item_id ) as sto on (sto.item_id = ti.item_id) LEFT JOIN ( SELECT item_id, SUM(CASE date_return WHEN '2014-06-01' THEN qty_return ELSE 0 END) AS "2014-06-01",SUM(CASE date_return WHEN '2014-06-02' THEN qty_return ELSE 0 END) AS "2014-06-02",SUM(CASE date_return WHEN '2014-06-03' THEN qty_return ELSE 0 END) AS "2014-06-03",SUM(CASE date_return WHEN '2014-06-04' THEN qty_return ELSE 0 END) AS "2014-06-04",SUM(CASE date_return WHEN '2014-06-05' THEN qty_return ELSE 0 END) AS "2014-06-05",SUM(CASE date_return WHEN '2014-06-06' THEN qty_return ELSE 0 END) AS "2014-06-06",SUM(CASE date_return WHEN '2014-06-07' THEN qty_return ELSE 0 END) AS "2014-06-07",SUM(CASE date_return WHEN '2014-06-08' THEN qty_return ELSE 0 END) AS "2014-06-08",SUM(CASE date_return WHEN '2014-06-09' THEN qty_return ELSE 0 END) AS "2014-06-09",SUM(CASE date_return WHEN '2014-06-10' THEN qty_return ELSE 0 END) AS "2014-06-10",SUM(CASE date_return WHEN '2014-06-11' THEN qty_return ELSE 0 END) AS "2014-06-11",SUM(CASE date_return WHEN '2014-06-12' THEN qty_return ELSE 0 END) AS "2014-06-12",SUM(CASE date_return WHEN '2014-06-13' THEN qty_return ELSE 0 END) AS "2014-06-13",SUM(CASE date_return WHEN '2014-06-14' THEN qty_return ELSE 0 END) AS "2014-06-14",SUM(CASE date_return WHEN '2014-06-15' THEN qty_return ELSE 0 END) AS "2014-06-15",SUM(CASE date_return WHEN '2014-06-16' THEN qty_return ELSE 0 END) AS "2014-06-16",SUM(CASE date_return WHEN '2014-06-17' THEN qty_return ELSE 0 END) AS "2014-06-17",SUM(CASE date_return WHEN '2014-06-18' THEN qty_return ELSE 0 END) AS "2014-06-18",SUM(CASE date_return WHEN '2014-06-19' THEN qty_return ELSE 0 END) AS "2014-06-19",SUM(CASE date_return WHEN '2014-06-20' THEN qty_return ELSE 0 END) AS "2014-06-20",SUM(CASE date_return WHEN '2014-06-21' THEN qty_return ELSE 0 END) AS "2014-06-21",SUM(CASE date_return WHEN '2014-06-22' THEN qty_return ELSE 0 END) AS "2014-06-22",SUM(CASE date_return WHEN '2014-06-23' THEN qty_return ELSE 0 END) AS "2014-06-23",SUM(CASE date_return WHEN '2014-06-24' THEN qty_return ELSE 0 END) AS "2014-06-24",SUM(CASE date_return WHEN '2014-06-25' THEN qty_return ELSE 0 END) AS "2014-06-25",SUM(CASE date_return WHEN '2014-06-26' THEN qty_return ELSE 0 END) AS "2014-06-26",SUM(CASE date_return WHEN '2014-06-27' THEN qty_return ELSE 0 END) AS "2014-06-27",SUM(CASE date_return WHEN '2014-06-28' THEN qty_return ELSE 0 END) AS "2014-06-28",SUM(CASE date_return WHEN '2014-06-29' THEN qty_return ELSE 0 END) AS "2014-06-29",SUM(CASE date_return WHEN '2014-06-30' THEN qty_return ELSE 0 END) AS "2014-06-30",SUM(CASE to_char(date_return, 'YY-MM') WHEN '14-06' THEN qty_return ELSE 0 END) AS "total_return" FROM tcminv_stock_return str GROUP BY item_id ) as str on (sto.item_id = str.item_id) GROUP BY sto.item_id ORDER BY 1
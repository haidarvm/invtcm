--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: tablefunc; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS tablefunc WITH SCHEMA public;


--
-- Name: EXTENSION tablefunc; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION tablefunc IS 'functions that manipulate whole tables, including crosstab';


SET search_path = public, pg_catalog;

--
-- Name: qty_unit; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE qty_unit AS ENUM (
    'kg',
    'pcs'
);


ALTER TYPE public.qty_unit OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: tcminv_item; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tcminv_item (
    item_id integer NOT NULL,
    item_name character varying(255),
    item_code character varying(255),
    qty integer,
    qty_unit_id smallint,
    item_desc character varying(255),
    create_at timestamp with time zone DEFAULT now(),
    update_at timestamp with time zone DEFAULT statement_timestamp(),
    user_id integer,
    category_id integer
);


ALTER TABLE public.tcminv_item OWNER TO postgres;

--
-- Name: tcm_item_item_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tcm_item_item_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tcm_item_item_id_seq OWNER TO postgres;

--
-- Name: tcm_item_item_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tcm_item_item_id_seq OWNED BY tcminv_item.item_id;


--
-- Name: tcminv_qty_unit; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tcminv_qty_unit (
    qty_unit_id integer NOT NULL,
    qty_unit character varying(255),
    description text
);


ALTER TABLE public.tcminv_qty_unit OWNER TO postgres;

--
-- Name: tcm_qty_unit_qty_unit_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tcm_qty_unit_qty_unit_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tcm_qty_unit_qty_unit_id_seq OWNER TO postgres;

--
-- Name: tcm_qty_unit_qty_unit_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tcm_qty_unit_qty_unit_id_seq OWNED BY tcminv_qty_unit.qty_unit_id;


--
-- Name: tcminv_category; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tcminv_category (
    category_id integer NOT NULL,
    category_name character varying(255),
    create_at timestamp with time zone DEFAULT now(),
    category_desc text,
    qty_unit_id smallint
);


ALTER TABLE public.tcminv_category OWNER TO postgres;

--
-- Name: tcminv_category_category_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tcminv_category_category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tcminv_category_category_id_seq OWNER TO postgres;

--
-- Name: tcminv_category_category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tcminv_category_category_id_seq OWNED BY tcminv_category.category_id;


--
-- Name: tcminv_contractor; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tcminv_contractor (
    contractor_id integer NOT NULL,
    contractor_name character varying(255),
    contractor_desc text,
    create_at timestamp with time zone DEFAULT now()
);


ALTER TABLE public.tcminv_contractor OWNER TO postgres;

--
-- Name: tcminv_contractor_contractor_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tcminv_contractor_contractor_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tcminv_contractor_contractor_id_seq OWNER TO postgres;

--
-- Name: tcminv_contractor_contractor_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tcminv_contractor_contractor_id_seq OWNED BY tcminv_contractor.contractor_id;


--
-- Name: tcminv_user_level; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tcminv_user_level (
    level_id integer NOT NULL,
    level character varying(255)
);


ALTER TABLE public.tcminv_user_level OWNER TO postgres;

--
-- Name: tcminv_level_level_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tcminv_level_level_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tcminv_level_level_id_seq OWNER TO postgres;

--
-- Name: tcminv_level_level_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tcminv_level_level_id_seq OWNED BY tcminv_user_level.level_id;


--
-- Name: tcminv_purchase_order; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tcminv_purchase_order (
    po_id integer NOT NULL,
    po_name character varying(255),
    po_number bigint,
    po_value character varying(255),
    supplier_id integer,
    create_at timestamp with time zone DEFAULT now()
);


ALTER TABLE public.tcminv_purchase_order OWNER TO postgres;

--
-- Name: tcminv_purchase_order_po_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tcminv_purchase_order_po_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tcminv_purchase_order_po_id_seq OWNER TO postgres;

--
-- Name: tcminv_purchase_order_po_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tcminv_purchase_order_po_id_seq OWNED BY tcminv_purchase_order.po_id;


--
-- Name: tcminv_stock_in; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tcminv_stock_in (
    stock_in_id integer NOT NULL,
    qty_in real NOT NULL,
    item_id integer NOT NULL,
    date_in date NOT NULL,
    create_at timestamp with time zone DEFAULT now(),
    surat_izin_id smallint NOT NULL,
    user_id smallint,
    update_at timestamp with time zone DEFAULT statement_timestamp(),
    unit_cost numeric,
    supplier_id smallint
);


ALTER TABLE public.tcminv_stock_in OWNER TO postgres;

--
-- Name: tcminv_stock_out; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tcminv_stock_out (
    stock_out_id integer NOT NULL,
    item_id integer NOT NULL,
    surat_izin_id smallint NOT NULL,
    date_out date NOT NULL,
    qty_out real NOT NULL,
    create_at timestamp with time zone DEFAULT now() NOT NULL,
    update_at timestamp with time zone DEFAULT statement_timestamp(),
    user_id smallint,
    contractor_id smallint
);


ALTER TABLE public.tcminv_stock_out OWNER TO postgres;

--
-- Name: tcminv_stock_out_stock_out_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tcminv_stock_out_stock_out_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tcminv_stock_out_stock_out_id_seq OWNER TO postgres;

--
-- Name: tcminv_stock_out_stock_out_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tcminv_stock_out_stock_out_id_seq OWNED BY tcminv_stock_out.stock_out_id;


--
-- Name: tcminv_stock_return; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tcminv_stock_return (
    stock_out_id integer DEFAULT nextval('tcminv_stock_out_stock_out_id_seq'::regclass) NOT NULL,
    item_id integer NOT NULL,
    surat_izin_id smallint NOT NULL,
    date_return date NOT NULL,
    qty_return real NOT NULL,
    create_at timestamp(6) with time zone DEFAULT now() NOT NULL,
    update_at timestamp(6) with time zone DEFAULT statement_timestamp(),
    user_id smallint,
    contractor_id smallint
);


ALTER TABLE public.tcminv_stock_return OWNER TO postgres;

--
-- Name: tcminv_stock_stock_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tcminv_stock_stock_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tcminv_stock_stock_id_seq OWNER TO postgres;

--
-- Name: tcminv_stock_stock_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tcminv_stock_stock_id_seq OWNED BY tcminv_stock_in.stock_in_id;


--
-- Name: tcminv_supplier; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tcminv_supplier (
    supplier_id integer NOT NULL,
    supplier_name character varying(255),
    supplier_details text,
    create_at timestamp with time zone DEFAULT now()
);


ALTER TABLE public.tcminv_supplier OWNER TO postgres;

--
-- Name: tcminv_supplier_supplier_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tcminv_supplier_supplier_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tcminv_supplier_supplier_id_seq OWNER TO postgres;

--
-- Name: tcminv_supplier_supplier_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tcminv_supplier_supplier_id_seq OWNED BY tcminv_supplier.supplier_id;


--
-- Name: tcminv_surat_izin; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tcminv_surat_izin (
    surat_izin_id integer NOT NULL,
    surat_izin character varying(255),
    surat_izin_desc text,
    create_at timestamp with time zone DEFAULT now(),
    date_min date,
    date_max date,
    user_id smallint,
    update_at timestamp with time zone DEFAULT statement_timestamp()
);


ALTER TABLE public.tcminv_surat_izin OWNER TO postgres;

--
-- Name: tcminv_surat_izin_surat_izin_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tcminv_surat_izin_surat_izin_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tcminv_surat_izin_surat_izin_id_seq OWNER TO postgres;

--
-- Name: tcminv_surat_izin_surat_izin_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tcminv_surat_izin_surat_izin_id_seq OWNED BY tcminv_surat_izin.surat_izin_id;


--
-- Name: tcminv_user; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tcminv_user (
    user_id integer NOT NULL,
    username character varying(255),
    fullname character varying(500),
    email character varying(255),
    address character varying(255),
    phone character varying(255),
    phone2 character varying(255),
    contractor_id integer,
    create_at timestamp with time zone DEFAULT now(),
    update_at timestamp with time zone DEFAULT statement_timestamp(),
    level_id smallint
);


ALTER TABLE public.tcminv_user OWNER TO postgres;

--
-- Name: tcminv_user_log; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tcminv_user_log (
    log_id bigint NOT NULL,
    user_id integer,
    login timestamp with time zone,
    logout timestamp with time zone
);


ALTER TABLE public.tcminv_user_log OWNER TO postgres;

--
-- Name: tcminv_user_log_log_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tcminv_user_log_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tcminv_user_log_log_id_seq OWNER TO postgres;

--
-- Name: tcminv_user_log_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tcminv_user_log_log_id_seq OWNED BY tcminv_user_log.log_id;


--
-- Name: tcminv_user_user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tcminv_user_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tcminv_user_user_id_seq OWNER TO postgres;

--
-- Name: tcminv_user_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tcminv_user_user_id_seq OWNED BY tcminv_user.user_id;


--
-- Name: category_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tcminv_category ALTER COLUMN category_id SET DEFAULT nextval('tcminv_category_category_id_seq'::regclass);


--
-- Name: contractor_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tcminv_contractor ALTER COLUMN contractor_id SET DEFAULT nextval('tcminv_contractor_contractor_id_seq'::regclass);


--
-- Name: item_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tcminv_item ALTER COLUMN item_id SET DEFAULT nextval('tcm_item_item_id_seq'::regclass);


--
-- Name: po_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tcminv_purchase_order ALTER COLUMN po_id SET DEFAULT nextval('tcminv_purchase_order_po_id_seq'::regclass);


--
-- Name: qty_unit_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tcminv_qty_unit ALTER COLUMN qty_unit_id SET DEFAULT nextval('tcm_qty_unit_qty_unit_id_seq'::regclass);


--
-- Name: stock_in_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tcminv_stock_in ALTER COLUMN stock_in_id SET DEFAULT nextval('tcminv_stock_stock_id_seq'::regclass);


--
-- Name: stock_out_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tcminv_stock_out ALTER COLUMN stock_out_id SET DEFAULT nextval('tcminv_stock_out_stock_out_id_seq'::regclass);


--
-- Name: supplier_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tcminv_supplier ALTER COLUMN supplier_id SET DEFAULT nextval('tcminv_supplier_supplier_id_seq'::regclass);


--
-- Name: surat_izin_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tcminv_surat_izin ALTER COLUMN surat_izin_id SET DEFAULT nextval('tcminv_surat_izin_surat_izin_id_seq'::regclass);


--
-- Name: user_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tcminv_user ALTER COLUMN user_id SET DEFAULT nextval('tcminv_user_user_id_seq'::regclass);


--
-- Name: level_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tcminv_user_level ALTER COLUMN level_id SET DEFAULT nextval('tcminv_level_level_id_seq'::regclass);


--
-- Name: log_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tcminv_user_log ALTER COLUMN log_id SET DEFAULT nextval('tcminv_user_log_log_id_seq'::regclass);


--
-- Name: tcm_item_item_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tcm_item_item_id_seq', 19, true);


--
-- Name: tcm_qty_unit_qty_unit_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tcm_qty_unit_qty_unit_id_seq', 2, true);


--
-- Data for Name: tcminv_category; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tcminv_category (category_id, category_name, create_at, category_desc, qty_unit_id) FROM stdin;
6	dinamit	2014-06-16 16:31:11.097022+08	bahan peledak	\N
5	detonator	2014-06-16 16:30:43.741295+08	alat pemicu	\N
9	pc	2014-06-19 21:09:03.736856+08	pc component	\N
\.


--
-- Name: tcminv_category_category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tcminv_category_category_id_seq', 9, true);


--
-- Data for Name: tcminv_contractor; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tcminv_contractor (contractor_id, contractor_name, contractor_desc, create_at) FROM stdin;
1	PAMA	PAMA contractor	2014-06-16 20:29:37.53725+08
2	BAS	BAS	2014-06-16 20:29:37.53725+08
\.


--
-- Name: tcminv_contractor_contractor_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tcminv_contractor_contractor_id_seq', 2, true);


--
-- Data for Name: tcminv_item; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tcminv_item (item_id, item_name, item_code, qty, qty_unit_id, item_desc, create_at, update_at, user_id, category_id) FROM stdin;
8	Connectadet 12 M 25 MS	S5010062	26	2	masuk 72	2014-06-04 15:34:15.378958+08	\N	\N	6
7	Anzomex	S5010013	99	1	72 masuks	2014-06-04 15:34:00.634844+08	\N	\N	6
18	power supply	123123	\N	2	psu 350W	2014-06-19 21:14:12.500643+08	2014-06-19 21:14:12.500643+08	\N	9
19	intel core i5-4300U	srz13	\N	2	intel processor	2014-06-19 21:15:41.09404+08	2014-06-19 21:15:41.09404+08	\N	9
3	POWERGEL MAGNUM 50mm X 400mm	S5060011	34	1	\N	2014-06-04 15:30:46.745681+08	\N	\N	6
1	AMMONIUM NITRATE	S5010031	55	1	\N	2014-06-04 15:29:34.432644+08	\N	\N	6
5	EXEL CONNECTADETE 6 MD 12	S5010016	54	2	\N	2014-06-04 15:32:39.381951+08	\N	\N	5
9	DYNADET C2 INSTANT 4M	S5010063	5	1	\N	2014-06-04 15:34:49.276431+08	\N	\N	5
4	SUPER POWER 0.5 Kg	S5060019	23	1	\N	2014-06-04 15:31:14.382921+08	\N	\N	6
2	EMULSIFIER CLARIANT	S5020015	2	1	\N	2014-06-04 15:30:31.421065+08	\N	\N	5
10	Electric Detonator	S5010063	21	2	\N	2014-06-04 15:35:30.590601+08	\N	\N	6
6	RAYDET DTH 9 M 500 MS	S5010041	51	2	\N	2014-06-04 15:33:19.426326+08	\N	\N	5
\.


--
-- Name: tcminv_level_level_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tcminv_level_level_id_seq', 1, false);


--
-- Data for Name: tcminv_purchase_order; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tcminv_purchase_order (po_id, po_name, po_number, po_value, supplier_id, create_at) FROM stdin;
\.


--
-- Name: tcminv_purchase_order_po_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tcminv_purchase_order_po_id_seq', 1, false);


--
-- Data for Name: tcminv_qty_unit; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tcminv_qty_unit (qty_unit_id, qty_unit, description) FROM stdin;
2	pcs	piece
1	kg	kilogram
\.


--
-- Data for Name: tcminv_stock_in; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tcminv_stock_in (stock_in_id, qty_in, item_id, date_in, create_at, surat_izin_id, user_id, update_at, unit_cost, supplier_id) FROM stdin;
67	2	1	2014-06-19	2014-06-19 19:58:37.494036+08	2	\N	2014-06-19 19:58:37.494036+08	\N	\N
76	2	1	2014-06-20	2014-06-19 20:14:00.820246+08	2	\N	2014-06-19 20:14:00.820246+08	\N	\N
77	1	2	2014-06-18	2014-06-19 20:14:14.103169+08	2	\N	2014-06-19 20:14:14.103169+08	\N	\N
78	1	3	2014-06-19	2014-06-19 20:14:30.630237+08	2	\N	2014-06-19 20:14:30.630237+08	\N	\N
79	0	18	2014-06-19	2014-06-19 21:14:12.508047+08	1	\N	2014-06-19 21:14:12.508047+08	\N	\N
80	0	18	2014-06-19	2014-06-19 21:14:12.539589+08	2	\N	2014-06-19 21:14:12.539589+08	\N	\N
81	0	19	2014-06-19	2014-06-19 21:15:41.205538+08	1	\N	2014-06-19 21:15:41.205538+08	\N	\N
82	0	19	2014-06-19	2014-06-19 21:15:41.371069+08	2	\N	2014-06-19 21:15:41.371069+08	\N	\N
83	1	18	2014-06-19	2014-06-19 21:19:29.106921+08	2	\N	2014-06-19 21:19:29.106921+08	\N	\N
84	1	19	2014-06-19	2014-06-19 21:19:29.218848+08	2	\N	2014-06-19 21:19:29.218848+08	\N	\N
85	2	18	2014-06-20	2014-06-19 22:00:29.216387+08	2	\N	2014-06-19 22:00:29.216387+08	\N	\N
86	3	19	2014-06-20	2014-06-19 22:00:29.341415+08	2	\N	2014-06-19 22:00:29.341415+08	\N	\N
37	656	1	2014-06-17	\N	2	\N	2014-06-18 23:30:19.678025+08	\N	\N
38	66	3	2014-06-17	\N	2	\N	2014-06-18 23:30:19.689133+08	\N	\N
39	35	5	2014-06-17	\N	2	\N	2014-06-18 23:30:19.700203+08	\N	\N
40	531	6	2014-06-17	\N	2	\N	2014-06-18 23:30:19.711302+08	\N	\N
41	343	9	2014-06-17	\N	2	\N	2014-06-18 23:30:19.722554+08	\N	\N
42	99	10	2014-06-17	\N	2	\N	2014-06-18 23:30:19.733497+08	\N	\N
43	123	2	2014-06-17	\N	2	\N	2014-06-18 23:30:19.744619+08	\N	\N
44	545	4	2014-06-17	\N	2	\N	2014-06-18 23:30:19.755689+08	\N	\N
45	91	8	2014-06-17	\N	2	\N	2014-06-18 23:30:19.767095+08	\N	\N
46	10	7	2014-06-17	\N	2	\N	2014-06-18 23:30:19.777926+08	\N	\N
27	1	1	2014-06-17	\N	1	\N	2014-06-18 23:30:19.563379+08	\N	\N
28	3	3	2014-06-17	\N	1	\N	2014-06-18 23:30:19.578398+08	\N	\N
47	1	5	2014-06-17	2014-06-19 12:44:18.077755+08	2	\N	2014-06-19 12:44:18.077755+08	\N	\N
48	4	8	2014-06-17	2014-06-19 12:44:18.108175+08	2	\N	2014-06-19 12:44:18.108175+08	\N	\N
49	11	6	2014-06-17	2014-06-19 12:44:18.118596+08	2	\N	2014-06-19 12:44:18.118596+08	\N	\N
50	12	3	2014-06-17	2014-06-19 12:44:18.130056+08	2	\N	2014-06-19 12:44:18.130056+08	\N	\N
51	13	4	2014-06-17	2014-06-19 12:44:18.141345+08	2	\N	2014-06-19 12:44:18.141345+08	\N	\N
52	14	7	2014-06-17	2014-06-19 12:44:18.152569+08	2	\N	2014-06-19 12:44:18.152569+08	\N	\N
53	15	10	2014-06-17	2014-06-19 12:44:18.163905+08	2	\N	2014-06-19 12:44:18.163905+08	\N	\N
54	16	1	2014-06-17	2014-06-19 12:44:18.175009+08	2	\N	2014-06-19 12:44:18.175009+08	\N	\N
55	17	9	2014-06-17	2014-06-19 12:44:18.185954+08	2	\N	2014-06-19 12:44:18.185954+08	\N	\N
56	18	2	2014-06-17	2014-06-19 12:44:18.197102+08	2	\N	2014-06-19 12:44:18.197102+08	\N	\N
57	1	5	2014-06-17	2014-06-19 12:47:32.190507+08	2	\N	2014-06-19 12:47:32.190507+08	\N	\N
58	1	8	2014-06-17	2014-06-19 12:47:32.303124+08	2	\N	2014-06-19 12:47:32.303124+08	\N	\N
59	1	5	2014-06-17	2014-06-19 12:54:40.321602+08	2	\N	2014-06-19 12:54:40.321602+08	\N	\N
60	1	8	2014-06-17	2014-06-19 12:54:40.425424+08	2	\N	2014-06-19 12:54:40.425424+08	\N	\N
61	1	5	2014-06-17	2014-06-19 12:55:53.228695+08	2	\N	2014-06-19 12:55:53.228695+08	\N	\N
62	1	5	2014-06-17	2014-06-19 12:56:32.607434+08	2	\N	2014-06-19 12:56:32.607434+08	\N	\N
63	3	8	2014-06-17	2014-06-19 13:11:03.436998+08	2	\N	2014-06-19 13:11:03.436998+08	\N	\N
64	1	5	2014-06-17	2014-06-19 13:11:44.127195+08	2	\N	2014-06-19 13:11:44.127195+08	\N	\N
65	2	5	2014-06-17	2014-06-19 19:13:31.420569+08	1	\N	2014-06-19 19:13:31.420569+08	\N	\N
66	2	5	2014-06-17	2014-06-19 19:13:43.313181+08	2	\N	2014-06-19 19:13:43.313181+08	\N	\N
68	2	1	2014-06-17	2014-06-19 19:58:44.684117+08	2	\N	2014-06-19 19:58:44.684117+08	\N	\N
69	4	1	2014-06-17	2014-06-19 19:58:49.252565+08	2	\N	2014-06-19 19:58:49.252565+08	\N	\N
70	1	2	2014-06-17	2014-06-19 20:03:47.082762+08	2	\N	2014-06-19 20:03:47.082762+08	\N	\N
71	2	3	2014-06-17	2014-06-19 20:03:47.336177+08	2	\N	2014-06-19 20:03:47.336177+08	\N	\N
72	2	4	2014-06-17	2014-06-19 20:03:47.558133+08	2	\N	2014-06-19 20:03:47.558133+08	\N	\N
73	2	5	2014-06-17	2014-06-19 20:03:47.602553+08	2	\N	2014-06-19 20:03:47.602553+08	\N	\N
74	1	7	2014-06-17	2014-06-19 20:03:47.624332+08	2	\N	2014-06-19 20:03:47.624332+08	\N	\N
75	1	10	2014-06-17	2014-06-19 20:03:47.646673+08	2	\N	2014-06-19 20:03:47.646673+08	\N	\N
87	5	18	2014-06-22	2014-06-23 23:07:33.806178+08	2	\N	2014-06-23 23:07:33.806178+08	\N	\N
88	5	19	2014-06-22	2014-06-23 23:07:34.116214+08	2	\N	2014-06-23 23:07:34.116214+08	\N	\N
\.


--
-- Data for Name: tcminv_stock_out; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tcminv_stock_out (stock_out_id, item_id, surat_izin_id, date_out, qty_out, create_at, update_at, user_id, contractor_id) FROM stdin;
40	1	2	2014-06-19	2	2014-06-19 19:14:25.699526+08	2014-06-19 19:14:25.699526+08	\N	\N
41	2	2	2014-06-19	2	2014-06-19 20:22:13.341279+08	2014-06-19 20:22:13.341279+08	\N	\N
42	1	2	2014-06-19	1	2014-06-19 20:42:15.245577+08	2014-06-19 20:42:15.245577+08	\N	\N
43	18	2	2014-06-19	1	2014-06-19 21:19:46.217117+08	2014-06-19 21:19:46.217117+08	\N	\N
44	19	2	2014-06-19	1	2014-06-19 21:19:46.328794+08	2014-06-19 21:19:46.328794+08	\N	\N
45	1	2	2014-06-19	1	2014-06-19 21:41:09.442935+08	2014-06-19 21:41:09.442935+08	\N	\N
46	1	2	2014-06-19	1	2014-06-19 21:41:18.771881+08	2014-06-19 21:41:18.771881+08	\N	\N
47	18	2	2014-06-20	1	2014-06-19 22:14:05.69649+08	2014-06-19 22:14:05.69649+08	\N	\N
48	19	2	2014-06-20	1	2014-06-19 22:14:05.809079+08	2014-06-19 22:14:05.809079+08	\N	\N
25	5	2	2014-06-17	1	2014-06-19 15:06:57.691315+08	2014-06-19 15:06:57.691315+08	\N	\N
26	8	2	2014-06-17	2	2014-06-19 15:06:57.808012+08	2014-06-19 15:06:57.808012+08	\N	\N
27	6	2	2014-06-17	3	2014-06-19 15:06:57.896842+08	2014-06-19 15:06:57.896842+08	\N	\N
28	3	2	2014-06-17	4	2014-06-19 15:06:58.029617+08	2014-06-19 15:06:58.029617+08	\N	\N
29	4	2	2014-06-17	5	2014-06-19 15:06:58.040563+08	2014-06-19 15:06:58.040563+08	\N	\N
30	7	2	2014-06-17	6	2014-06-19 15:06:58.051898+08	2014-06-19 15:06:58.051898+08	\N	\N
31	10	2	2014-06-17	7	2014-06-19 15:06:58.062024+08	2014-06-19 15:06:58.062024+08	\N	\N
32	1	2	2014-06-17	8	2014-06-19 15:06:58.073724+08	2014-06-19 15:06:58.073724+08	\N	\N
33	9	2	2014-06-17	9	2014-06-19 15:06:58.084799+08	2014-06-19 15:06:58.084799+08	\N	\N
34	2	2	2014-06-17	10	2014-06-19 15:06:58.096006+08	2014-06-19 15:06:58.096006+08	\N	\N
35	1	2	2014-06-17	1	2014-06-19 19:06:31.310221+08	2014-06-19 19:06:31.310221+08	\N	\N
36	1	2	2014-06-17	1	2014-06-19 19:06:42.408152+08	2014-06-19 19:06:42.408152+08	\N	\N
37	2	2	2014-06-17	11	2014-06-19 19:07:20.826429+08	2014-06-19 19:07:20.826429+08	\N	\N
38	3	2	2014-06-17	1	2014-06-19 19:07:25.709364+08	2014-06-19 19:07:25.709364+08	\N	\N
39	6	2	2014-06-17	9	2014-06-19 19:08:19.834543+08	2014-06-19 19:08:19.834543+08	\N	\N
49	18	2	2014-06-23	1	2014-06-23 23:05:47.687337+08	2014-06-23 23:05:47.687337+08	\N	\N
50	19	2	2014-06-23	2	2014-06-23 23:05:47.847529+08	2014-06-23 23:05:47.847529+08	\N	\N
51	18	2	2014-06-22	4	2014-06-23 23:07:48.858738+08	2014-06-23 23:07:48.858738+08	\N	\N
52	19	2	2014-06-22	4	2014-06-23 23:07:48.996219+08	2014-06-23 23:07:48.996219+08	\N	\N
53	1	2	2014-06-24	2	2014-06-24 13:44:27.492383+08	2014-06-24 13:44:27.492383+08	\N	\N
54	18	2	2014-06-24	1	2014-06-24 13:44:27.861439+08	2014-06-24 13:44:27.861439+08	\N	\N
55	19	2	2014-06-24	1	2014-06-24 13:44:27.869749+08	2014-06-24 13:44:27.869749+08	\N	\N
56	2	2	2014-06-24	1	2014-06-24 13:45:17.928812+08	2014-06-24 13:45:17.928812+08	\N	1
57	4	2	2014-06-24	5	2014-06-24 13:45:18.041516+08	2014-06-24 13:45:18.041516+08	\N	1
61	1	2	2014-06-27	2	2014-07-11 15:34:03.758432+08	2014-07-11 15:34:03.758432+08	\N	1
62	2	2	2014-06-27	10	2014-07-11 15:34:04.3577+08	2014-07-11 15:34:04.3577+08	\N	1
63	3	2	2014-06-27	5	2014-07-11 15:34:04.371824+08	2014-07-11 15:34:04.371824+08	\N	1
64	4	2	2014-06-27	6	2014-07-11 15:34:04.382956+08	2014-07-11 15:34:04.382956+08	\N	1
65	5	2	2014-06-27	12	2014-07-11 15:34:04.39411+08	2014-07-11 15:34:04.39411+08	\N	1
\.


--
-- Name: tcminv_stock_out_stock_out_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tcminv_stock_out_stock_out_id_seq', 76, true);


--
-- Data for Name: tcminv_stock_return; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tcminv_stock_return (stock_out_id, item_id, surat_izin_id, date_return, qty_return, create_at, update_at, user_id, contractor_id) FROM stdin;
40	1	2	2014-06-19	2	2014-06-19 19:14:25.699526+08	2014-06-19 19:14:25.699526+08	\N	\N
41	2	2	2014-06-19	2	2014-06-19 20:22:13.341279+08	2014-06-19 20:22:13.341279+08	\N	\N
42	1	2	2014-06-19	1	2014-06-19 20:42:15.245577+08	2014-06-19 20:42:15.245577+08	\N	\N
43	18	2	2014-06-19	1	2014-06-19 21:19:46.217117+08	2014-06-19 21:19:46.217117+08	\N	\N
44	19	2	2014-06-19	1	2014-06-19 21:19:46.328794+08	2014-06-19 21:19:46.328794+08	\N	\N
45	1	2	2014-06-19	1	2014-06-19 21:41:09.442935+08	2014-06-19 21:41:09.442935+08	\N	\N
46	1	2	2014-06-19	1	2014-06-19 21:41:18.771881+08	2014-06-19 21:41:18.771881+08	\N	\N
47	18	2	2014-06-20	1	2014-06-19 22:14:05.69649+08	2014-06-19 22:14:05.69649+08	\N	\N
48	19	2	2014-06-20	1	2014-06-19 22:14:05.809079+08	2014-06-19 22:14:05.809079+08	\N	\N
25	5	2	2014-06-17	1	2014-06-19 15:06:57.691315+08	2014-06-19 15:06:57.691315+08	\N	\N
26	8	2	2014-06-17	2	2014-06-19 15:06:57.808012+08	2014-06-19 15:06:57.808012+08	\N	\N
27	6	2	2014-06-17	3	2014-06-19 15:06:57.896842+08	2014-06-19 15:06:57.896842+08	\N	\N
28	3	2	2014-06-17	4	2014-06-19 15:06:58.029617+08	2014-06-19 15:06:58.029617+08	\N	\N
29	4	2	2014-06-17	5	2014-06-19 15:06:58.040563+08	2014-06-19 15:06:58.040563+08	\N	\N
30	7	2	2014-06-17	6	2014-06-19 15:06:58.051898+08	2014-06-19 15:06:58.051898+08	\N	\N
31	10	2	2014-06-17	7	2014-06-19 15:06:58.062024+08	2014-06-19 15:06:58.062024+08	\N	\N
32	1	2	2014-06-17	8	2014-06-19 15:06:58.073724+08	2014-06-19 15:06:58.073724+08	\N	\N
33	9	2	2014-06-17	9	2014-06-19 15:06:58.084799+08	2014-06-19 15:06:58.084799+08	\N	\N
34	2	2	2014-06-17	10	2014-06-19 15:06:58.096006+08	2014-06-19 15:06:58.096006+08	\N	\N
35	1	2	2014-06-17	1	2014-06-19 19:06:31.310221+08	2014-06-19 19:06:31.310221+08	\N	\N
36	1	2	2014-06-17	1	2014-06-19 19:06:42.408152+08	2014-06-19 19:06:42.408152+08	\N	\N
37	2	2	2014-06-17	11	2014-06-19 19:07:20.826429+08	2014-06-19 19:07:20.826429+08	\N	\N
38	3	2	2014-06-17	1	2014-06-19 19:07:25.709364+08	2014-06-19 19:07:25.709364+08	\N	\N
39	6	2	2014-06-17	9	2014-06-19 19:08:19.834543+08	2014-06-19 19:08:19.834543+08	\N	\N
49	18	2	2014-06-23	1	2014-06-23 23:05:47.687337+08	2014-06-23 23:05:47.687337+08	\N	\N
50	19	2	2014-06-23	2	2014-06-23 23:05:47.847529+08	2014-06-23 23:05:47.847529+08	\N	\N
51	18	2	2014-06-22	4	2014-06-23 23:07:48.858738+08	2014-06-23 23:07:48.858738+08	\N	\N
52	19	2	2014-06-22	4	2014-06-23 23:07:48.996219+08	2014-06-23 23:07:48.996219+08	\N	\N
53	1	2	2014-06-24	2	2014-06-24 13:44:27.492383+08	2014-06-24 13:44:27.492383+08	\N	\N
54	18	2	2014-06-24	1	2014-06-24 13:44:27.861439+08	2014-06-24 13:44:27.861439+08	\N	\N
55	19	2	2014-06-24	1	2014-06-24 13:44:27.869749+08	2014-06-24 13:44:27.869749+08	\N	\N
56	2	2	2014-06-24	1	2014-06-24 13:45:17.928812+08	2014-06-24 13:45:17.928812+08	\N	1
57	4	2	2014-06-24	5	2014-06-24 13:45:18.041516+08	2014-06-24 13:45:18.041516+08	\N	1
59	1	2	2014-06-25	1	2014-06-25 16:07:14.288792+08	2014-06-25 16:07:14.288792+08	\N	1
60	2	2	2014-06-25	1	2014-06-25 16:23:13.188348+08	2014-06-25 16:23:13.188348+08	\N	1
71	1	2	2014-06-27	1	2014-07-14 11:48:50.981747+08	2014-07-14 11:48:50.981747+08	\N	1
72	2	2	2014-06-27	1	2014-07-14 11:48:51.064354+08	2014-07-14 11:48:51.064354+08	\N	1
73	3	2	2014-06-27	1	2014-07-14 11:48:51.174373+08	2014-07-14 11:48:51.174373+08	\N	1
74	4	2	2014-06-27	1	2014-07-14 11:48:51.184608+08	2014-07-14 11:48:51.184608+08	\N	1
75	5	2	2014-06-27	1	2014-07-14 11:48:51.195696+08	2014-07-14 11:48:51.195696+08	\N	1
76	6	2	2014-06-27	1	2014-07-14 11:48:51.207154+08	2014-07-14 11:48:51.207154+08	\N	1
\.


--
-- Name: tcminv_stock_stock_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tcminv_stock_stock_id_seq', 88, true);


--
-- Data for Name: tcminv_supplier; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tcminv_supplier (supplier_id, supplier_name, supplier_details, create_at) FROM stdin;
\.


--
-- Name: tcminv_supplier_supplier_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tcminv_supplier_supplier_id_seq', 1, false);


--
-- Data for Name: tcminv_surat_izin; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tcminv_surat_izin (surat_izin_id, surat_izin, surat_izin_desc, create_at, date_min, date_max, user_id, update_at) FROM stdin;
1	SI 7980	SI/7899/X/2013 Ex SI/2906/IV/2014	2014-06-16 18:43:57.706294+08	2014-05-08	2014-06-04	\N	2014-06-16 18:43:57.706294+08
2	SI 7972	Surat Izin Control	2014-06-16 19:00:02.370814+08	2014-06-05	2014-08-31	\N	2014-06-16 19:00:02.370814+08
\.


--
-- Name: tcminv_surat_izin_surat_izin_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tcminv_surat_izin_surat_izin_id_seq', 2, true);


--
-- Data for Name: tcminv_user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tcminv_user (user_id, username, fullname, email, address, phone, phone2, contractor_id, create_at, update_at, level_id) FROM stdin;
\.


--
-- Data for Name: tcminv_user_level; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tcminv_user_level (level_id, level) FROM stdin;
\.


--
-- Data for Name: tcminv_user_log; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tcminv_user_log (log_id, user_id, login, logout) FROM stdin;
\.


--
-- Name: tcminv_user_log_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tcminv_user_log_log_id_seq', 1, false);


--
-- Name: tcminv_user_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tcminv_user_user_id_seq', 1, false);


--
-- Name: tcm_item2_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tcminv_item
    ADD CONSTRAINT tcm_item2_pkey PRIMARY KEY (item_id);


--
-- Name: tcm_qty_unit_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tcminv_qty_unit
    ADD CONSTRAINT tcm_qty_unit_pkey PRIMARY KEY (qty_unit_id);


--
-- Name: tcminv_category_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tcminv_category
    ADD CONSTRAINT tcminv_category_pkey PRIMARY KEY (category_id);


--
-- Name: tcminv_contractor_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tcminv_contractor
    ADD CONSTRAINT tcminv_contractor_pkey PRIMARY KEY (contractor_id);


--
-- Name: tcminv_level_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tcminv_user_level
    ADD CONSTRAINT tcminv_level_pkey PRIMARY KEY (level_id);


--
-- Name: tcminv_purchase_order_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tcminv_purchase_order
    ADD CONSTRAINT tcminv_purchase_order_pkey PRIMARY KEY (po_id);


--
-- Name: tcminv_stock_out_copy_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tcminv_stock_return
    ADD CONSTRAINT tcminv_stock_out_copy_pkey PRIMARY KEY (stock_out_id);


--
-- Name: tcminv_stock_out_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tcminv_stock_out
    ADD CONSTRAINT tcminv_stock_out_pkey PRIMARY KEY (stock_out_id);


--
-- Name: tcminv_stock_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tcminv_stock_in
    ADD CONSTRAINT tcminv_stock_pkey PRIMARY KEY (stock_in_id);


--
-- Name: tcminv_supplier_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tcminv_supplier
    ADD CONSTRAINT tcminv_supplier_pkey PRIMARY KEY (supplier_id);


--
-- Name: tcminv_surat_izin_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tcminv_surat_izin
    ADD CONSTRAINT tcminv_surat_izin_pkey PRIMARY KEY (surat_izin_id);


--
-- Name: tcminv_user_log_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tcminv_user_log
    ADD CONSTRAINT tcminv_user_log_pkey PRIMARY KEY (log_id);


--
-- Name: tcminv_user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tcminv_user
    ADD CONSTRAINT tcminv_user_pkey PRIMARY KEY (user_id);


--
-- Name: category; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tcminv_item
    ADD CONSTRAINT category FOREIGN KEY (category_id) REFERENCES tcminv_category(category_id);


--
-- Name: contractor; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tcminv_stock_out
    ADD CONSTRAINT contractor FOREIGN KEY (contractor_id) REFERENCES tcminv_contractor(contractor_id);


--
-- Name: item; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tcminv_stock_in
    ADD CONSTRAINT item FOREIGN KEY (item_id) REFERENCES tcminv_item(item_id);


--
-- Name: item; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tcminv_stock_out
    ADD CONSTRAINT item FOREIGN KEY (item_id) REFERENCES tcminv_item(item_id);


--
-- Name: qty_unit; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tcminv_item
    ADD CONSTRAINT qty_unit FOREIGN KEY (qty_unit_id) REFERENCES tcminv_qty_unit(qty_unit_id);


--
-- Name: qty_unit; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tcminv_category
    ADD CONSTRAINT qty_unit FOREIGN KEY (qty_unit_id) REFERENCES tcminv_qty_unit(qty_unit_id);


--
-- Name: supplier; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tcminv_stock_in
    ADD CONSTRAINT supplier FOREIGN KEY (supplier_id) REFERENCES tcminv_supplier(supplier_id);


--
-- Name: surat_izin; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tcminv_stock_in
    ADD CONSTRAINT surat_izin FOREIGN KEY (surat_izin_id) REFERENCES tcminv_surat_izin(surat_izin_id);


--
-- Name: surat_izin; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tcminv_stock_out
    ADD CONSTRAINT surat_izin FOREIGN KEY (surat_izin_id) REFERENCES tcminv_surat_izin(surat_izin_id);


--
-- Name: tcminv_stock_return_copy_contractor_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tcminv_stock_return
    ADD CONSTRAINT tcminv_stock_return_copy_contractor_id_fkey FOREIGN KEY (contractor_id) REFERENCES tcminv_contractor(contractor_id);


--
-- Name: tcminv_stock_return_copy_item_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tcminv_stock_return
    ADD CONSTRAINT tcminv_stock_return_copy_item_id_fkey FOREIGN KEY (item_id) REFERENCES tcminv_item(item_id);


--
-- Name: tcminv_stock_return_copy_surat_izin_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tcminv_stock_return
    ADD CONSTRAINT tcminv_stock_return_copy_surat_izin_id_fkey FOREIGN KEY (surat_izin_id) REFERENCES tcminv_surat_izin(surat_izin_id);


--
-- Name: user; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tcminv_item
    ADD CONSTRAINT "user" FOREIGN KEY (user_id) REFERENCES tcminv_user(user_id);


--
-- Name: user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tcminv_user_log
    ADD CONSTRAINT user_id FOREIGN KEY (user_id) REFERENCES tcminv_user(user_id);


--
-- Name: user_level; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tcminv_user
    ADD CONSTRAINT user_level FOREIGN KEY (level_id) REFERENCES tcminv_user_level(level_id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--


CREATE TABLE "public"."tcm_item" (
"item_id" serial4 NOT NULL,
"name" varchar(255) COLLATE "default",
"item_code" varchar(255) COLLATE "default",
"surat_izin" int2,
"qty" int2,
"qty_unit_id" int2,
"description" varchar(255) COLLATE "default",
"create_at" timestamptz(6) DEFAULT now(),
"update_at" timestamptz(6),
"user_id" int4,
CONSTRAINT "tcm_item2_pkey" PRIMARY KEY ("item_id")
);


SELECT ti.item_id,ti.item_name,SUM(qty_in) as sum_qty_in, SUM(qty_in) - SUM(qty_in) qty_av, SUM(qty_out) as sum_qty_out, sti.surat_izin_id, si.surat_izin
FROM "tcminv_item"  ti
INNER JOIN "tcminv_stock_in"  sti ON sti."item_id" = ti."item_id" 
INNER JOIN "tcminv_surat_izin" si ON si."surat_izin_id" = sti."surat_izin_id"
LEFT JOIN tcminv_stock_out sto ON sto.item_id = ti.item_id
WHERE sti."surat_izin_id" = '2'  AND sto.surat_izin_id = '2'
GROUP BY ti.item_id, sti.surat_izin_id, sto.surat_izin_id, si.surat_izin	
ORDER BY ti.item_id;

 SELECT "tcminv_stock_in"."item_id", MIN("item_name"), MIN("item_code"), SUM(qty_in) as sum_qty_in, SUM(qty_out) as sum_qty_out, SUM(qty_in) - SUM(qty_in) qty_av, 
"tcminv_stock_in"."surat_izin_id", "tcminv_surat_izin"."surat_izin" 
FROM "tcminv_item" 
INNER JOIN "tcminv_stock_in" ON "tcminv_stock_in"."item_id" = "tcminv_item"."item_id" 
INNER JOIN "tcminv_surat_izin" ON "tcminv_surat_izin"."surat_izin_id" = "tcminv_stock_in"."surat_izin_id" 
LEFT JOIN "tcminv_stock_out" ON "tcminv_stock_out"."item_id" = "tcminv_stock_out"."item_id" 
WHERE "tcminv_stock_in"."surat_izin_id" = '2' 
GROUP BY "tcminv_stock_in"."item_id", "tcminv_stock_in"."surat_izin_id", "tcminv_stock_out"."surat_izin_id", "tcminv_surat_izin"."surat_izin";


SELECT item_id, surat_izin_id, SUM(qty_out) 
FROM tcminv_stock_out
GROUP BY item_id, surat_izin_id
ORDER BY item_id;

SELECT item_id, surat_izin_id, SUM(qty_in) 
FROM tcminv_stock_in
GROUP BY item_id, surat_izin_id
ORDER BY item_id;



SELECT sti.item_id, item_name , item_code, sti.surat_izin_id, surat_izin, sum_qty_in, sum_qty_out, sum_qty_in - sum_qty_out as qty_av,
qty_unit
FROM (
SELECT MIN(ti.item_name) as item_name, MAX(ti.item_code) as item_code, sti.item_id , 
MAX(si.surat_izin) as surat_izin, sti.surat_izin_id , SUM(qty_in) as sum_qty_in,
MAX(qty_unit) as qty_unit
FROM tcminv_stock_in sti
INNER JOIN tcminv_item ti ON sti.item_id = ti.item_id
INNER JOIN tcminv_qty_unit qu ON ti.qty_unit_id = qu.qty_unit_id
RIGHT JOIN tcminv_surat_izin si ON sti.surat_izin_id = si.surat_izin_id
GROUP BY sti.item_id, sti.surat_izin_id
) sti
LEFT JOIN (
SELECT item_id, surat_izin_id, SUM(qty_out)  as sum_qty_out
FROM tcminv_stock_out
GROUP BY item_id, surat_izin_id
) sto USING ( item_id, surat_izin_id)
ORDER BY item_id, surat_izin_id DESC;


SELECT MIN(ti.item_name) as item_name, MAX(ti.item_code) as item_code, sti.item_id , 
MAX(si.surat_izin) as surat_izin, sti.surat_izin_id , SUM(qty_in) as sum_qty_in,
MAX(qty_unit) as qty_unit
FROM tcminv_stock_in sti
RIGHT JOIN tcminv_item ti ON sti.item_id = ti.item_id
INNER JOIN tcminv_qty_unit qu ON ti.qty_unit_id = qu.qty_unit_id
RIGHT JOIN tcminv_surat_izin si ON sti.surat_izin_id = si.surat_izin_id
GROUP BY sti.item_id, sti.surat_izin_id

SELECT sti.item_id, MAX(sti.surat_izin_id), MAX(sti.qty_in)
-- ,SUM(qty_out)
-- SUM(qty_in) as sum_in, SUM(qty_out) as sum_out
FROM tcminv_stock_in sti
-- INNER JOIN tcminv_stock_out sto ON sti.item_id =  sto.item_id   AND  sto.surat_izin_id = sti.surat_izin_id
INNER JOIN tcminv_stock_out sto on sti.item_id = sto.item_id
GROUP BY sti.item_id, sti.surat_izin_id
ORDER BY sti.item_id;public

-- DELETE FROM tcminv_stock_in WHERE qty_in = 0


 SELECT * FROM "tcminv_surat_izin" WHERE "date_min" <= '2014-06-19' AND "date_max" >= '2014-06-19';



-- Daily Report
SELECT sti.item_id, date_in, SUM(qty_in), string_agg(  to_char(qty_in, '999'),  ', ' )  
FROM  tcminv_item ti
LEFT JOIN tcminv_stock_in sti ON sti.item_id = ti.item_id
GROUP BY sti.item_id, sti.date_in, sti.surat_izin_id
ORDER BY sti.item_id;

select  item_id,qty_in,
    coalesce(to_char(date_in,'YYYY-MM-DD'), '') as A
  from tcminv_stock_in ;

SELECT MIN(stock_in_id), MAX(date_in),item_id, to_char(qty_in, '999')
--string_agg(qty_in, ',')
FROM tcminv_stock_in 
GROUP BY item_id, qty_in;

select * FROM crosstab(
'SELECT item_id, qty_in, date_in 
FROM tcminv_stock_in
GROUP BY 1,2
ORDER BY 1
'
) as qty_in ('Item' text,  '2014-06-20' text, '2014-06-19' text )
;

select  extract(day from DATE '2014-06-20');

select extract(day from  date_in) from tcminv_stock_in;

SELECT item_id,
       SUM(CASE date_in  WHEN '2014-06-20' THEN qty_in ELSE 0 END) AS  "20",
       SUM(CASE date_in   WHEN '2014-06-19' THEN qty_in ELSE 0 END) AS "19"
FROM tcminv_stock_in
GROUP BY item_id
ORDER BY 1;

 SELECT item_id, 
SUM(CASE date_in WHEN '2014-06-17' THEN qty_in ELSE 0 END) AS "2014-06-17",
SUM(CASE date_in WHEN '2014-06-18' THEN qty_in ELSE 0 END) AS "2014-06-18",
SUM(CASE date_in WHEN '2014-06-19' THEN qty_in ELSE 0 END) AS "2014-06-19",
SUM(CASE date_in WHEN '2014-06-20' THEN qty_in ELSE 0 END) AS "2014-06-20" ,
SUM(CASE to_char(date_in,'YY-MM') WHEN '14-06' THEN qty_in ELSE 0 END) AS "total" 
FROM tcminv_stock_in GROUP BY item_id ORDER BY 1;



 SELECT date_in, 
SUM(CASE ti.category_id WHEN '6' THEN qty_in ELSE 0 END) AS "dinamit",
SUM(CASE ti.category_id WHEN '5' THEN qty_in ELSE 0 END) AS "detonator",
SUM(CASE ti.category_id WHEN '9' THEN qty_in ELSE 0 END) AS "pc" 
FROM tcminv_stock_in sti INNER JOIN tcminv_item ti ON ti.item_id = sti.item_id 
INNER JOIN tcminv_category tc ON tc.category_id = ti.category_id 
GROUP BY date_in ORDER BY date_in;


SELECT item_id, SUM(qty_in) , max(date_in) FROM tcminv_stock_in
GROUP BY item_id, date_in
ORDER BY item_id;

SELECT item_id, date_in, qty_in FROM tcminv_stock_in
ORDER BY date_in DESC;

SELECT date_in FROM tcminv_stock_in
GROUP BY date_in 
ORDER BY date_in DESC;

SELECT item_name, qty_in 
FROM  tcminv_item ti
LEFT JOIN tcminv_stock_in sti ON sti.item_id = ti.item_id

UPDATE tcminv_stock_out 
set date_out = '2014-06-17'
WHERE date_out = '1970-01-01'



SELECT MAX(sti.item_id) as item_id, MAX(qty_unit) as qty_unit , MAX(date_in),
SUM(CASE ti.category_id WHEN '6' THEN qty_in ELSE 0 END) AS "dinamit",
SUM(CASE ti.category_id WHEN '5' THEN qty_in ELSE 0 END) AS "detonator",
SUM(CASE ti.category_id WHEN '9' THEN qty_in ELSE 0 END) AS "pc" 
FROM tcminv_stock_in sti 
INNER JOIN tcminv_item ti ON sti.item_id = ti.item_id 
INNER JOIN tcminv_category tc on tc.category_id = ti.category_id
INNER JOIN tcminv_surat_izin si ON sti.surat_izin_id = si.surat_izin_id 
INNER JOIN tcminv_qty_unit qu ON ti.qty_unit_id = qu.qty_unit_id 
WHERE sti.surat_izin_id = 2 GROUP BY date_in 


SELECT MAX(sti.item_id) as item_id, MAX(qty_unit) as qty_unit , MAX(date_in) as date_in,
SUM(CASE ti.category_id WHEN '6' THEN qty_in ELSE 0 END) AS "dinamit_in",
SUM(CASE ti.category_id WHEN '5' THEN qty_in ELSE 0 END) AS "detonator_in",
SUM(CASE ti.category_id WHEN '9' THEN qty_in ELSE 0 END) AS "pc_in" 
FROM tcminv_stock_in sti 
INNER JOIN tcminv_item ti ON sti.item_id = ti.item_id 
INNER JOIN tcminv_category tc on tc.category_id = ti.category_id
INNER JOIN tcminv_surat_izin si ON sti.surat_izin_id = si.surat_izin_id 
INNER JOIN tcminv_qty_unit qu ON ti.qty_unit_id = qu.qty_unit_id 
WHERE sti.surat_izin_id = 2 AND date_in ='2014-06-19'


SELECT date_in,"dinamit_in", "dinamit_out"
FROM ( 
SELECT MAX(sti.item_id) as item_id, MAX(qty_unit) as qty_unit , MAX(date_in) as date_in,
SUM(CASE ti.category_id WHEN '6' THEN qty_in ELSE 0 END) AS "dinamit_in",
SUM(CASE ti.category_id WHEN '5' THEN qty_in ELSE 0 END) AS "detonator_in",
SUM(CASE ti.category_id WHEN '9' THEN qty_in ELSE 0 END) AS "pc_in" 
FROM tcminv_stock_in sti 
INNER JOIN tcminv_item ti ON sti.item_id = ti.item_id 
INNER JOIN tcminv_category tc on tc.category_id = ti.category_id
INNER JOIN tcminv_surat_izin si ON sti.surat_izin_id = si.surat_izin_id 
INNER JOIN tcminv_qty_unit qu ON ti.qty_unit_id = qu.qty_unit_id 
WHERE sti.surat_izin_id = 2 GROUP BY date_in  ) sti 
LEFT JOIN ( 
SELECT date_out , SUM(qty_out) as sum_qty_out , MAX(sto.item_id) as item_id,
SUM(CASE ti.category_id WHEN '6' THEN qty_out ELSE 0 END) AS "dinamit_out",
SUM(CASE ti.category_id WHEN '5' THEN qty_out ELSE 0 END) AS "detonator_out",
SUM(CASE ti.category_id WHEN '9' THEN qty_out ELSE 0 END) AS "pc_out" 
FROM tcminv_stock_out sto
INNER JOIN tcminv_item ti ON sto.item_id = ti.item_id 
INNER JOIN tcminv_category tc on tc.category_id = ti.category_id
WHERE surat_izin_id = 2 GROUP BY date_out ) sto 
ON (date_in = date_out )
LEFT JOIN ( 
SELECT date_return , MAX(rtn.item_id) as item_id , SUM(qty_return) as sum_qty_return ,
SUM(CASE ti.category_id WHEN '6' THEN qty_return ELSE 0 END) AS "dinamit_return",
SUM(CASE ti.category_id WHEN '5' THEN qty_return ELSE 0 END) AS "detonator_return",
SUM(CASE ti.category_id WHEN '9' THEN qty_return ELSE 0 END) AS "pc_return" 
FROM tcminv_stock_return  rtn
INNER JOIN tcminv_item ti ON rtn.item_id = ti.item_id 
INNER JOIN tcminv_category tc on tc.category_id = ti.category_id
WHERE surat_izin_id = 2 
GROUP BY date_return ) rtn
ON (date_in = date_return)
ORDER BY date_in DESC;

SELECT item_id, surat_izin_id, SUM(qty_return) as sum_qty_return FROM tcminv_stock_return WHERE surat_izin_id = 2 GROUP BY item_id, surat_izin_id


SELECT date_out FROM tcminv_stock_out GROUP BY date_out ORDER BY date_out ASC;

- qty_return

SELECT tso.item_id,MAX(item_name) as item_name, MAX(item_code) as item_code, MAX(qty_unit) as qty_unit, 
SUM(CASE date_out WHEN '2014-06-17' THEN qty_out  ELSE 0 END) AS "2014-06-17",
SUM(CASE date_out WHEN '2014-06-19' THEN qty_out ELSE 0 END) AS "2014-06-19",
SUM(CASE date_out WHEN '2014-06-20' THEN qty_out ELSE 0 END) AS "2014-06-20",
SUM(CASE date_out WHEN '2014-06-22' THEN qty_out ELSE 0 END) AS "2014-06-22",
SUM(CASE date_out WHEN '2014-06-23' THEN qty_out ELSE 0 END) AS "2014-06-23",
SUM(CASE date_out WHEN '2014-06-24' THEN qty_out ELSE 0 END) AS "2014-06-24",
SUM(CASE to_char(date_out, 'MM-YY') WHEN '06-14' THEN qty_out ELSE 0 END) AS "Total" 
FROM tcminv_stock_out tso INNER JOIN tcminv_item ti ON ti.item_id = tso.item_id 
INNER JOIN tcminv_qty_unit qu ON qu.qty_unit_id = ti.qty_unit_id 
-- LEFT JOIN tcminv_stock_return tsr ON tsr.item_id = tso.item_id
WHERE tso.surat_izin_id =2 
AND to_char(date_out, 'MM-YY') ='06-14'
-- AND tso.contractor_id ='1' 
 GROUP BY tso.item_id ORDER BY 1;


SELECT sto.item_id,  sum(qty_out) FROM  tcminv_stock_out sto
INNER JOIN tcminv_item ti ON ti.item_id = sto.item_id 
INNER JOIN tcminv_qty_unit qu ON qu.qty_unit_id = ti.qty_unit_id 
WHERE surat_izin_id =2 AND to_char(date_out, 'MM-YY') ='06-14' AND contractor_id ='1' 
AND sto.item_id= 1 AND date_out = '2014-06-17'
GROUP BY sto.item_id;


SELECT item_id, sum(qty_return) FROM  tcminv_stock_return
where item_id= 1 AND date_return = '2014-06-17'
GROUP BY item_id;




-- Query to get stock use
SELECT 
sto.item_id as item_id, item_name, 
COALESCE (sto."2014-06-17" - str."2014-06-17") as sum_use
FROM tcminv_item ti
INNER JOIN (
SELECT item_id,  
SUM(CASE date_out WHEN '2014-06-17' THEN qty_out  ELSE 0 END) AS "2014-06-17"
-- sum(qty_out)  as sum_qty_out 
FROM  tcminv_stock_out 
GROUP BY item_id
) as sto on (sto.item_id = ti.item_id)
LEFT JOIN (
SELECT item_id,
SUM(CASE date_return WHEN '2014-06-17' THEN qty_return  ELSE 0 END) AS "2014-06-17"
 FROM  tcminv_stock_return
GROUP BY item_id
) as str on (sto.item_id = str.item_id)



 SELECT tso.item_id,MAX(item_name) as item_name, MAX(item_code) as item_code, 
MAX(qty_unit) as qty_unit, 
SUM(CASE date_out WHEN '2014-06-01' THEN qty_out ELSE 0 END) AS "2014-06-01", SUM(CASE date_out WHEN '2014-06-02' THEN qty_out ELSE 0 END) AS "2014-06-02", SUM(CASE date_out WHEN '2014-06-03' THEN qty_out ELSE 0 END) AS "2014-06-03", SUM(CASE date_out WHEN '2014-06-04' THEN qty_out ELSE 0 END) AS "2014-06-04", SUM(CASE date_out WHEN '2014-06-05' THEN qty_out ELSE 0 END) AS "2014-06-05", SUM(CASE date_out WHEN '2014-06-06' THEN qty_out ELSE 0 END) AS "2014-06-06", SUM(CASE date_out WHEN '2014-06-07' THEN qty_out ELSE 0 END) AS "2014-06-07", SUM(CASE date_out WHEN '2014-06-08' THEN qty_out ELSE 0 END) AS "2014-06-08", SUM(CASE date_out WHEN '2014-06-09' THEN qty_out ELSE 0 END) AS "2014-06-09", SUM(CASE date_out WHEN '2014-06-10' THEN qty_out ELSE 0 END) AS "2014-06-10", SUM(CASE date_out WHEN '2014-06-11' THEN qty_out ELSE 0 END) AS "2014-06-11", SUM(CASE date_out WHEN '2014-06-12' THEN qty_out ELSE 0 END) AS "2014-06-12", SUM(CASE date_out WHEN '2014-06-13' THEN qty_out ELSE 0 END) AS "2014-06-13", SUM(CASE date_out WHEN '2014-06-14' THEN qty_out ELSE 0 END) AS "2014-06-14", SUM(CASE date_out WHEN '2014-06-15' THEN qty_out ELSE 0 END) AS "2014-06-15", SUM(CASE date_out WHEN '2014-06-16' THEN qty_out ELSE 0 END) AS "2014-06-16", SUM(CASE date_out WHEN '2014-06-17' THEN qty_out ELSE 0 END) AS "2014-06-17", SUM(CASE date_out WHEN '2014-06-18' THEN qty_out ELSE 0 END) AS "2014-06-18", SUM(CASE date_out WHEN '2014-06-19' THEN qty_out ELSE 0 END) AS "2014-06-19", SUM(CASE date_out WHEN '2014-06-20' THEN qty_out ELSE 0 END) AS "2014-06-20", SUM(CASE date_out WHEN '2014-06-21' THEN qty_out ELSE 0 END) AS "2014-06-21", SUM(CASE date_out WHEN '2014-06-22' THEN qty_out ELSE 0 END) AS "2014-06-22", SUM(CASE date_out WHEN '2014-06-23' THEN qty_out ELSE 0 END) AS "2014-06-23", SUM(CASE date_out WHEN '2014-06-24' THEN qty_out ELSE 0 END) AS "2014-06-24", SUM(CASE date_out WHEN '2014-06-25' THEN qty_out ELSE 0 END) AS "2014-06-25", SUM(CASE date_out WHEN '2014-06-26' THEN qty_out ELSE 0 END) AS "2014-06-26", SUM(CASE date_out WHEN '2014-06-27' THEN qty_out ELSE 0 END) AS "2014-06-27", SUM(CASE date_out WHEN '2014-06-28' THEN qty_out ELSE 0 END) AS "2014-06-28", SUM(CASE date_out WHEN '2014-06-29' THEN qty_out ELSE 0 END) AS "2014-06-29", SUM(CASE date_out WHEN '2014-06-30' THEN qty_out ELSE 0 END) AS "2014-06-30", SUM(CASE to_char(date_out, 'YY-MM') WHEN '14-06' THEN qty_out ELSE 0 END) AS "Total" FROM tcminv_stock_out tso INNER JOIN tcminv_item ti ON ti.item_id = tso.item_id INNER JOIN tcminv_qty_unit qu ON qu.qty_unit_id = ti.qty_unit_id 
WHERE surat_izin_id =2 AND to_char(date_out, 'MM-YY') ='06-14' AND contractor_id ='1' GROUP BY tso.item_id ORDER BY 1;

SELECT sto.item_id as item_id, MAX(item_name) as item_name, MAX(item_code) as item_code, MAX(qty_unit) as qty_unit, MAX(sto.surat_izin_id) as surat_izin_id, 
COALESCE (MAX(sto."2014-06-17") - MAX(str."2014-06-17")) as "2014-04-17" 
FROM tcminv_item ti INNER JOIN tcminv_qty_unit qu ON qu.qty_unit_id = ti.qty_unit_id 
INNER JOIN ( 
SELECT item_id, MAX(sto.surat_izin_id) as surat_izin_id, MAX(sto.contractor_id) as contractor_id, SUM(CASE date_out WHEN '2014-06-01' THEN qty_out ELSE 0 END) AS "2014-06-01",SUM(CASE date_out WHEN '2014-06-02' THEN qty_out ELSE 0 END) AS "2014-06-02",SUM(CASE date_out WHEN '2014-06-03' THEN qty_out ELSE 0 END) AS "2014-06-03",SUM(CASE date_out WHEN '2014-06-04' THEN qty_out ELSE 0 END) AS "2014-06-04",SUM(CASE date_out WHEN '2014-06-05' THEN qty_out ELSE 0 END) AS "2014-06-05",SUM(CASE date_out WHEN '2014-06-06' THEN qty_out ELSE 0 END) AS "2014-06-06",SUM(CASE date_out WHEN '2014-06-07' THEN qty_out ELSE 0 END) AS "2014-06-07",SUM(CASE date_out WHEN '2014-06-08' THEN qty_out ELSE 0 END) AS "2014-06-08",SUM(CASE date_out WHEN '2014-06-09' THEN qty_out ELSE 0 END) AS "2014-06-09",SUM(CASE date_out WHEN '2014-06-10' THEN qty_out ELSE 0 END) AS "2014-06-10",SUM(CASE date_out WHEN '2014-06-11' THEN qty_out ELSE 0 END) AS "2014-06-11",SUM(CASE date_out WHEN '2014-06-12' THEN qty_out ELSE 0 END) AS "2014-06-12",SUM(CASE date_out WHEN '2014-06-13' THEN qty_out ELSE 0 END) AS "2014-06-13",SUM(CASE date_out WHEN '2014-06-14' THEN qty_out ELSE 0 END) AS "2014-06-14",SUM(CASE date_out WHEN '2014-06-15' THEN qty_out ELSE 0 END) AS "2014-06-15",SUM(CASE date_out WHEN '2014-06-16' THEN qty_out ELSE 0 END) AS "2014-06-16",SUM(CASE date_out WHEN '2014-06-17' THEN qty_out ELSE 0 END) AS "2014-06-17",SUM(CASE date_out WHEN '2014-06-18' THEN qty_out ELSE 0 END) AS "2014-06-18",SUM(CASE date_out WHEN '2014-06-19' THEN qty_out ELSE 0 END) AS "2014-06-19",SUM(CASE date_out WHEN '2014-06-20' THEN qty_out ELSE 0 END) AS "2014-06-20",SUM(CASE date_out WHEN '2014-06-21' THEN qty_out ELSE 0 END) AS "2014-06-21",SUM(CASE date_out WHEN '2014-06-22' THEN qty_out ELSE 0 END) AS "2014-06-22",SUM(CASE date_out WHEN '2014-06-23' THEN qty_out ELSE 0 END) AS "2014-06-23",SUM(CASE date_out WHEN '2014-06-24' THEN qty_out ELSE 0 END) AS "2014-06-24",SUM(CASE date_out WHEN '2014-06-25' THEN qty_out ELSE 0 END) AS "2014-06-25",SUM(CASE date_out WHEN '2014-06-26' THEN qty_out ELSE 0 END) AS "2014-06-26",SUM(CASE date_out WHEN '2014-06-27' THEN qty_out ELSE 0 END) AS "2014-06-27",SUM(CASE date_out WHEN '2014-06-28' THEN qty_out ELSE 0 END) AS "2014-06-28",SUM(CASE date_out WHEN '2014-06-29' THEN qty_out ELSE 0 END) AS "2014-06-29",SUM(CASE date_out WHEN '2014-06-30' THEN qty_out ELSE 0 END) AS "2014-06-30" FROM tcminv_stock_out sto WHERE sto.surat_izin_id =2 AND to_char(date_out, 'MM-YY') ='06-14' AND sto.contractor_id ='1' GROUP BY item_id ) as sto on (sto.item_id = ti.item_id) LEFT JOIN ( SELECT item_id, SUM(CASE date_return WHEN '2014-06-01' THEN qty_return ELSE 0 END) AS "2014-06-01",SUM(CASE date_return WHEN '2014-06-02' THEN qty_return ELSE 0 END) AS "2014-06-02",SUM(CASE date_return WHEN '2014-06-03' THEN qty_return ELSE 0 END) AS "2014-06-03",SUM(CASE date_return WHEN '2014-06-04' THEN qty_return ELSE 0 END) AS "2014-06-04",SUM(CASE date_return WHEN '2014-06-05' THEN qty_return ELSE 0 END) AS "2014-06-05",SUM(CASE date_return WHEN '2014-06-06' THEN qty_return ELSE 0 END) AS "2014-06-06",SUM(CASE date_return WHEN '2014-06-07' THEN qty_return ELSE 0 END) AS "2014-06-07",SUM(CASE date_return WHEN '2014-06-08' THEN qty_return ELSE 0 END) AS "2014-06-08",SUM(CASE date_return WHEN '2014-06-09' THEN qty_return ELSE 0 END) AS "2014-06-09",SUM(CASE date_return WHEN '2014-06-10' THEN qty_return ELSE 0 END) AS "2014-06-10",SUM(CASE date_return WHEN '2014-06-11' THEN qty_return ELSE 0 END) AS "2014-06-11",SUM(CASE date_return WHEN '2014-06-12' THEN qty_return ELSE 0 END) AS "2014-06-12",SUM(CASE date_return WHEN '2014-06-13' THEN qty_return ELSE 0 END) AS "2014-06-13",SUM(CASE date_return WHEN '2014-06-14' THEN qty_return ELSE 0 END) AS "2014-06-14",SUM(CASE date_return WHEN '2014-06-15' THEN qty_return ELSE 0 END) AS "2014-06-15",SUM(CASE date_return WHEN '2014-06-16' THEN qty_return ELSE 0 END) AS "2014-06-16",SUM(CASE date_return WHEN '2014-06-17' THEN qty_return ELSE 0 END) AS "2014-06-17",SUM(CASE date_return WHEN '2014-06-18' THEN qty_return ELSE 0 END) AS "2014-06-18",SUM(CASE date_return WHEN '2014-06-19' THEN qty_return ELSE 0 END) AS "2014-06-19",SUM(CASE date_return WHEN '2014-06-20' THEN qty_return ELSE 0 END) AS "2014-06-20",SUM(CASE date_return WHEN '2014-06-21' THEN qty_return ELSE 0 END) AS "2014-06-21",SUM(CASE date_return WHEN '2014-06-22' THEN qty_return ELSE 0 END) AS "2014-06-22",SUM(CASE date_return WHEN '2014-06-23' THEN qty_return ELSE 0 END) AS "2014-06-23",SUM(CASE date_return WHEN '2014-06-24' THEN qty_return ELSE 0 END) AS "2014-06-24",SUM(CASE date_return WHEN '2014-06-25' THEN qty_return ELSE 0 END) AS "2014-06-25",SUM(CASE date_return WHEN '2014-06-26' THEN qty_return ELSE 0 END) AS "2014-06-26",SUM(CASE date_return WHEN '2014-06-27' THEN qty_return ELSE 0 END) AS "2014-06-27",SUM(CASE date_return WHEN '2014-06-28' THEN qty_return ELSE 0 END) AS "2014-06-28",SUM(CASE date_return WHEN '2014-06-29' THEN qty_return ELSE 0 END) AS "2014-06-29",SUM(CASE date_return WHEN '2014-06-30' THEN qty_return ELSE 0 END) AS "2014-06-30",SUM(CASE to_char(date_return, 'YY-MM') WHEN '14-06' THEN qty_return ELSE 0 END) AS "total_return" FROM tcminv_stock_return str GROUP BY item_id ) as str on (sto.item_id = str.item_id) GROUP BY sto.item_id ORDER BY 1

SELECT sto.item_id as item_id, MAX(item_name) as item_name, MAX(item_code) as item_code, MAX(qty_unit) as qty_unit, MAX(sto.surat_izin_id) as surat_izin_id, COALESCE (MAX(sto."2014-06-01") - MAX(str."2014-06-01")) as "2014-06-01", COALESCE (MAX(sto."2014-06-02") - MAX(str."2014-06-02")) as "2014-06-02", COALESCE (MAX(sto."2014-06-03") - MAX(str."2014-06-03")) as "2014-06-03", COALESCE (MAX(sto."2014-06-04") - MAX(str."2014-06-04")) as "2014-06-04", COALESCE (MAX(sto."2014-06-05") - MAX(str."2014-06-05")) as "2014-06-05", COALESCE (MAX(sto."2014-06-06") - MAX(str."2014-06-06")) as "2014-06-06", COALESCE (MAX(sto."2014-06-07") - MAX(str."2014-06-07")) as "2014-06-07", COALESCE (MAX(sto."2014-06-08") - MAX(str."2014-06-08")) as "2014-06-08", COALESCE (MAX(sto."2014-06-09") - MAX(str."2014-06-09")) as "2014-06-09", COALESCE (MAX(sto."2014-06-10") - MAX(str."2014-06-10")) as "2014-06-10", COALESCE (MAX(sto."2014-06-11") - MAX(str."2014-06-11")) as "2014-06-11", COALESCE (MAX(sto."2014-06-12") - MAX(str."2014-06-12")) as "2014-06-12", COALESCE (MAX(sto."2014-06-13") - MAX(str."2014-06-13")) as "2014-06-13", COALESCE (MAX(sto."2014-06-14") - MAX(str."2014-06-14")) as "2014-06-14", COALESCE (MAX(sto."2014-06-15") - MAX(str."2014-06-15")) as "2014-06-15", COALESCE (MAX(sto."2014-06-16") - MAX(str."2014-06-16")) as "2014-06-16", COALESCE (MAX(sto."2014-06-17") - MAX(str."2014-06-17")) as "2014-06-17", COALESCE (MAX(sto."2014-06-18") - MAX(str."2014-06-18")) as "2014-06-18", COALESCE (MAX(sto."2014-06-19") - MAX(str."2014-06-19")) as "2014-06-19", COALESCE (MAX(sto."2014-06-20") - MAX(str."2014-06-20")) as "2014-06-20", COALESCE (MAX(sto."2014-06-21") - MAX(str."2014-06-21")) as "2014-06-21", COALESCE (MAX(sto."2014-06-22") - MAX(str."2014-06-22")) as "2014-06-22", COALESCE (MAX(sto."2014-06-23") - MAX(str."2014-06-23")) as "2014-06-23", COALESCE (MAX(sto."2014-06-24") - MAX(str."2014-06-24")) as "2014-06-24", COALESCE (MAX(sto."2014-06-25") - MAX(str."2014-06-25")) as "2014-06-25", COALESCE (MAX(sto."2014-06-26") - MAX(str."2014-06-26")) as "2014-06-26", COALESCE (MAX(sto."2014-06-27") - MAX(str."2014-06-27")) as "2014-06-27", COALESCE (MAX(sto."2014-06-28") - MAX(str."2014-06-28")) as "2014-06-28", COALESCE (MAX(sto."2014-06-29") - MAX(str."2014-06-29")) as "2014-06-29", COALESCE (MAX(sto."2014-06-30") - MAX(str."2014-06-30")) as "2014-06-30", COALESCE (MAX(sto.total_out) - MAX(str.total_return)) as total_use FROM tcminv_item ti INNER JOIN tcminv_qty_unit qu ON qu.qty_unit_id = ti.qty_unit_id INNER JOIN ( SELECT item_id, MAX(sto.surat_izin_id) as surat_izin_id, MAX(sto.contractor_id) as contractor_id, SUM(CASE date_out WHEN '2014-06-01' THEN qty_out ELSE 0 END) AS "2014-06-01",SUM(CASE date_out WHEN '2014-06-02' THEN qty_out ELSE 0 END) AS "2014-06-02",SUM(CASE date_out WHEN '2014-06-03' THEN qty_out ELSE 0 END) AS "2014-06-03",SUM(CASE date_out WHEN '2014-06-04' THEN qty_out ELSE 0 END) AS "2014-06-04",SUM(CASE date_out WHEN '2014-06-05' THEN qty_out ELSE 0 END) AS "2014-06-05",SUM(CASE date_out WHEN '2014-06-06' THEN qty_out ELSE 0 END) AS "2014-06-06",SUM(CASE date_out WHEN '2014-06-07' THEN qty_out ELSE 0 END) AS "2014-06-07",SUM(CASE date_out WHEN '2014-06-08' THEN qty_out ELSE 0 END) AS "2014-06-08",SUM(CASE date_out WHEN '2014-06-09' THEN qty_out ELSE 0 END) AS "2014-06-09",SUM(CASE date_out WHEN '2014-06-10' THEN qty_out ELSE 0 END) AS "2014-06-10",SUM(CASE date_out WHEN '2014-06-11' THEN qty_out ELSE 0 END) AS "2014-06-11",SUM(CASE date_out WHEN '2014-06-12' THEN qty_out ELSE 0 END) AS "2014-06-12",SUM(CASE date_out WHEN '2014-06-13' THEN qty_out ELSE 0 END) AS "2014-06-13",SUM(CASE date_out WHEN '2014-06-14' THEN qty_out ELSE 0 END) AS "2014-06-14",SUM(CASE date_out WHEN '2014-06-15' THEN qty_out ELSE 0 END) AS "2014-06-15",SUM(CASE date_out WHEN '2014-06-16' THEN qty_out ELSE 0 END) AS "2014-06-16",SUM(CASE date_out WHEN '2014-06-17' THEN qty_out ELSE 0 END) AS "2014-06-17",SUM(CASE date_out WHEN '2014-06-18' THEN qty_out ELSE 0 END) AS "2014-06-18",SUM(CASE date_out WHEN '2014-06-19' THEN qty_out ELSE 0 END) AS "2014-06-19",SUM(CASE date_out WHEN '2014-06-20' THEN qty_out ELSE 0 END) AS "2014-06-20",SUM(CASE date_out WHEN '2014-06-21' THEN qty_out ELSE 0 END) AS "2014-06-21",SUM(CASE date_out WHEN '2014-06-22' THEN qty_out ELSE 0 END) AS "2014-06-22",SUM(CASE date_out WHEN '2014-06-23' THEN qty_out ELSE 0 END) AS "2014-06-23",SUM(CASE date_out WHEN '2014-06-24' THEN qty_out ELSE 0 END) AS "2014-06-24",SUM(CASE date_out WHEN '2014-06-25' THEN qty_out ELSE 0 END) AS "2014-06-25",SUM(CASE date_out WHEN '2014-06-26' THEN qty_out ELSE 0 END) AS "2014-06-26",SUM(CASE date_out WHEN '2014-06-27' THEN qty_out ELSE 0 END) AS "2014-06-27",SUM(CASE date_out WHEN '2014-06-28' THEN qty_out ELSE 0 END) AS "2014-06-28",SUM(CASE date_out WHEN '2014-06-29' THEN qty_out ELSE 0 END) AS "2014-06-29",SUM(CASE date_out WHEN '2014-06-30' THEN qty_out ELSE 0 END) AS "2014-06-30",SUM(CASE to_char(date_out, 'YY-MM') WHEN '14-06' THEN qty_out ELSE 0 END) AS "total_out" FROM tcminv_stock_out sto WHERE sto.surat_izin_id =2 AND to_char(date_out, 'MM-YY') ='06-14' GROUP BY item_id ) as sto on (sto.item_id = ti.item_id) LEFT JOIN ( SELECT item_id, SUM(CASE date_return WHEN '2014-06-01' THEN qty_return ELSE 0 END) AS "2014-06-01",SUM(CASE date_return WHEN '2014-06-02' THEN qty_return ELSE 0 END) AS "2014-06-02",SUM(CASE date_return WHEN '2014-06-03' THEN qty_return ELSE 0 END) AS "2014-06-03",SUM(CASE date_return WHEN '2014-06-04' THEN qty_return ELSE 0 END) AS "2014-06-04",SUM(CASE date_return WHEN '2014-06-05' THEN qty_return ELSE 0 END) AS "2014-06-05",SUM(CASE date_return WHEN '2014-06-06' THEN qty_return ELSE 0 END) AS "2014-06-06",SUM(CASE date_return WHEN '2014-06-07' THEN qty_return ELSE 0 END) AS "2014-06-07",SUM(CASE date_return WHEN '2014-06-08' THEN qty_return ELSE 0 END) AS "2014-06-08",SUM(CASE date_return WHEN '2014-06-09' THEN qty_return ELSE 0 END) AS "2014-06-09",SUM(CASE date_return WHEN '2014-06-10' THEN qty_return ELSE 0 END) AS "2014-06-10",SUM(CASE date_return WHEN '2014-06-11' THEN qty_return ELSE 0 END) AS "2014-06-11",SUM(CASE date_return WHEN '2014-06-12' THEN qty_return ELSE 0 END) AS "2014-06-12",SUM(CASE date_return WHEN '2014-06-13' THEN qty_return ELSE 0 END) AS "2014-06-13",SUM(CASE date_return WHEN '2014-06-14' THEN qty_return ELSE 0 END) AS "2014-06-14",SUM(CASE date_return WHEN '2014-06-15' THEN qty_return ELSE 0 END) AS "2014-06-15",SUM(CASE date_return WHEN '2014-06-16' THEN qty_return ELSE 0 END) AS "2014-06-16",SUM(CASE date_return WHEN '2014-06-17' THEN qty_return ELSE 0 END) AS "2014-06-17",SUM(CASE date_return WHEN '2014-06-18' THEN qty_return ELSE 0 END) AS "2014-06-18",SUM(CASE date_return WHEN '2014-06-19' THEN qty_return ELSE 0 END) AS "2014-06-19",SUM(CASE date_return WHEN '2014-06-20' THEN qty_return ELSE 0 END) AS "2014-06-20",SUM(CASE date_return WHEN '2014-06-21' THEN qty_return ELSE 0 END) AS "2014-06-21",SUM(CASE date_return WHEN '2014-06-22' THEN qty_return ELSE 0 END) AS "2014-06-22",SUM(CASE date_return WHEN '2014-06-23' THEN qty_return ELSE 0 END) AS "2014-06-23",SUM(CASE date_return WHEN '2014-06-24' THEN qty_return ELSE 0 END) AS "2014-06-24",SUM(CASE date_return WHEN '2014-06-25' THEN qty_return ELSE 0 END) AS "2014-06-25",SUM(CASE date_return WHEN '2014-06-26' THEN qty_return ELSE 0 END) AS "2014-06-26",SUM(CASE date_return WHEN '2014-06-27' THEN qty_return ELSE 0 END) AS "2014-06-27",SUM(CASE date_return WHEN '2014-06-28' THEN qty_return ELSE 0 END) AS "2014-06-28",SUM(CASE date_return WHEN '2014-06-29' THEN qty_return ELSE 0 END) AS "2014-06-29",SUM(CASE date_return WHEN '2014-06-30' THEN qty_return ELSE 0 END) AS "2014-06-30",SUM(CASE to_char(date_return, 'YY-MM') WHEN '14-06' THEN qty_return ELSE 0 END) AS "total_return" FROM tcminv_stock_return str GROUP BY item_id ) as str on (sto.item_id = str.item_id) GROUP BY sto.item_id ORDER BY 1

-- AND ti.category_id ='6'
SELECT SUM(CASE ti.category_id WHEN '6' THEN qty_in ELSE 0 END) AS "dinamit",
SUM(CASE ti.category_id WHEN '5' THEN qty_in ELSE 0 END) AS "detonator",
SUM(CASE ti.category_id WHEN '9' THEN qty_in ELSE 0 END) AS "pc", 
SUM(CASE WHEN (date_in  ='2014-06-07' AND ti.category_id ='6') THEN qty_in ELSE 0 END) AS "2014-06-17",
SUM(CASE to_char(date_in, 'YY-MM') WHEN '14-08' THEN qty_in ELSE 0 END) AS "total_in" 
FROM tcminv_stock_in st 
INNER JOIN tcminv_item ti ON ti.item_id = st.item_id 
INNER JOIN tcminv_category tc ON tc.category_id = ti.category_id 
WHERE st.surat_izin_id =2 
--GROUP BY ti.category_id
ORDER BY 1;

SELECT SUM(qty_in), MAX(ti.category_id)
FROM tcminv_stock_in st
INNER JOIN tcminv_item ti ON ti.item_id = st.item_id
--INNER JOIN tcminv_category c ON ti.category_id = c.category_id
WHERE date_in = '2014-06-19'
AND ti.category_id = 6
AND st.surat_izin_id ='2'
--GROUP BY ti.category_id;

SELECT SUM(qty_in), MAX(ti.category_id)
FROM tcminv_stock_in st
INNER JOIN tcminv_item ti ON ti.item_id = st.item_id
WHERE  ti.category_id = 6
AND st.surat_izin_id ='2'


SELECT SUM(CASE ti.category_id WHEN '6' THEN qty_in ELSE 0 END) AS "dinamit",SUM(CASE ti.category_id WHEN '5' THEN qty_in ELSE 0 END) AS "detonator",
SUM(CASE ti.category_id WHEN '9' THEN qty_in ELSE 0 END) AS "pc", 
SUM(CASE WHEN (date_in ='2014-06-01' AND ti.category_id='6') THEN qty_in ELSE 0 END) AS "2014-06-01_6",
SUM(CASE WHEN (date_in ='2014-06-01' AND ti.category_id='5') THEN qty_in ELSE 0 END) AS "2014-06-01_5",
SUM(CASE WHEN (date_in ='2014-06-01' AND ti.category_id='9') THEN qty_in ELSE 0 END) AS "2014-06-01_9",
SUM(CASE WHEN (date_in ='2014-06-02' AND ti.category_id='6') THEN qty_in ELSE 0 END) AS "2014-06-02_6",
SUM(CASE WHEN (date_in ='2014-06-02' AND ti.category_id='5') THEN qty_in ELSE 0 END) AS "2014-06-02_5",
SUM(CASE WHEN (date_in ='2014-06-02' AND ti.category_id='9') THEN qty_in ELSE 0 END) AS "2014-06-02_9",
SUM(CASE WHEN (date_in ='2014-06-03' AND ti.category_id='6') THEN qty_in ELSE 0 END) AS "2014-06-03_6",
SUM(CASE WHEN (date_in ='2014-06-03' AND ti.category_id='5') THEN qty_in ELSE 0 END) AS "2014-06-03_5",
SUM(CASE WHEN (date_in ='2014-06-03' AND ti.category_id='9') THEN qty_in ELSE 0 END) AS "2014-06-03_9",
SUM(CASE WHEN (date_in ='2014-06-04' AND ti.category_id='6') THEN qty_in ELSE 0 END) AS "2014-06-04_6",SUM(CASE WHEN (date_in ='2014-06-04' AND ti.category_id='5') THEN qty_in ELSE 0 END) AS "2014-06-04_5",SUM(CASE WHEN (date_in ='2014-06-04' AND ti.category_id='9') THEN qty_in ELSE 0 END) AS "2014-06-04_9",SUM(CASE WHEN (date_in ='2014-06-05' AND ti.category_id='6') THEN qty_in ELSE 0 END) AS "2014-06-05_6",SUM(CASE WHEN (date_in ='2014-06-05' AND ti.category_id='5') THEN qty_in ELSE 0 END) AS "2014-06-05_5",SUM(CASE WHEN (date_in ='2014-06-05' AND ti.category_id='9') THEN qty_in ELSE 0 END) AS "2014-06-05_9",SUM(CASE WHEN (date_in ='2014-06-06' AND ti.category_id='6') THEN qty_in ELSE 0 END) AS "2014-06-06_6",SUM(CASE WHEN (date_in ='2014-06-06' AND ti.category_id='5') THEN qty_in ELSE 0 END) AS "2014-06-06_5",SUM(CASE WHEN (date_in ='2014-06-06' AND ti.category_id='9') THEN qty_in ELSE 0 END) AS "2014-06-06_9",SUM(CASE WHEN (date_in ='2014-06-07' AND ti.category_id='6') THEN qty_in ELSE 0 END) AS "2014-06-07_6",SUM(CASE WHEN (date_in ='2014-06-07' AND ti.category_id='5') THEN qty_in ELSE 0 END) AS "2014-06-07_5",SUM(CASE WHEN (date_in ='2014-06-07' AND ti.category_id='9') THEN qty_in ELSE 0 END) AS "2014-06-07_9",SUM(CASE WHEN (date_in ='2014-06-08' AND ti.category_id='6') THEN qty_in ELSE 0 END) AS "2014-06-08_6",SUM(CASE WHEN (date_in ='2014-06-08' AND ti.category_id='5') THEN qty_in ELSE 0 END) AS "2014-06-08_5",SUM(CASE WHEN (date_in ='2014-06-08' AND ti.category_id='9') THEN qty_in ELSE 0 END) AS "2014-06-08_9",SUM(CASE WHEN (date_in ='2014-06-09' AND ti.category_id='6') THEN qty_in ELSE 0 END) AS "2014-06-09_6",SUM(CASE WHEN (date_in ='2014-06-09' AND ti.category_id='5') THEN qty_in ELSE 0 END) AS "2014-06-09_5",SUM(CASE WHEN (date_in ='2014-06-09' AND ti.category_id='9') THEN qty_in ELSE 0 END) AS "2014-06-09_9",SUM(CASE WHEN (date_in ='2014-06-10' AND ti.category_id='6') THEN qty_in ELSE 0 END) AS "2014-06-10_6",SUM(CASE WHEN (date_in ='2014-06-10' AND ti.category_id='5') THEN qty_in ELSE 0 END) AS "2014-06-10_5",SUM(CASE WHEN (date_in ='2014-06-10' AND ti.category_id='9') THEN qty_in ELSE 0 END) AS "2014-06-10_9",SUM(CASE WHEN (date_in ='2014-06-11' AND ti.category_id='6') THEN qty_in ELSE 0 END) AS "2014-06-11_6",SUM(CASE WHEN (date_in ='2014-06-11' AND ti.category_id='5') THEN qty_in ELSE 0 END) AS "2014-06-11_5",SUM(CASE WHEN (date_in ='2014-06-11' AND ti.category_id='9') THEN qty_in ELSE 0 END) AS "2014-06-11_9",SUM(CASE WHEN (date_in ='2014-06-12' AND ti.category_id='6') THEN qty_in ELSE 0 END) AS "2014-06-12_6",SUM(CASE WHEN (date_in ='2014-06-12' AND ti.category_id='5') THEN qty_in ELSE 0 END) AS "2014-06-12_5",SUM(CASE WHEN (date_in ='2014-06-12' AND ti.category_id='9') THEN qty_in ELSE 0 END) AS "2014-06-12_9",SUM(CASE WHEN (date_in ='2014-06-13' AND ti.category_id='6') THEN qty_in ELSE 0 END) AS "2014-06-13_6",SUM(CASE WHEN (date_in ='2014-06-13' AND ti.category_id='5') THEN qty_in ELSE 0 END) AS "2014-06-13_5",SUM(CASE WHEN (date_in ='2014-06-13' AND ti.category_id='9') THEN qty_in ELSE 0 END) AS "2014-06-13_9",SUM(CASE WHEN (date_in ='2014-06-14' AND ti.category_id='6') THEN qty_in ELSE 0 END) AS "2014-06-14_6",SUM(CASE WHEN (date_in ='2014-06-14' AND ti.category_id='5') THEN qty_in ELSE 0 END) AS "2014-06-14_5",SUM(CASE WHEN (date_in ='2014-06-14' AND ti.category_id='9') THEN qty_in ELSE 0 END) AS "2014-06-14_9",SUM(CASE WHEN (date_in ='2014-06-15' AND ti.category_id='6') THEN qty_in ELSE 0 END) AS "2014-06-15_6",SUM(CASE WHEN (date_in ='2014-06-15' AND ti.category_id='5') THEN qty_in ELSE 0 END) AS "2014-06-15_5",SUM(CASE WHEN (date_in ='2014-06-15' AND ti.category_id='9') THEN qty_in ELSE 0 END) AS "2014-06-15_9",SUM(CASE WHEN (date_in ='2014-06-16' AND ti.category_id='6') THEN qty_in ELSE 0 END) AS "2014-06-16_6",SUM(CASE WHEN (date_in ='2014-06-16' AND ti.category_id='5') THEN qty_in ELSE 0 END) AS "2014-06-16_5",SUM(CASE WHEN (date_in ='2014-06-16' AND ti.category_id='9') THEN qty_in ELSE 0 END) AS "2014-06-16_9",SUM(CASE WHEN (date_in ='2014-06-17' AND ti.category_id='6') THEN qty_in ELSE 0 END) AS "2014-06-17_6",SUM(CASE WHEN (date_in ='2014-06-17' AND ti.category_id='5') THEN qty_in ELSE 0 END) AS "2014-06-17_5",SUM(CASE WHEN (date_in ='2014-06-17' AND ti.category_id='9') THEN qty_in ELSE 0 END) AS "2014-06-17_9",SUM(CASE WHEN (date_in ='2014-06-18' AND ti.category_id='6') THEN qty_in ELSE 0 END) AS "2014-06-18_6",SUM(CASE WHEN (date_in ='2014-06-18' AND ti.category_id='5') THEN qty_in ELSE 0 END) AS "2014-06-18_5",SUM(CASE WHEN (date_in ='2014-06-18' AND ti.category_id='9') THEN qty_in ELSE 0 END) AS "2014-06-18_9",SUM(CASE WHEN (date_in ='2014-06-19' AND ti.category_id='6') THEN qty_in ELSE 0 END) AS "2014-06-19_6",SUM(CASE WHEN (date_in ='2014-06-19' AND ti.category_id='5') THEN qty_in ELSE 0 END) AS "2014-06-19_5",SUM(CASE WHEN (date_in ='2014-06-19' AND ti.category_id='9') THEN qty_in ELSE 0 END) AS "2014-06-19_9",SUM(CASE WHEN (date_in ='2014-06-20' AND ti.category_id='6') THEN qty_in ELSE 0 END) AS "2014-06-20_6",SUM(CASE WHEN (date_in ='2014-06-20' AND ti.category_id='5') THEN qty_in ELSE 0 END) AS "2014-06-20_5",SUM(CASE WHEN (date_in ='2014-06-20' AND ti.category_id='9') THEN qty_in ELSE 0 END) AS "2014-06-20_9",SUM(CASE WHEN (date_in ='2014-06-21' AND ti.category_id='6') THEN qty_in ELSE 0 END) AS "2014-06-21_6",SUM(CASE WHEN (date_in ='2014-06-21' AND ti.category_id='5') THEN qty_in ELSE 0 END) AS "2014-06-21_5",SUM(CASE WHEN (date_in ='2014-06-21' AND ti.category_id='9') THEN qty_in ELSE 0 END) AS "2014-06-21_9",SUM(CASE WHEN (date_in ='2014-06-22' AND ti.category_id='6') THEN qty_in ELSE 0 END) AS "2014-06-22_6",SUM(CASE WHEN (date_in ='2014-06-22' AND ti.category_id='5') THEN qty_in ELSE 0 END) AS "2014-06-22_5",SUM(CASE WHEN (date_in ='2014-06-22' AND ti.category_id='9') THEN qty_in ELSE 0 END) AS "2014-06-22_9",SUM(CASE WHEN (date_in ='2014-06-23' AND ti.category_id='6') THEN qty_in ELSE 0 END) AS "2014-06-23_6",SUM(CASE WHEN (date_in ='2014-06-23' AND ti.category_id='5') THEN qty_in ELSE 0 END) AS "2014-06-23_5",SUM(CASE WHEN (date_in ='2014-06-23' AND ti.category_id='9') THEN qty_in ELSE 0 END) AS "2014-06-23_9",SUM(CASE WHEN (date_in ='2014-06-24' AND ti.category_id='6') THEN qty_in ELSE 0 END) AS "2014-06-24_6",SUM(CASE WHEN (date_in ='2014-06-24' AND ti.category_id='5') THEN qty_in ELSE 0 END) AS "2014-06-24_5",SUM(CASE WHEN (date_in ='2014-06-24' AND ti.category_id='9') THEN qty_in ELSE 0 END) AS "2014-06-24_9",SUM(CASE WHEN (date_in ='2014-06-25' AND ti.category_id='6') THEN qty_in ELSE 0 END) AS "2014-06-25_6",SUM(CASE WHEN (date_in ='2014-06-25' AND ti.category_id='5') THEN qty_in ELSE 0 END) AS "2014-06-25_5",SUM(CASE WHEN (date_in ='2014-06-25' AND ti.category_id='9') THEN qty_in ELSE 0 END) AS "2014-06-25_9",SUM(CASE WHEN (date_in ='2014-06-26' AND ti.category_id='6') THEN qty_in ELSE 0 END) AS "2014-06-26_6",SUM(CASE WHEN (date_in ='2014-06-26' AND ti.category_id='5') THEN qty_in ELSE 0 END) AS "2014-06-26_5",SUM(CASE WHEN (date_in ='2014-06-26' AND ti.category_id='9') THEN qty_in ELSE 0 END) AS "2014-06-26_9",SUM(CASE WHEN (date_in ='2014-06-27' AND ti.category_id='6') THEN qty_in ELSE 0 END) AS "2014-06-27_6",SUM(CASE WHEN (date_in ='2014-06-27' AND ti.category_id='5') THEN qty_in ELSE 0 END) AS "2014-06-27_5",SUM(CASE WHEN (date_in ='2014-06-27' AND ti.category_id='9') THEN qty_in ELSE 0 END) AS "2014-06-27_9",SUM(CASE WHEN (date_in ='2014-06-28' AND ti.category_id='6') THEN qty_in ELSE 0 END) AS "2014-06-28_6",SUM(CASE WHEN (date_in ='2014-06-28' AND ti.category_id='5') THEN qty_in ELSE 0 END) AS "2014-06-28_5",SUM(CASE WHEN (date_in ='2014-06-28' AND ti.category_id='9') THEN qty_in ELSE 0 END) AS "2014-06-28_9",SUM(CASE WHEN (date_in ='2014-06-29' AND ti.category_id='6') THEN qty_in ELSE 0 END) AS "2014-06-29_6",SUM(CASE WHEN (date_in ='2014-06-29' AND ti.category_id='5') THEN qty_in ELSE 0 END) AS "2014-06-29_5",SUM(CASE WHEN (date_in ='2014-06-29' AND ti.category_id='9') THEN qty_in ELSE 0 END) AS "2014-06-29_9",SUM(CASE WHEN (date_in ='2014-06-30' AND ti.category_id='6') THEN qty_in ELSE 0 END) AS "2014-06-30_6",SUM(CASE WHEN (date_in ='2014-06-30' AND ti.category_id='5') THEN qty_in ELSE 0 END) AS "2014-06-30_5",SUM(CASE WHEN (date_in ='2014-06-30' AND ti.category_id='9') THEN qty_in ELSE 0 END) AS "2014-06-30_9",SUM(CASE to_char(date_in, 'YY-MM') WHEN '14-06' THEN qty_in ELSE 0 END) AS "total_in" FROM tcminv_stock_in st INNER JOIN tcminv_item ti ON ti.item_id = st.item_id INNER JOIN tcminv_category tc ON tc.category_id = ti.category_id WHERE st.surat_izin_id =2 ORDER BY 1;


-- Query to get Category stock  use
SELECT 
COALESCE (MAX(sto."2014-06-17_5") - MAX(str."2014-06-17_5") )as sum_use5
FROM tcminv_item ti
INNER JOIN (
SELECT category_id,
SUM(CASE WHEN (date_out  ='2014-06-17' AND ti.category_id='5') THEN qty_out  ELSE 0 END) AS "2014-06-17_5"
FROM  tcminv_stock_out  sto
INNER JOIN tcminv_item ti ON sto.item_id = ti.item_id
GROUP BY category_id
) as sto on (sto.category_id = ti.category_id)
LEFT JOIN (
SELECT category_id,
SUM(CASE  WHEN (date_return  ='2014-06-17' AND ti.category_id='5')  THEN qty_return  ELSE 0 END) AS "2014-06-17_5"
 FROM  tcminv_stock_return str
INNER JOIN tcminv_item ti ON str.item_id = ti.item_id
GROUP BY category_id;

SELECT SUM(CASE ti.category_id WHEN '6' THEN qty_out ELSE 0 END) AS "dinamit",SUM(CASE ti.category_id WHEN '5' THEN qty_out ELSE 0 END) AS "detonator",SUM(CASE ti.category_id WHEN '9' THEN qty_out ELSE 0 END) AS "AN", SUM(CASE WHEN (date_out ='2014-07-01' AND ti.category_id='6') THEN qty_out ELSE 0 END) AS "d2014_07_01_6",SUM(CASE WHEN (date_out ='2014-07-01' AND ti.category_id='5') THEN qty_out ELSE 0 END) AS "d2014_07_01_5",SUM(CASE WHEN (date_out ='2014-07-01' AND ti.category_id='9') THEN qty_out ELSE 0 END) AS "d2014_07_01_9",SUM(CASE WHEN (date_out ='2014-07-02' AND ti.category_id='6') THEN qty_out ELSE 0 END) AS "d2014_07_02_6",SUM(CASE WHEN (date_out ='2014-07-02' AND ti.category_id='5') THEN qty_out ELSE 0 END) AS "d2014_07_02_5",SUM(CASE WHEN (date_out ='2014-07-02' AND ti.category_id='9') THEN qty_out ELSE 0 END) AS "d2014_07_02_9",SUM(CASE WHEN (date_out ='2014-07-03' AND ti.category_id='6') THEN qty_out ELSE 0 END) AS "d2014_07_03_6",SUM(CASE WHEN (date_out ='2014-07-03' AND ti.category_id='5') THEN qty_out ELSE 0 END) AS "d2014_07_03_5",SUM(CASE WHEN (date_out ='2014-07-03' AND ti.category_id='9') THEN qty_out ELSE 0 END) AS "d2014_07_03_9",SUM(CASE WHEN (date_out ='2014-07-04' AND ti.category_id='6') THEN qty_out ELSE 0 END) AS "d2014_07_04_6",SUM(CASE WHEN (date_out ='2014-07-04' AND ti.category_id='5') THEN qty_out ELSE 0 END) AS "d2014_07_04_5",SUM(CASE WHEN (date_out ='2014-07-04' AND ti.category_id='9') THEN qty_out ELSE 0 END) AS "d2014_07_04_9",SUM(CASE WHEN (date_out ='2014-07-05' AND ti.category_id='6') THEN qty_out ELSE 0 END) AS "d2014_07_05_6",SUM(CASE WHEN (date_out ='2014-07-05' AND ti.category_id='5') THEN qty_out ELSE 0 END) AS "d2014_07_05_5",SUM(CASE WHEN (date_out ='2014-07-05' AND ti.category_id='9') THEN qty_out ELSE 0 END) AS "d2014_07_05_9",SUM(CASE WHEN (date_out ='2014-07-06' AND ti.category_id='6') THEN qty_out ELSE 0 END) AS "d2014_07_06_6",SUM(CASE WHEN (date_out ='2014-07-06' AND ti.category_id='5') THEN qty_out ELSE 0 END) AS "d2014_07_06_5",SUM(CASE WHEN (date_out ='2014-07-06' AND ti.category_id='9') THEN qty_out ELSE 0 END) AS "d2014_07_06_9",SUM(CASE WHEN (date_out ='2014-07-07' AND ti.category_id='6') THEN qty_out ELSE 0 END) AS "d2014_07_07_6",SUM(CASE WHEN (date_out ='2014-07-07' AND ti.category_id='5') THEN qty_out ELSE 0 END) AS "d2014_07_07_5",SUM(CASE WHEN (date_out ='2014-07-07' AND ti.category_id='9') THEN qty_out ELSE 0 END) AS "d2014_07_07_9",SUM(CASE WHEN (date_out ='2014-07-08' AND ti.category_id='6') THEN qty_out ELSE 0 END) AS "d2014_07_08_6",SUM(CASE WHEN (date_out ='2014-07-08' AND ti.category_id='5') THEN qty_out ELSE 0 END) AS "d2014_07_08_5",SUM(CASE WHEN (date_out ='2014-07-08' AND ti.category_id='9') THEN qty_out ELSE 0 END) AS "d2014_07_08_9",SUM(CASE WHEN (date_out ='2014-07-09' AND ti.category_id='6') THEN qty_out ELSE 0 END) AS "d2014_07_09_6",SUM(CASE WHEN (date_out ='2014-07-09' AND ti.category_id='5') THEN qty_out ELSE 0 END) AS "d2014_07_09_5",SUM(CASE WHEN (date_out ='2014-07-09' AND ti.category_id='9') THEN qty_out ELSE 0 END) AS "d2014_07_09_9",SUM(CASE WHEN (date_out ='2014-07-10' AND ti.category_id='6') THEN qty_out ELSE 0 END) AS "d2014_07_10_6",SUM(CASE WHEN (date_out ='2014-07-10' AND ti.category_id='5') THEN qty_out ELSE 0 END) AS "d2014_07_10_5",SUM(CASE WHEN (date_out ='2014-07-10' AND ti.category_id='9') THEN qty_out ELSE 0 END) AS "d2014_07_10_9",SUM(CASE WHEN (date_out ='2014-07-11' AND ti.category_id='6') THEN qty_out ELSE 0 END) AS "d2014_07_11_6",SUM(CASE WHEN (date_out ='2014-07-11' AND ti.category_id='5') THEN qty_out ELSE 0 END) AS "d2014_07_11_5",SUM(CASE WHEN (date_out ='2014-07-11' AND ti.category_id='9') THEN qty_out ELSE 0 END) AS "d2014_07_11_9",SUM(CASE WHEN (date_out ='2014-07-12' AND ti.category_id='6') THEN qty_out ELSE 0 END) AS "d2014_07_12_6",SUM(CASE WHEN (date_out ='2014-07-12' AND ti.category_id='5') THEN qty_out ELSE 0 END) AS "d2014_07_12_5",SUM(CASE WHEN (date_out ='2014-07-12' AND ti.category_id='9') THEN qty_out ELSE 0 END) AS "d2014_07_12_9",SUM(CASE WHEN (date_out ='2014-07-13' AND ti.category_id='6') THEN qty_out ELSE 0 END) AS "d2014_07_13_6",SUM(CASE WHEN (date_out ='2014-07-13' AND ti.category_id='5') THEN qty_out ELSE 0 END) AS "d2014_07_13_5",SUM(CASE WHEN (date_out ='2014-07-13' AND ti.category_id='9') THEN qty_out ELSE 0 END) AS "d2014_07_13_9",SUM(CASE WHEN (date_out ='2014-07-14' AND ti.category_id='6') THEN qty_out ELSE 0 END) AS "d2014_07_14_6",SUM(CASE WHEN (date_out ='2014-07-14' AND ti.category_id='5') THEN qty_out ELSE 0 END) AS "d2014_07_14_5",SUM(CASE WHEN (date_out ='2014-07-14' AND ti.category_id='9') THEN qty_out ELSE 0 END) AS "d2014_07_14_9",SUM(CASE WHEN (date_out ='2014-07-15' AND ti.category_id='6') THEN qty_out ELSE 0 END) AS "d2014_07_15_6",SUM(CASE WHEN (date_out ='2014-07-15' AND ti.category_id='5') THEN qty_out ELSE 0 END) AS "d2014_07_15_5",SUM(CASE WHEN (date_out ='2014-07-15' AND ti.category_id='9') THEN qty_out ELSE 0 END) AS "d2014_07_15_9",SUM(CASE WHEN (date_out ='2014-07-16' AND ti.category_id='6') THEN qty_out ELSE 0 END) AS "d2014_07_16_6",SUM(CASE WHEN (date_out ='2014-07-16' AND ti.category_id='5') THEN qty_out ELSE 0 END) AS "d2014_07_16_5",SUM(CASE WHEN (date_out ='2014-07-16' AND ti.category_id='9') THEN qty_out ELSE 0 END) AS "d2014_07_16_9",SUM(CASE WHEN (date_out ='2014-07-17' AND ti.category_id='6') THEN qty_out ELSE 0 END) AS "d2014_07_17_6",SUM(CASE WHEN (date_out ='2014-07-17' AND ti.category_id='5') THEN qty_out ELSE 0 END) AS "d2014_07_17_5",SUM(CASE WHEN (date_out ='2014-07-17' AND ti.category_id='9') THEN qty_out ELSE 0 END) AS "d2014_07_17_9",SUM(CASE WHEN (date_out ='2014-07-18' AND ti.category_id='6') THEN qty_out ELSE 0 END) AS "d2014_07_18_6",SUM(CASE WHEN (date_out ='2014-07-18' AND ti.category_id='5') THEN qty_out ELSE 0 END) AS "d2014_07_18_5",SUM(CASE WHEN (date_out ='2014-07-18' AND ti.category_id='9') THEN qty_out ELSE 0 END) AS "d2014_07_18_9",SUM(CASE WHEN (date_out ='2014-07-19' AND ti.category_id='6') THEN qty_out ELSE 0 END) AS "d2014_07_19_6",SUM(CASE WHEN (date_out ='2014-07-19' AND ti.category_id='5') THEN qty_out ELSE 0 END) AS "d2014_07_19_5",SUM(CASE WHEN (date_out ='2014-07-19' AND ti.category_id='9') THEN qty_out ELSE 0 END) AS "d2014_07_19_9",SUM(CASE WHEN (date_out ='2014-07-20' AND ti.category_id='6') THEN qty_out ELSE 0 END) AS "d2014_07_20_6",SUM(CASE WHEN (date_out ='2014-07-20' AND ti.category_id='5') THEN qty_out ELSE 0 END) AS "d2014_07_20_5",SUM(CASE WHEN (date_out ='2014-07-20' AND ti.category_id='9') THEN qty_out ELSE 0 END) AS "d2014_07_20_9",SUM(CASE WHEN (date_out ='2014-07-21' AND ti.category_id='6') THEN qty_out ELSE 0 END) AS "d2014_07_21_6",SUM(CASE WHEN (date_out ='2014-07-21' AND ti.category_id='5') THEN qty_out ELSE 0 END) AS "d2014_07_21_5",SUM(CASE WHEN (date_out ='2014-07-21' AND ti.category_id='9') THEN qty_out ELSE 0 END) AS "d2014_07_21_9",SUM(CASE WHEN (date_out ='2014-07-22' AND ti.category_id='6') THEN qty_out ELSE 0 END) AS "d2014_07_22_6",SUM(CASE WHEN (date_out ='2014-07-22' AND ti.category_id='5') THEN qty_out ELSE 0 END) AS "d2014_07_22_5",SUM(CASE WHEN (date_out ='2014-07-22' AND ti.category_id='9') THEN qty_out ELSE 0 END) AS "d2014_07_22_9",SUM(CASE WHEN (date_out ='2014-07-23' AND ti.category_id='6') THEN qty_out ELSE 0 END) AS "d2014_07_23_6",SUM(CASE WHEN (date_out ='2014-07-23' AND ti.category_id='5') THEN qty_out ELSE 0 END) AS "d2014_07_23_5",SUM(CASE WHEN (date_out ='2014-07-23' AND ti.category_id='9') THEN qty_out ELSE 0 END) AS "d2014_07_23_9",SUM(CASE WHEN (date_out ='2014-07-24' AND ti.category_id='6') THEN qty_out ELSE 0 END) AS "d2014_07_24_6",SUM(CASE WHEN (date_out ='2014-07-24' AND ti.category_id='5') THEN qty_out ELSE 0 END) AS "d2014_07_24_5",SUM(CASE WHEN (date_out ='2014-07-24' AND ti.category_id='9') THEN qty_out ELSE 0 END) AS "d2014_07_24_9",SUM(CASE WHEN (date_out ='2014-07-25' AND ti.category_id='6') THEN qty_out ELSE 0 END) AS "d2014_07_25_6",SUM(CASE WHEN (date_out ='2014-07-25' AND ti.category_id='5') THEN qty_out ELSE 0 END) AS "d2014_07_25_5",SUM(CASE WHEN (date_out ='2014-07-25' AND ti.category_id='9') THEN qty_out ELSE 0 END) AS "d2014_07_25_9",SUM(CASE WHEN (date_out ='2014-07-26' AND ti.category_id='6') THEN qty_out ELSE 0 END) AS "d2014_07_26_6",SUM(CASE WHEN (date_out ='2014-07-26' AND ti.category_id='5') THEN qty_out ELSE 0 END) AS "d2014_07_26_5",SUM(CASE WHEN (date_out ='2014-07-26' AND ti.category_id='9') THEN qty_out ELSE 0 END) AS "d2014_07_26_9",SUM(CASE WHEN (date_out ='2014-07-27' AND ti.category_id='6') THEN qty_out ELSE 0 END) AS "d2014_07_27_6",SUM(CASE WHEN (date_out ='2014-07-27' AND ti.category_id='5') THEN qty_out ELSE 0 END) AS "d2014_07_27_5",SUM(CASE WHEN (date_out ='2014-07-27' AND ti.category_id='9') THEN qty_out ELSE 0 END) AS "d2014_07_27_9",SUM(CASE WHEN (date_out ='2014-07-28' AND ti.category_id='6') THEN qty_out ELSE 0 END) AS "d2014_07_28_6",SUM(CASE WHEN (date_out ='2014-07-28' AND ti.category_id='5') THEN qty_out ELSE 0 END) AS "d2014_07_28_5",SUM(CASE WHEN (date_out ='2014-07-28' AND ti.category_id='9') THEN qty_out ELSE 0 END) AS "d2014_07_28_9",SUM(CASE WHEN (date_out ='2014-07-29' AND ti.category_id='6') THEN qty_out ELSE 0 END) AS "d2014_07_29_6",SUM(CASE WHEN (date_out ='2014-07-29' AND ti.category_id='5') THEN qty_out ELSE 0 END) AS "d2014_07_29_5",SUM(CASE WHEN (date_out ='2014-07-29' AND ti.category_id='9') THEN qty_out ELSE 0 END) AS "d2014_07_29_9",SUM(CASE WHEN (date_out ='2014-07-30' AND ti.category_id='6') THEN qty_out ELSE 0 END) AS "d2014_07_30_6",SUM(CASE WHEN (date_out ='2014-07-30' AND ti.category_id='5') THEN qty_out ELSE 0 END) AS "d2014_07_30_5",SUM(CASE WHEN (date_out ='2014-07-30' AND ti.category_id='9') THEN qty_out ELSE 0 END) AS "d2014_07_30_9",SUM(CASE WHEN (date_out ='2014-07-31' AND ti.category_id='6') THEN qty_out ELSE 0 END) AS "d2014_07_31_6",SUM(CASE WHEN (date_out ='2014-07-31' AND ti.category_id='5') THEN qty_out ELSE 0 END) AS "d2014_07_31_5",SUM(CASE WHEN (date_out ='2014-07-31' AND ti.category_id='9') THEN qty_out ELSE 0 END) AS "d2014_07_31_9" FROM tcminv_stock_out st INNER JOIN tcminv_item ti ON ti.item_id = st.item_id WHERE st.surat_izin_id =2 ORDER BY 1;

) as str on (sto.category_id = str.category_id);

SELECT MAX(date_out), SUM(qty_out)
FROM  tcminv_stock_out sto
INNER JOIN tcminv_item ti ON sto.item_id = ti.item_id
WHERE category_id ='5'
AND date_out ='2014-06-17'

SELECT date_out, qty_out, ti.category_id
FROM  tcminv_stock_out sto
INNER JOIN tcminv_item ti ON sto.item_id = ti.item_id
WHERE date_out ='2014-06-17'
ORDER BY category_id


 SELECT st.item_id,MAX(item_name) as item_name, MAX(item_code) as item_code, MAX(qty_unit) as qty_unit, SUM(CASE date_in WHEN '2014-08-01' THEN qty_in ELSE 0 END) AS "2014-08-01",SUM(CASE date_in WHEN '2014-08-02' THEN qty_in ELSE 0 END) AS "2014-08-02",SUM(CASE date_in WHEN '2014-08-03' THEN qty_in ELSE 0 END) AS "2014-08-03",SUM(CASE date_in WHEN '2014-08-04' THEN qty_in ELSE 0 END) AS "2014-08-04",SUM(CASE date_in WHEN '2014-08-05' THEN qty_in ELSE 0 END) AS "2014-08-05",SUM(CASE date_in WHEN '2014-08-06' THEN qty_in ELSE 0 END) AS "2014-08-06",SUM(CASE date_in WHEN '2014-08-07' THEN qty_in ELSE 0 END) AS "2014-08-07",SUM(CASE date_in WHEN '2014-08-08' THEN qty_in ELSE 0 END) AS "2014-08-08",SUM(CASE date_in WHEN '2014-08-09' THEN qty_in ELSE 0 END) AS "2014-08-09",SUM(CASE date_in WHEN '2014-08-10' THEN qty_in ELSE 0 END) AS "2014-08-10",SUM(CASE date_in WHEN '2014-08-11' THEN qty_in ELSE 0 END) AS "2014-08-11",SUM(CASE date_in WHEN '2014-08-12' THEN qty_in ELSE 0 END) AS "2014-08-12",SUM(CASE date_in WHEN '2014-08-13' THEN qty_in ELSE 0 END) AS "2014-08-13",SUM(CASE date_in WHEN '2014-08-14' THEN qty_in ELSE 0 END) AS "2014-08-14",SUM(CASE date_in WHEN '2014-08-15' THEN qty_in ELSE 0 END) AS "2014-08-15",SUM(CASE date_in WHEN '2014-08-16' THEN qty_in ELSE 0 END) AS "2014-08-16",SUM(CASE date_in WHEN '2014-08-17' THEN qty_in ELSE 0 END) AS "2014-08-17",SUM(CASE date_in WHEN '2014-08-18' THEN qty_in ELSE 0 END) AS "2014-08-18",SUM(CASE date_in WHEN '2014-08-19' THEN qty_in ELSE 0 END) AS "2014-08-19",SUM(CASE date_in WHEN '2014-08-20' THEN qty_in ELSE 0 END) AS "2014-08-20",SUM(CASE date_in WHEN '2014-08-21' THEN qty_in ELSE 0 END) AS "2014-08-21",SUM(CASE date_in WHEN '2014-08-22' THEN qty_in ELSE 0 END) AS "2014-08-22",SUM(CASE date_in WHEN '2014-08-23' THEN qty_in ELSE 0 END) AS "2014-08-23",SUM(CASE date_in WHEN '2014-08-24' THEN qty_in ELSE 0 END) AS "2014-08-24",SUM(CASE date_in WHEN '2014-08-25' THEN qty_in ELSE 0 END) AS "2014-08-25",SUM(CASE date_in WHEN '2014-08-26' THEN qty_in ELSE 0 END) AS "2014-08-26",SUM(CASE date_in WHEN '2014-08-27' THEN qty_in ELSE 0 END) AS "2014-08-27",SUM(CASE date_in WHEN '2014-08-28' THEN qty_in ELSE 0 END) AS "2014-08-28",SUM(CASE date_in WHEN '2014-08-29' THEN qty_in ELSE 0 END) AS "2014-08-29",SUM(CASE date_in WHEN '2014-08-30' THEN qty_in ELSE 0 END) AS "2014-08-30",SUM(CASE date_in WHEN '2014-08-31' THEN qty_in ELSE 0 END) AS "2014-08-31",SUM(CASE to_char(date_in, 'YY-MM') WHEN '14-08' THEN qty_in ELSE 0 END) AS "Total" 
FROM tcminv_stock_in st INNER JOIN tcminv_item ti ON ti.item_id = st.item_id 
INNER JOIN tcminv_qty_unit qu ON qu.qty_unit_id = ti.qty_unit_id WHERE surat_izin_id =2 AND to_char(date_in, 'MM-YY') ='08-14' GROUP BY st.item_id ORDER BY 1;

 SELECT * FROM "tcminv_surat_izin" WHERE "date_min" <= '2014-09-01' AND "date_max" >= '2014-03-01' ORDER BY "create_at" desc LIMIT 1

UPDATE tcminv_stock_in 
SET qty_in = 0

UPDATE tcminv_stock_out 
SET qty_out = 0

UPDATE tcminv_stock_return 
SET qty_return= 0


SELECT * FROM "tcminv_item" 
LEFT JOIN "tcminv_stock_in" ON "tcminv_item"."item_id" = "tcminv_stock_in"."item_id" 
LEFT JOIN "tcminv_stock_out" ON "tcminv_item"."item_id" = "tcminv_stock_out"."item_id"


 SELECT sti.item_id, item_name,item_code,sti.surat_izin_id,surat_izin,sum_qty_in, sum_qty_out,sum_qty_in - sum_qty_out + sum_qty_return as qty_av,qty_unit, sum_qty_return 
FROM (
 SELECT MIN(ti.item_name) as item_name, sti.item_id ,MAX(ti.item_code) as item_code, MAX(si.surat_izin) as surat_izin, sti.surat_izin_id , SUM(qty_in) as sum_qty_in,MAX(qty_unit) as qty_unit 
FROM tcminv_stock_in sti 
INNER JOIN tcminv_item ti ON sti.item_id = ti.item_id 
INNER JOIN tcminv_surat_izin si ON sti.surat_izin_id = si.surat_izin_id 
INNER JOIN tcminv_qty_unit qu ON ti.qty_unit_id = qu.qty_unit_id 
WHERE sti.surat_izin_id = 3 GROUP BY sti.item_id, sti.surat_izin_id ) sti 
LEFT JOIN ( 
SELECT item_id, surat_izin_id, SUM(qty_out) as sum_qty_out 
FROM tcminv_stock_out WHERE surat_izin_id = 3 GROUP BY item_id, surat_izin_id)  sto ON (sti.item_id = sto.item_id ) 
LEFT JOIN ( 
SELECT item_id, surat_izin_id, SUM(qty_return) as sum_qty_return FROM tcminv_stock_return 
WHERE surat_izin_id = 3 GROUP BY item_id, surat_izin_id) rtn ON (sti.item_id = rtn.item_id) 
-- USING ( item_id, surat_izin_id) ORDER BY item_id, surat_izin_id DESC;

 SELECT sti.item_id, item_name,item_code,sti.surat_izin_id,surat_izin,sum_qty_in, sum_qty_out,sum_qty_in - sum_qty_out + sum_qty_return as qty_av,qty_unit, sum_qty_return, sum_qty_out - sum_qty_return as sum_qty_use 
FROM ( 
SELECT MIN(ti.item_name) as item_name, sti.item_id ,MAX(ti.item_code) as item_code, MAX(si.surat_izin) as surat_izin, sti.surat_izin_id , 
SUM(qty_in) as sum_qty_in,MAX(qty_unit) as qty_unit 
FROM tcminv_stock_in sti 
RIGHT JOIN tcminv_item ti ON sti.item_id = ti.item_id 
RIGHT JOIN tcminv_surat_izin si ON sti.surat_izin_id = si.surat_izin_id 
INNER JOIN tcminv_qty_unit qu ON ti.qty_unit_id = qu.qty_unit_id 
WHERE sti.surat_izin_id = 3 GROUP BY sti.item_id, sti.surat_izin_id ) sti LEFT JOIN ( SELECT item_id, surat_izin_id, SUM(qty_out) as sum_qty_out FROM tcminv_stock_out WHERE surat_izin_id = 3 GROUP BY item_id, surat_izin_id) sto ON (sti.item_id = sto.item_id ) LEFT JOIN ( SELECT item_id, surat_izin_id, SUM(qty_return) as sum_qty_return FROM tcminv_stock_return WHERE surat_izin_id = 3 GROUP BY item_id, surat_izin_id) rtn ON (sti.item_id = rtn.item_id) -- USING ( item_id, surat_izin_id) ORDER BY item_id, surat_izin_id DESC;

SELECT MAX(item_name) FROM tcminv_stock_in sti
LEFT JOIN tcminv_item ti on sti.item_id = ti.item_id
WHERE sti.surat_izin_id = 3
GROUP BY sti.item_id

SELECT sto.item_id as item_id, MAX(item_name) as item_name, MAX(item_code) as item_code, MAX(qty_unit) as qty_unit, MAX(sto.surat_izin_id) as surat_izin_id, COALESCE (MAX(sto.total_out) - MAX(str.total_return)) as total_use 
FROM tcminv_item ti 
INNER JOIN tcminv_qty_unit qu ON qu.qty_unit_id = ti.qty_unit_id 
INNER JOIN ( 
SELECT item_id, MAX(sto.surat_izin_id) as surat_izin_id, MAX(sto.contractor_id) as contractor_id, SUM(qty_out) AS "total_out" 
FROM tcminv_stock_out sto WHERE sto.surat_izin_id =3 AND to_char(date_out, 'MM-YY') ='09-14' GROUP BY item_id ) as sto on (sto.item_id = ti.item_id) LEFT JOIN ( 
SELECT item_id, SUM(qty_return) AS "total_return" 
FROM tcminv_stock_return str GROUP BY item_id ) as str on (sto.item_id = str.item_id) 
GROUP BY sto.item_id ORDER BY 1

SELECT sto.item_id as item_id, COALESCE (MAX(sto.total_out) - MAX(str.total_return)) as total_use, MAX(sti.total_in) as total_in, 
COALESCE (MAX(sti.total_in) - MAX(sto.total_out) - MAX(str.total_return)) as stock_awal 
FROM tcminv_item ti 
INNER JOIN tcminv_qty_unit qu ON qu.qty_unit_id = ti.qty_unit_id 
INNER JOIN ( 
SELECT item_id, MAX(sti.surat_izin_id) as surat_izin_id, SUM(qty_in) AS "total_in" 
FROM tcminv_stock_in sti WHERE sti.surat_izin_id =3 AND to_char(date_in, 'MM-YY') <='2014-08' GROUP BY item_id ) as sti on (sti.item_id = ti.item_id) 
INNER JOIN ( SELECT item_id, MAX(sto.surat_izin_id) as surat_izin_id, MAX(sto.contractor_id) as contractor_id, SUM(qty_out) AS "total_out" 
FROM tcminv_stock_out sto WHERE sto.surat_izin_id =3 AND to_char(date_out, 'MM-YY') <='2014-08' GROUP BY item_id ) as sto on (sto.item_id = ti.item_id) 
LEFT JOIN ( SELECT item_id, SUM(qty_return) AS "total_return" FROM tcminv_stock_return str GROUP BY item_id ) as str on (sto.item_id = str.item_id) 
GROUP BY sto.item_id ORDER BY 1

SELECT sum(qty_in)
FROM tcminv_stock_in 
WHERE to_char(date_in, 'MM-YY') <='09-14'

SELECT sto.item_id as item_id, MAX(item_name) as item_name, MAX(item_code) as item_code, MAX(qty_unit) as qty_unit, MAX(sto.surat_izin_id) as surat_izin_id, 
COALESCE (MAX(sto.total_out) - MAX(str.total_return)) as total_use, MAX(sti.total_in) as total_in, 
MAX(sta.stock_awal) as stock_awal 
FROM tcminv_item ti 
INNER JOIN tcminv_qty_unit qu ON qu.qty_unit_id = ti.qty_unit_id 
LEFT JOIN ( 
							--stockawal 
				SELECT sto.item_id as item_id, COALESCE (MAX(sto.total_out) - MAX(str.total_return),0) as total_use, MAX(sti.total_in) as total_in, 
								MAX(sto.total_out) as total_out, MAX(str.total_return) as total_return,
								COALESCE (MAX(sti.total_in) - COALESCE(MAX(sto.total_out) - MAX(str.total_return),0),0) as stock_awal 
								FROM tcminv_item ti INNER JOIN tcminv_qty_unit qu ON qu.qty_unit_id = ti.qty_unit_id 
								INNER JOIN ( SELECT item_id, MAX(sti.surat_izin_id) as surat_izin_id, SUM(qty_in) AS "total_in" FROM tcminv_stock_in sti WHERE sti.surat_izin_id =3 AND to_char(date_in, 'MM-YY') <='08-14' GROUP BY item_id ) as sti on (sti.item_id = ti.item_id) 
								LEFT JOIN ( SELECT item_id, MAX(sto.surat_izin_id) as surat_izin_id, MAX(sto.contractor_id) as contractor_id, SUM(qty_out) AS "total_out" 
																					FROM tcminv_stock_out sto WHERE sto.surat_izin_id =3 AND to_char(date_out, 'MM-YY') <='08-14' GROUP BY item_id ) as sto on (sto.item_id = ti.item_id) 
								LEFT JOIN ( SELECT item_id, SUM(qty_return) AS "total_return" FROM tcminv_stock_return str GROUP BY item_id ) as str on (sto.item_id = str.item_id) 
								GROUP BY sto.item_id
) as sta on (sta.item_id = ti.item_id) 
LEFT JOIN ( 
								SELECT item_id, MAX(sti.surat_izin_id) as surat_izin_id, SUM(qty_in) AS "total_in" 
								FROM tcminv_stock_in sti WHERE sti.surat_izin_id =3 AND to_char(date_in, 'MM-YY') ='09-14' GROUP BY item_id 
) as sti on (sti.item_id = ti.item_id) 
LEFT JOIN ( 
								SELECT item_id, MAX(sto.surat_izin_id) as surat_izin_id, MAX(sto.contractor_id) as contractor_id, SUM(qty_out) AS "total_out" 
								FROM tcminv_stock_out sto WHERE sto.surat_izin_id =3 AND to_char(date_out, 'MM-YY') ='09-14' GROUP BY item_id 
) as sto on (sto.item_id = ti.item_id) 
LEFT JOIN ( 
								SELECT item_id, SUM(qty_return) AS "total_return" FROM tcminv_stock_return str GROUP BY item_id 
) as str on (sto.item_id = str.item_id) 
GROUP BY sto.item_id ORDER BY 1


SELECT sti.item_id as item_id, MAX(item_name) as item_name, MAX(item_code) as item_code, MAX(qty_unit) as qty_unit, MAX(sto.surat_izin_id) as surat_izin_id, COALESCE (MAX(sto.total_out) - MAX(str.total_return),0) as total_use, MAX(sti.total_in) as total_in, MAX(sta.stock_awal) as stock_awal, COALESCE(MAX(sta.stock_awal) + MAX(sti.total_in) - COALESCE (MAX(sto.total_out) - MAX(str.total_return),0),0) as stock_akhir FROM tcminv_item ti INNER JOIN tcminv_qty_unit qu ON qu.qty_unit_id = ti.qty_unit_id LEFT JOIN ( SELECT sto.item_id as item_id, COALESCE (MAX(sto.total_out) - MAX(str.total_return),0) as total_use, MAX(sti.total_in) as total_in, COALESCE (MAX(sti.total_in) - COALESCE(MAX(sto.total_out) - MAX(str.total_return),0),0) as stock_awal FROM tcminv_item ti INNER JOIN tcminv_qty_unit qu ON qu.qty_unit_id = ti.qty_unit_id INNER JOIN ( SELECT item_id, MAX(sti.surat_izin_id) as surat_izin_id, SUM(qty_in) AS "total_in" FROM tcminv_stock_in sti WHERE sti.surat_izin_id =3 AND to_char(date_in, 'MM-YY') ='08-14' GROUP BY item_id ) as sti on (sti.item_id = ti.item_id) LEFT JOIN ( SELECT item_id, MAX(sto.surat_izin_id) as surat_izin_id, MAX(sto.contractor_id) as contractor_id, SUM(qty_out) AS "total_out" FROM tcminv_stock_out sto WHERE sto.surat_izin_id =3 AND to_char(date_out, 'MM-YY') ='08-14' GROUP BY item_id ) as sto on (sto.item_id = ti.item_id) LEFT JOIN ( SELECT item_id, SUM(qty_return) AS "total_return" FROM tcminv_stock_return str GROUP BY item_id ) as str on (sto.item_id = str.item_id) GROUP BY sto.item_id ORDER BY 1 ) as sta on (sta.item_id = ti.item_id) INNER JOIN ( SELECT item_id, MAX(sti.surat_izin_id) as surat_izin_id, SUM(qty_in) AS "total_in" FROM tcminv_stock_in sti WHERE sti.surat_izin_id =3 AND to_char(date_in, 'MM-YY') ='09-14' GROUP BY item_id ) as sti on (sti.item_id = ti.item_id) LEFT JOIN ( SELECT item_id, MAX(sto.surat_izin_id) as surat_izin_id, MAX(sto.contractor_id) as contractor_id, SUM(qty_out) AS "total_out" FROM tcminv_stock_out sto WHERE sto.surat_izin_id =3 AND to_char(date_out, 'MM-YY') ='09-14' GROUP BY item_id ) as sto on (sto.item_id = ti.item_id) LEFT JOIN ( SELECT item_id, SUM(qty_return) AS "total_return" FROM tcminv_stock_return str GROUP BY item_id ) as str on (sto.item_id = str.item_id) GROUP BY sti.item_id ORDER BY 1

SELECT * FROM tcminv_stock_in 
WHERE item_id =3
	AND to_char(date_in,'MM-YY') ='08-14' 
ORDER BY date_in, item_id ASC;

SELECT * FROM tcminv_stock_out 
WHERE item_id = 20 OR item_id =21
	AND to_char(date_out ,'MM-YY') ='09-14' 
ORDER BY date_out, item_id ASC;

SELECT * FROM tcminv_stock_return 
WHERE item_id = 20 OR item_id =21
	AND to_char(date_return, 'MM-YY') ='09-14' 
ORDER BY date_return, item_id ASC;

DELETE FROM tcminv_stock_in
WHERE date_in = '2014-09-23'


SELECT item_id,SUM(qty_in)
FROM tcminv_stock_in
WHERE surat_izin_id=7
GROUP BY item_id

SELECT item_id,SUM(qty_out)
FROM tcminv_stock_out
WHERE surat_izin_id=7
GROUP BY item_id

SELECT item_id,SUM(qty_return)
FROM tcminv_stock_return
WHERE surat_izin_id=7
GROUP BY item_id


<?php

@session_start();

function define_sess($username, $user_id, $full_name, $level) {
	$_SESSION['tcm_username'] = $username;
	$_SESSION['tcm_user_id'] = $user_id;
	$_SESSION['tcm_full_name'] = $full_name;
	$_SESSION['tcm_level'] = $level;
	define('USERNAME', $username);
	define('USER_ID', $user_id);
	define('FULLNAME', $full_name);
	// echo USERNAME;exit;
	// print_r($_SESSION);exit;
}

function previous_url() {
	if ($_SESSION['last_url']) {
		return header('Location: ' . $_SESSION['last_url']);
	} elseif ($_SESSION['last_url'] == 'logout') {
		echo 'bad';
	} else {
		return redirect('inv');
	}
}

function datef($date){
	// 	$now = date('Y-m-d H:i:s');
	return date("D M d y h:i:s A", strtotime($date));
}

function datepickerf($date) {
	return date("Y-m-d", strtotime($date));
}

function getYearMonth($date) {
	return date("y-m", strtotime($date));
}

function indoDate($date) {
	return $newDate = date("d/m/y", strtotime($date));
}

function getQurater($month_year){
	$curMonth = DateTime::createFromFormat('m-y', $month_year);
	$monthFormat = new DateTime($curMonth->format('Y-m'));
	$monthFormat = $monthFormat->format('m-y');
	$quarter = ceil($monthFormat/3);
	return $quarter;
}

function getFirstQuarterMonth($quarter) {
	if($quarter ==1) return 01;
	if($quarter ==2) return 04;
	if($quarter ==3) return 07;
	return 10;
}

function getMonthAgo($ago) {
	return date("Y-m-d", strtotime( date( "Y-m-d", strtotime( date("Y-m-d") ) ) . "-$ago month" ) );
}

function where_si($type,$si) {
	return ' WHERE surat_izin_id ='.$si;
}

function where_date($type,$mm_yy) {
	return ' AND to_char(date_'.$type.', \'MM-YY\') = \''.$mm_yy.'\'';
}

function where_date_before($type,$mm_yy) {
	return ' AND to_char(date_'.$type.', \'MM-YY\') <= \''.$mm_yy.'\'';
}


function getDateBetween($date, $between) {
	return date("Y-m-d", strtotime("$between months", strtotime(date("Y-m-d"), strtotime(date($date)))));
}

function getMonthNameAgo($ago) {
	return date("F", strtotime("$ago months", strtotime(date("Y-m"), strtotime(date(date("Y-m"))))));
	}

function getMonthYearBetween($month_year,$between) {
	$date = DateTime::createFromFormat('m-y', $month_year);
	$new_date = new DateTime($date->format('Y-m'));
	$between_m = str_replace('-', '', $between);
	$between_p = str_replace('+', '', $between);
// 	echo $between_m;
	if(!empty($between)) {
		if(strpos($between, '-') !== false) {
			$new_date->sub(new DateInterval('P'.abs($between).'M'));
			return $new_date->format('m-y');
		} else {
			$new_date->sub(new DateInterval('P'.abs($between).'M'));
			return $new_date->format('m-y');
		}
	} else {
		return $month_year;
	}
}

function incrementDate($startDate, $monthIncrement = 0) {

	$startingTimeStamp = $startDate->getTimestamp();
	// Get the month value of the given date:
	$monthString = date('m-y', $startingTimeStamp);
	// Create a date string corresponding to the 1st of the give month,
	// making it safe for monthly calculations:
	$safeDateString = "first day of $monthString";
	// Increment date by given month increments:
	$incrementedDateString = "$safeDateString $monthIncrement month";
	$newTimeStamp = strtotime($incrementedDateString);
	$newDate = DateTime::createFromFormat('U', $newTimeStamp);
	return $newDate;
}

function getAllDate($month, $year){
	$list=array();

	for($d=1; $d<=31; $d++)
	{
	$time=mktime(12, 0, 0, $month, $d, $year);
	if (date('m', $time)==$month)
		$list[]=date('Y-m-d', $time);
	}
// 	echo "<pre>";
// 		print_r($list);
// 	echo "</pre>";
	return $list;
}

function safeDate($date){
	$dates = str_replace("-", "_", $date);
	return 'd'.$dates;
}

function safeDateAdd($date,$add){
	$dates = str_replace("-", "_", $date);
	return 'd'.$dates."_".$add;
}

function getYearMonthLong($date) {
	return date("M-y", strtotime($date));
	
}
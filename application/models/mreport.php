<?php
/**
 * @author haidar
 * By Haidar Mar'ie Email = coder5@ymail.com 
 *
 */
class MReport extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	function getAllDaily($contractor=NULL, $si=NULL) {
		$sql = "SELECT item_id,
		        SUM(CASE date_in  WHEN '2014-06-20' THEN qty_in ELSE 0 END) AS  \"20\",
		        SUM(CASE date_in   WHEN '2014-06-19' THEN qty_in ELSE 0 END) AS \"19\"
				FROM {PRE}stock_in
				GROUP BY item_id
				ORDER BY 1;";
		$query = $this->db->query($sql);
		return $query->result();
	}
	
	function pivotDailyStockIn($dates, $si=NULL, $month = NULL) {
		$case = "";
		foreach($dates as $date) {
			$case .= "SUM(CASE date_in  WHEN '".$date."' THEN qty_in ELSE 0 END) AS  \"".$date."\",";
		}
		$case .= "SUM(CASE to_char(date_in, 'YY-MM') WHEN '".getYearMonth($dates[0])."' THEN qty_in ELSE 0 END) AS \"Total\"";
		//$cases = rtrim($case, ",");
		$where_si = !empty($si) ? ' WHERE surat_izin_id ='.$si : " ";
		$where_month = !empty($month) ? ' AND to_char(date_in, \'MM-YY\')  =\''.$month.'\'' : " ";
		$sql = "SELECT tsi.item_id,MAX(item_name) as item_name, MAX(item_code) as item_code,
				MAX(qty_unit) as qty_unit,
		        ".$case."
				FROM {PRE}stock_in tsi
		        INNER JOIN {PRE}item ti ON ti.item_id = tsi.item_id	
		        INNER JOIN {PRE}qty_unit qu ON qu.qty_unit_id = ti.qty_unit_id
		        ".$where_si."			
		        ".$where_month."			
				GROUP BY tsi.item_id
				ORDER BY 1;";
		$query = $this->db->query($sql);
		//////$this->firephp->log($this->db->last_query());
		return $query->result();
	}
	
	/**
	 * Stock Use
	 * @param unknown $dates
	 * @param string $si
	 * @param string $month
	 * @param string $contractor
	 */
	function pivotDailyStockUse($dates, $si=NULL, $month = NULL, $contractor= NULL) {
		$case_out = "";
		foreach($dates as $date) {
			$case_out .= "SUM(CASE date_out  WHEN '".$date."' THEN qty_out ELSE 0 END) AS  \"".$date."\",";
		}
		//$case_out = rtrim($case_out, ",");
		$case_out .= "SUM(CASE to_char(date_out, 'YY-MM') WHEN '".getYearMonth($dates[0])."' THEN qty_out ELSE 0 END) AS \"total_out\"";
		# For Case Return
		$case_return = "";
		foreach($dates as $date) {
			$case_return .= "SUM(CASE date_return  WHEN '".$date."' THEN qty_return ELSE 0 END) AS  \"".$date."\",";
		}
		//$case_return = rtrim($case_return, ",");
		$case_return .= "SUM(CASE to_char(date_return, 'YY-MM') WHEN '".getYearMonth($dates[0])."' THEN qty_return ELSE 0 END) AS \"total_return\"";
	
		$where_si = !empty($si) ? ' WHERE sto.surat_izin_id ='.$si : " ";
		$where_month = !empty($month) ? ' AND to_char(date_out, \'MM-YY\') =\''.$month.'\'' : " ";
		$where_contractor = !empty($contractor) ? ' AND sto.contractor_id =\''.$contractor . '\'' : " ";
		// Coalesce
		$coalesce = "";
		foreach ($dates as $date) {
			$coalesce .= 'COALESCE (MAX(sto."'.$date.'") - MAX(str."'.$date.'")) as "'.$date.'", ';
		}
		$coalesce .= ' COALESCE (MAX(sto.total_out) - MAX(str.total_return)) as total_use';
		$sql = 'SELECT
				sto.item_id as item_id, MAX(item_name) as item_name, MAX(item_code) as item_code,
				MAX(qty_unit) as qty_unit, MAX(sto.surat_izin_id) as surat_izin_id,
				'.$coalesce.'
				FROM {PRE}item ti
				INNER JOIN {PRE}qty_unit qu ON qu.qty_unit_id = ti.qty_unit_id
				INNER JOIN (
				SELECT item_id, MAX(sto.surat_izin_id) as surat_izin_id, MAX(sto.contractor_id) as contractor_id,
				'.$case_out.'
				FROM  {PRE}stock_out sto
				'.$where_si.'
		        '.$where_month.'
        		'.$where_contractor.'
				GROUP BY item_id
				) as sto on (sto.item_id = ti.item_id)
				LEFT JOIN (
				SELECT item_id,
				'.$case_return.'
				FROM  {PRE}stock_return str
				GROUP BY item_id
				) as str on (sto.item_id = str.item_id)
				GROUP BY sto.item_id
				ORDER BY 1';
		$query = $this->db->query($sql);
		//$this->firephp->log('stock_use='.$this->db->last_query());
		return $query->result();
	}
	
	/**
	 * Stock Use Monthly
	 * @param unknown $dates
	 * @param string $si
	 * @param string $month
	 * @param string $contractor
	 */
	function pivotMonthlyStockUse($month, $si=NULL, $contractor= NULL) {
		# For Case Return
		$where_si = !empty($si) ? ' WHERE surat_izin_id ='.$si : " ";
		$where_contractor = !empty($contractor) ? ' AND sto.contractor_id =\''.$contractor . '\'' : " ";
		// Coalesce
		$coalesce = ' COALESCE (MAX(sto.total_out) - MAX(str.total_return),0) as total_use';
		$sql = 'SELECT
				sti.item_id as item_id, MAX(item_name) as item_name, MAX(item_code) as item_code,
				MAX(qty_unit) as qty_unit,
				MAX(sti.total_in) as total_in, '.$coalesce.', 
				COALESCE(MAX(sta.stock_awal),0) as stock_awal,
				COALESCE(COALESCE(MAX(sta.stock_awal),0) + MAX(sti.total_in) - COALESCE (MAX(sto.total_out) - MAX(str.total_return),0),0) as stock_akhir
				FROM {PRE}item ti
				INNER JOIN {PRE}qty_unit qu ON qu.qty_unit_id = ti.qty_unit_id
				LEFT JOIN (
				'.$this->getStockLastMonth($month,$si).'
				) as sta on (sta.item_id = ti.item_id)
				INNER JOIN (
				SELECT item_id, 
				SUM(qty_in) AS "total_in"
				FROM  {PRE}stock_in sti
				'.$where_si.'
		        '.where_date('in', $month).'
				GROUP BY item_id
				) as sti on (sti.item_id = ti.item_id)		
				LEFT JOIN (
				SELECT item_id, 
				SUM(qty_out) AS "total_out"
				FROM  {PRE}stock_out sto
				'.$where_si.'
		        '.where_date('out', $month).'
        		'.$where_contractor.'
				GROUP BY item_id
				) as sto on (sto.item_id = ti.item_id)
				LEFT JOIN (
				SELECT item_id,
				SUM(qty_return) AS "total_return"
				FROM  {PRE}stock_return str
				'.$where_si.'
		        '.where_date('return', $month).'
        		'.$where_contractor.'
				GROUP BY item_id
				) as str on (sto.item_id = str.item_id)
				GROUP BY sti.item_id
				ORDER BY 1';
		$query = $this->db->query($sql);
		// //$this->firephp->log('montly_'.$month.'='.$this->db->last_query());
		return $query->result();
	}
	
	function getStockLastMonth($month, $si=NULL, $contractor= NULL){
		$month_ago = getMonthYearBetween($month, -1);
		$where_si = !empty($si) ? ' WHERE surat_izin_id ='.$si : " ";
		$where_contractor = !empty($contractor) ? ' AND sto.contractor_id =\''.$contractor . '\'' : " ";
		// Coalesce
		$coalesce = ' COALESCE (MAX(sto.total_out) - MAX(str.total_return),0) as total_use';
		$sql = '--stock_awal 
				SELECT
				sto.item_id as item_id,
				MAX(sti.total_in) as total_in, '.$coalesce.',
				COALESCE (MAX(sti.total_in) - COALESCE(MAX(sto.total_out) - MAX(str.total_return),0),0) as stock_awal
				FROM {PRE}item ti
				INNER JOIN {PRE}qty_unit qu ON qu.qty_unit_id = ti.qty_unit_id
				INNER JOIN (
				SELECT item_id, 
				SUM(qty_in) AS "total_in"
				FROM  {PRE}stock_in sti
				'.$where_si.'
		        '.where_date_before('in', $month_ago).'
				GROUP BY item_id
				) as sti on (sti.item_id = ti.item_id)		
				LEFT JOIN (
				SELECT item_id,
				SUM(qty_out) AS "total_out"
		        FROM  {PRE}stock_out sto
				'.$where_si.'
		        '.where_date_before('out', $month_ago).'
        		'.$where_contractor.'
				GROUP BY item_id
				) as sto on (sto.item_id = ti.item_id)
				LEFT JOIN (
				SELECT item_id,
				SUM(qty_return) AS "total_return"
				FROM  {PRE}stock_return str
				'.$where_si.'
		        '.where_date_before('return', $month_ago).'
        		'.$where_contractor.'
				GROUP BY item_id
				) as str on (sto.item_id = str.item_id)
				GROUP BY sto.item_id
				ORDER BY 1 --stock_awal_end
				';
		return $sql;
	}
	
	function pivotDailyStock($type, $dates, $si=NULL, $month_year = NULL, $contractor=NULL) {
		$case = "";
		foreach($dates as $date) {
			$case .= "SUM(CASE date_".$type."  WHEN '".$date."' THEN qty_".$type." ELSE 0 END) AS  \"".$date."\",";
		}
// 		$cases = rtrim($case, ",");
		$case .= "SUM(CASE to_char(date_".$type.", 'YY-MM') WHEN '".getYearMonth($dates[0])."' THEN qty_".$type." ELSE 0 END) AS \"Total\"";
		$where_si = !empty($si) ? ' WHERE surat_izin_id ='.$si : " ";
		$where_month = !empty($month_year) ? ' AND to_char(date_'.$type.', \'MM-YY\') =\''.$month_year.'\'' : " ";
		$where_contractor = !empty($contractor) ? ' AND contractor_id =\''.$contractor . '\'' : " ";
		$sql = "SELECT st.item_id,MAX(item_name) as item_name, MAX(item_code) as item_code,\r\n
				MAX(qty_unit) as qty_unit,\r\n
		        ".$case."
				FROM {PRE}stock_".$type." st
		        INNER JOIN {PRE}item ti ON ti.item_id = st.item_id
		        INNER JOIN {PRE}qty_unit qu ON qu.qty_unit_id = ti.qty_unit_id
        		".$where_si."			
		        ".$where_month."	
        		".$where_contractor."			
				GROUP BY st.item_id
				ORDER BY 1;";
		$query = $this->db->query($sql);
		//////$this->firephp->log($this->db->last_query());
		return $query->result();
	}
	
	/**
	 * Pivot Daily Category Stock In / Out / Return
	 * @param unknown $type
	 * @param unknown $dates
	 * @param string $si
	 * @param string $month_year
	 * @param string $contractor
	 */
	function pivotDailyCatStock($type,$dates,$si= NULL, $month_year=NULL, $contractor=NULL) {
		$cats = $this->getAllCategories();
		$case_type = "";
		foreach($dates as $date) {
			foreach ($cats as $cat) {
			$case_type .= "SUM(CASE WHEN (date_".$type." ='".$date."' AND ti.category_id='".$cat->category_id."') THEN qty_".$type." ELSE 0 END) AS  \"".safeDateAdd($date,$cat->category_id)."\",";
			}
		}
		$cases_type = rtrim($case_type, ",");
// 		$case_type .= "SUM(CASE to_char(date_".$type.", 'YY-MM') WHEN '".getYearMonth($dates[0])."' THEN qty_".$type." ELSE 0 END) AS \"total_".$type."\"";
		$where_si = !empty($si) ? ' WHERE st.surat_izin_id ='.$si : " ";
		$where_month = !empty($month_year) ? ' AND to_char(date_'.$type.', \'MM-YY\') =\''.$month_year.'\'' : " ";
		$case_cat = "";
		foreach ($cats as $cat) {
			$case_cat .= "SUM(CASE ti.category_id WHEN '".$cat->category_id."' THEN qty_".$type." ELSE 0 END) AS \"".$cat->category_name."\",";
		}
		//$cases = rtrim($case, ",");
		$sql = "SELECT 
				".$case_cat."
				".$cases_type."
				FROM {PRE}stock_".$type." st
				INNER JOIN {PRE}item ti ON ti.item_id = st.item_id
				".$where_si."
				".$where_month."
				ORDER BY 1;";
		$query = $this->db->query($sql);
// 		die($this->db->last_query());
		//////$this->firephp->log($this->db->last_query());
		return $query->result();
	}
	
	/**
	 * Pivot Daily Category Stock Use
	 * @param unknown $dates
	 * @param string $si
	 * @param string $month_year
	 * @param string $contractor
	 */
	function pivotDailyCatStockUse($dates,$si= NULL, $month_year=NULL, $contractor=NULL) {
		$cats = $this->getAllCategories();
		$case_out = "";
		foreach($dates as $date) {
			foreach($cats as $cat) {
				$case_out .= "SUM(CASE WHEN (date_out ='".$date."' AND ti.category_id='".$cat->category_id."') THEN qty_out ELSE 0 END) AS  \"".safeDateAdd($date,$cat->category_id)."\",";
			}
		}
		foreach($cats as $cat) {
			$case_out .= "SUM(CASE  WHEN (to_char(date_out, 'YY-MM') = '".getYearMonth($dates[0])."' AND ti.category_id='".$cat->category_id."') THEN qty_out ELSE 0 END) AS \"".$cat->category_name."\",";
		}
		$case_out = rtrim($case_out, ",");
		# For Case Return
		$case_return = "";
		foreach($dates as $date) {
			foreach($cats as $cat) {
				$case_return .= "SUM(CASE WHEN (date_return ='".$date."' AND ti.category_id='".$cat->category_id."') THEN qty_return ELSE 0 END) AS  \"".safeDateAdd($date,$cat->category_id)."\",";
			}
		}
		foreach($cats as $cat) {
			$case_return .= "SUM(CASE  WHEN (to_char(date_return, 'YY-MM') = '".getYearMonth($dates[0])."' AND ti.category_id='".$cat->category_id."') THEN qty_return ELSE 0 END) AS \"".$cat->category_name."\",";
		}
		$case_return = rtrim($case_return, ",");
		$where_si = !empty($si) ? ' WHERE sto.surat_izin_id ='.$si : " ";
		$where_month = !empty($month_year) ? ' AND to_char(date_out, \'MM-YY\') =\''.$month_year.'\'' : " ";
		$where_contractor = !empty($contractor) ? ' AND contractor_id =\''.$contractor . '\'' : " ";
		
		$coalesce = "";
		foreach ($dates as $date) {
			foreach($cats as $cat) {
				$coalesce .= 'COALESCE (MAX(sto."'.safeDateAdd($date,$cat->category_id).'") - MAX(str."'.safeDateAdd($date,$cat->category_id).'")) as "'.safeDateAdd($date,$cat->category_id).'", ';
			}
		}
		foreach($cats as $cat) {
			$coalesce .= ' COALESCE (MAX(sto."'.$cat->category_name.'") - MAX(str."'.$cat->category_name.'")) as "'.$cat->category_name.'",';
		}
		$coalesce = rtrim($coalesce, ",");
		$sql = 'SELECT
				'.$coalesce.'
				FROM {PRE}item ti
				INNER JOIN (
				SELECT category_id, MAX(sto.contractor_id) as contractor_id,
				'.$case_out.'
				FROM {PRE}stock_out sto
				INNER JOIN tcminv_item ti ON sto.item_id = ti.item_id
				'.$where_si.'
		        '.$where_month.'
        		'.$where_contractor.'
				GROUP BY category_id
				) as sto on (sto.category_id = ti.category_id)
				LEFT JOIN (
				SELECT category_id, 
				'.$case_return.'
				FROM  {PRE}stock_return str
				INNER JOIN tcminv_item ti ON str.item_id = ti.item_id
				GROUP BY category_id
				) as str on (sto.category_id = str.category_id)
				ORDER BY 1';
		$query = $this->db->query($sql);
// 		die($this->db->last_query());
		//////$this->firephp->log($this->db->last_query());
		return $query->result();
	}
	
	function pivotCatStockIn($dates,$si= NULL, $month_year=NULL, $contractor=NULL) {
		$case_in = "";
		foreach($dates as $date) {
			$case_in .= "SUM(CASE date_in  WHEN '".$date."' THEN qty_in ELSE 0 END) AS  \"".$date."\",";
		}
		$case_in .= "SUM(CASE to_char(date_in, 'YY-MM') WHEN '".getYearMonth($dates[0])."' THEN qty_out ELSE 0 END) AS \"total_in\"";
		$cats = $this->getAllCategories();
		$where_si = !empty($si) ? ' WHERE sti.surat_izin_id ='.$si : " ";
		$where_month = !empty($month) ? ' AND to_char(date_in, \'MM-YY\') =\''.$month.'\'' : " ";
		$case = "";
		foreach ($cats as $cat) {
			$case .= "SUM(CASE ti.category_id WHEN '".$cat->category_id."' THEN qty_in ELSE 0 END) AS \"".$cat->category_name."\",";
		}
		$cases = rtrim($case, ",");
		$sql = "SELECT date_in, 
				".$cases."
				".$case_in."
				FROM {PRE}stock_in sti
				INNER JOIN {PRE}item ti ON ti.item_id = sti.item_id
				INNER JOIN {PRE}category tc ON tc.category_id = ti.category_id
				".$where_si."		
				".$where_month."		
				GROUP BY date_in
				ORDER BY date_in;";
		$query = $this->db->query($sql);
		//////$this->firephp->log($this->db->last_query());
		return $query->result();
	}

	function pivotCatStockOut($dates,$si= NULL, $month_year=NULL, $contractor=NULL) {
		$case_out = "";
		foreach($dates as $date) {
			$case_out .= "SUM(CASE date_out  WHEN '".$date."' THEN qty_out ELSE 0 END) AS  \"".$date."\",";
		}
		$case_out .= "SUM(CASE to_char(date_out, 'YY-MM') WHEN '".getYearMonth($dates[0])."' THEN qty_out ELSE 0 END) AS \"total_out\"";
		$cats = $this->getAllCategories();
		$where_si = !empty($si) ? ' WHERE sto.surat_izin_id ='.$si : " ";
		$where_month = !empty($month) ? ' AND to_char(date_out, \'MM-YY\') =\''.$month.'\'' : " ";
		$case = "";
		foreach ($cats as $cat) {
			$case .= "SUM(CASE ti.category_id WHEN '".$cat->category_id."' THEN qty_out ELSE 0 END) AS \"".$cat->category_name."\",";
		}
		$cases = rtrim($case, ",");
		$sql = "SELECT date_out,
				".$cases."
				".$case_out."
				FROM {PRE}stock_out sto
				INNER JOIN {PRE}item ti ON ti.item_id = sto.item_id
				INNER JOIN {PRE}category tc ON tc.category_id = ti.category_id
				".$where_si."		
				".$where_month."
				GROUP BY date_out
				ORDER BY date_out;";
		$query = $this->db->query($sql);
		//////$this->firephp->log($this->db->last_query());
		return $query->result();
	}
	
	function pivotCatStockReturn($dates,$si= NULL, $month_year=NULL, $contractor=NULL) {
		$case_return = "";
		foreach($dates as $date) {
			$case_return .= "SUM(CASE date_return  WHEN '".$date."' THEN qty_return ELSE 0 END) AS  \"".$date."\",";
		}
		$case_return .= "SUM(CASE to_char(date_out, 'YY-MM') WHEN '".getYearMonth($dates[0])."' THEN qty_out ELSE 0 END) AS \"total_out\"";
		
		$cats = $this->getAllCategories();
		$where_si = !empty($si) ? ' WHERE str.surat_izin_id ='.$si : " ";
		$where_month = !empty($month) ? ' AND to_char(date_return, \'MM-YY\') =\''.$month.'\'' : " ";
		$case = "";
		foreach ($cats as $cat) {
			$case .= "SUM(CASE ti.category_id WHEN '".$cat->category_id."' THEN qty_return ELSE 0 END) AS \"".$cat->category_name."\",";
		}
		$cases = rtrim($case, ",");
		$sql = "SELECT date_return,
				".$cases."
				FROM {PRE}stock_return str
				INNER JOIN {PRE}item ti ON ti.item_id = str.item_id
				INNER JOIN {PRE}category tc ON tc.category_id = ti.category_id
				".$where_si."		
				".$where_month."
				GROUP BY date_return
				ORDER BY date_return;";
		$query = $this->db->query($sql);
		//////$this->firephp->log($this->db->last_query());
		return $query->result();
	}
	
	function getSuratIzin($si){
		$query = $this->db->get_where('surat_izin', array('surat_izin_id'=>$si));
		return $query->row();
	}
	
	function getAllDateIn($month=NULL) {
		$sql = "SELECT date_in FROM {PRE}stock_in
				GROUP BY date_in 
				ORDER BY date_in ASC;";
		$query = $this->db->query($sql);
		return $query->result();
	}

	function getAllDateOut($month=NULL) {
		$sql = "SELECT date_out FROM {PRE}stock_out
				GROUP BY date_out
				ORDER BY date_out ASC;";
		$query = $this->db->query($sql);
		return $query->result();
	}
	
	function getAllCategories() {
		$query = $this->db->get('category');
		return $query->result();
	}
	
	function getAllContractor() {
		$query = $this->db->get('contractor');
		return $query->result();
	}
	
	function getContractor($id) {
		$query = $this->db->get_where('contractor', array('contractor_id'=>$id));
		return $query->row();
	}
	
}
<?php
/*
 * By Haidar Mar'ie Email = coder5@ymail.com MGps
 */
class MStock extends CI_Model {

	function __construct() {
		parent::__construct();
	}
	
	function getAllItem() {
		$query = $this->db->get('item');
		return $query->result();
	}

	function getAllStock() {
		$this->db->join('stock_in', 'item.item_id = stock_in.item_id', 'left');
		$this->db->join('stock_out', 'stock_in.stock_in_id = stock_out.stock_in_id', 'left');
		$query = $this->db->get('item');
		return $query->result();
	}
	
	function getAllContractor(){
		$query = $this->db->get('contractor');
		return $query->result();
	}
	
	function getAllItemSIQty($si=NULL) {
		$this->db->select('item.item_id,item_name,item_code, SUM(qty_in) as sum_qty_in, SUM(qty_out) as sum_qty_out, SUM(qty_in) - SUM(qty_in) qty_av, 
				stock_in.surat_izin_id, surat_izin.surat_izin');
		if($si) {
			$this->db->where('stock_in.surat_izin_id',$si);
		}
		$this->db->join('stock_in', 'stock_in.item_id = item.item_id', 'inner');
		$this->db->join('surat_izin', 'surat_izin.surat_izin_id = stock_in.surat_izin_id', 'inner');
		$this->db->join('stock_out', 'stock_out.item_id = stock_out.item_id', 'left');
		$this->db->group_by('item.item_id, stock_in.surat_izin_id, stock_in.surat_izin_id, surat_izin.surat_izin');
		$query = $this->db->get('item');
		//$this->firephp->log($this->db->last_query());
		return $query;
	}
	
	function getAllStockSIQty($si=NULL){
		$where_sti = !empty($si)  ? " WHERE sti.surat_izin_id = $si " : "";
		$where = !empty($si)  ? " WHERE surat_izin_id = $si " : "";
		$sql = "SELECT sti.item_id, item_name,item_code,sti.surat_izin_id,surat_izin,sum_qty_in, 
				sum_qty_out,sum_qty_in - sum_qty_out + sum_qty_return as qty_av,qty_unit, sum_qty_return,
				sum_qty_out - sum_qty_return as sum_qty_use
				FROM (
					SELECT MIN(ti.item_name) as item_name, sti.item_id ,MAX(ti.item_code) as item_code, 
					MAX(si.surat_izin) as surat_izin, sti.surat_izin_id , SUM(qty_in) as sum_qty_in,MAX(qty_unit) as qty_unit
					FROM {PRE}stock_in sti
					RIGHT JOIN {PRE}item ti ON sti.item_id = ti.item_id
					RIGHT JOIN {PRE}surat_izin si ON sti.surat_izin_id = si.surat_izin_id
					INNER JOIN tcminv_qty_unit qu ON ti.qty_unit_id = qu.qty_unit_id
					".$where_sti." 
					GROUP BY sti.item_id, sti.surat_izin_id
					) sti
				LEFT JOIN (
					SELECT item_id, surat_izin_id, SUM(qty_out)  as sum_qty_out
					FROM {PRE}stock_out
					".$where." 
					GROUP BY item_id, surat_izin_id) sto 
				ON (sti.item_id = sto.item_id )
				LEFT JOIN ( 
					SELECT item_id, surat_izin_id, SUM(qty_return) as sum_qty_return 
					FROM {PRE}stock_return
					".$where." 
					GROUP BY item_id, surat_izin_id) rtn
				ON (sti.item_id = rtn.item_id)
				-- USING ( item_id, surat_izin_id)
				ORDER BY item_id, surat_izin_id DESC;";
		$query = $this->db->query($sql);
		//$this->firephp->log($this->db->last_query());
		return $query;
	}
	
	function sumPerCategory($cat_id){
// 		$this->
	}
	
	function getAllStockIn(){
		$this->db->join('stock_in', 'item.item_id = stock_in.item_id');
		return $this->db->get('item');
	}
	
	function getAllStockOut(){
		$this->db->join('stock_out', 'stock_in.stock_in_id = stock_out.stock_in_id', 'inner');
		return $this->db->get('stock_in');
	}
	
	function getStock($type, $item_id, $si, $contractor_id, $date) {
		$qty_type = 'qty_'.$type;
		$this->db->select_sum($qty_type);
		$query = $this->db->get_where('stock_'.$type, array('item_id'=> $item_id, 'surat_izin_id'=> $si,
				 'contractor_id'=>$contractor_id,'date_'.$type => $date));
		if ($query->num_rows() > 0) {
			$qty = $query->row();
// 			//$this->firephp->log($this->db->last_query());
			return $qty->$qty_type;
		} else {
			return 0;
		}
	}
	
// 	function getStock($type,$id) {
// 		$query = $this->db->get_where('stock_'.$type, array('stock_'.$type.'_id' => $id));
// 		return $query->row();
// 	}
	
	function insertStock($type,$data) {
		$this->db->insert('stock_'.$type, $data);
		//$this->firephp->log($this->db->last_query());
		return $this->db->insert_id();
	}
	
	function updateStock($type,$data,$id) {
		//$this->firephp->log($this->db->last_query());
		return $this->db->update('stock_'.$type, $data, array('stock_'.$type.'_id'=> $id));
	}
	
	public function insertStockSi($type,$si) {
		$items = $this->getAllItem();
		foreach($items as $item) {
			$data['surat_izin_id'] = $si;
			$data['item_id'] = $item->item_id;
			$data['qty_'.$type] = 0;
			$data['date_'.$type] = date('Y-m-d');
			$this->insertStock('in',$data);
		}
	}
	
	public function deleteStock($type, $item_id) {
		return $this->db->delete('stock_'.$type, array('item_id'=>$item_id));
	}
	
}	
	
<?php
/*
 * By Haidar Mar'ie Email = coder5@ymail.com MGps
 */
class MItem extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	function getAllItem() {
		$query = $this->db->get('item');
		return $query->result();
	}
	
	function addItem($data){
		$this->db->insert('item', $data);
		$data['item_id'] = $this->db->insert_id();
		$this->addEmptyStock($data);
		return $data['item_id'];
	}
	
	function addEmptyStock($data){
		$si_id = $this->getAllSIId();
		foreach($si_id as $si ) {
			$data_stock['surat_izin_id'] = $si->surat_izin_id;
			$data_stock['item_id'] = $data['item_id'];
			$data_stock['date_in'] = date('Y-m-d');
// 			$data_stock['user_id'] = $data['user_id'];
			$data_stock['qty_in'] = 0;
			$this->db->insert('stock_in', $data_stock);
		}
	}
	
	function updateItem($data,$item_id){
		return $this->db->update('item',$data, array('item_id'=> $item_id));
	}
	
	function getItem($id) {
		$this->db->join('qty_unit', 'qty_unit.qty_unit_id = item.qty_unit_id', 'inner');
		$query = $this->db->get_where('item', array('item_id'=> $id));
		return $query->row();
	}
	
	function deleteItem($id) {
		$this->db->delete('stock_in', array('item_id'=>$id));
		$this->db->delete('stock_out', array('item_id'=>$id));
		$this->db->delete('stock_return', array('item_id'=>$id));
		return $this->db->delete('item', array('item_id'=>$id));
	}
	
	function addCategory($data){
		$this->db->insert('category',$data);
		return $this->db->insert_id();
	}
	
	function updateCategory($data,$cat_id){
		return $this->db->update('category',$data, array('category_id'=> $cat_id));
	}
	
	function getCategory($id) {
		$query = $this->db->get_where('category', array('category_id'=> $id));
		return $query->row();
	}
	
	function getAllCategory(){
		return $this->db->get('category');
	}
	
	function getAllQtyUnit(){
		return $this->db->get('qty_unit');
	}
	
	function insertSuratIzin($data){
		$this->db->insert('surat_izin',$data);
		return $this->db->insert_id();
	}
	
	function updateSuratIzin($data,$cat_id){
		return $this->db->update('surat_izin',$data, array('surat_izin_id'=> $cat_id));
	}
	
	function getSuratIzin($id) {
		$query = $this->db->get_where('surat_izin', array('surat_izin_id'=> $id));
		return $query->row();
	}
	
	function getActiveSI() {
		$now = date('Y-m-d');
		$this->db->where('date_min <=', $now);
		$this->db->where('date_max >=', getMonthAgo(6));
		$this->db->order_by("create_at", "desc");
		$this->db->limit(1);
		$query = $this->db->get('surat_izin');
		// $this->firephp->log($this->db->last_query());
		return $query->row();
	}
	
	function getAllSuratIzin(){
		$query = $this->db->get('surat_izin');
		return $query->result();
	}
	
	function getAllSIId(){
		$this->db->select('surat_izin_id');
		$query = $this->db->get('surat_izin');
		return $query->result();
	}
	
}
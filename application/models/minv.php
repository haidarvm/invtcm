<?php
/*
 * By Haidar Mar'ie Email = coder5@ymail.com MGps
 */
class Minv extends CI_Model {

	function __construct() {
		parent::__construct();
	}
	
	function addContractor($data){
		$this->db->insert('contractor',$data);
		return $this->db->insert_id();
	}
	
	function updateContractor($data,$cont_id){
		return $this->db->update('contractor',$data, array('contractor_id'=> $cont_id));
	}
	
	function getContractor($id) {
		$query = $this->db->get_where('contractor', array('contractor_id'=> $id));
		return $query->row();
	}
	
	function getAllContractor(){
		return $this->db->get('contractor');
	}
}
<?php
class Inv extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->model('mitem');
		$this->load->model('minv');
		$this->load->model('mstock');
		$this->mitem = new MItem();
		$this->minv = new MInv();
		$this->mstock = new MStock();
	}
	
	public function index(){
		$this->list_item();
	}
	
	public function list_item(){
		$data['pageTitle'] = "List Item";
		$data['items'] = $this->mitem->getAllItem();
		$this->load->template('item/list_item', $data);
	}
	
	public function item($id=NULL){
		$data['pageTitle'] = "Item";
		$data['all_item'] = $this->mitem->getAllItem();
		$data['all_qty_unit'] = $this->mitem->getAllQtyUnit();
		$data['all_cat'] = $this->mitem->getAllCategory();
		$data['all_si'] = $this->mitem->getAllSuratIzin();
		$post = $this->input->post();
		if($post) {
			//print_r($post);exit;
			$post_data['item_name'] = $post['item_name'];
			$post_data['item_desc'] = $post['item_desc'];
// 			$post_data['surat_izin_id'] = $post['surat_izin_id'];
			$post_data['category_id'] = $post['category_id'];
			$post_data['item_code'] = $post['item_code'];
			$post_data['qty_unit_id'] = $post['qty_unit_id'];
// 			$post_data['qty'] = $post['qty'];
			if(!empty($post['item_id'])) {
				$this->mitem->updateItem($post_data, $post['item_id']);
				redirect('inv/item/'.$post['item_id']);
			} else {
				$last_id = $this->mitem->addItem($post_data);
				redirect('inv/item/'.$last_id);
			}
		} elseif($id) {
			$data['item'] = $this->mitem->getItem($id);
		}
		$this->load->template('item/item.php',$data);
	}
	
	public function delete_item($id=NULL)  {
		if($id) {
			$this->mitem->deleteItem($id);
		}
		return $this->item();
	}
	
	public function surat_izin($id=NULL) {
		$data['pageTitle'] = "Surat Izin";
		$data['all_si'] = $this->mitem->getAllSuratIzin();
		$post = $this->input->post();
		if($post) {
			//print_r($post);exit;
			$post_data['surat_izin'] = $post['surat_izin'];
			$post_data['surat_izin_desc'] = $post['surat_izin_desc'];
			$post_data['date_min'] =  date("Y-m-d", strtotime($post['date_min']));
			$post_data['date_max'] =  date("Y-m-d", strtotime($post['date_max']));

			if($post['surat_izin_id']) {
				$this->mitem->updateSuratIzin($post_data, $post['surat_izin_id']);
				redirect('inv/surat_izin/'.$post['surat_izin_id']);
			} else {
				$last_id = $this->mitem->insertSuratIzin($post_data);
// 				site_url('stock/insert_stock_si/'.$last_id);
				$this->mstock->insertStockSi('in', $last_id);
				redirect('inv/surat_izin/'.$last_id);
			}
		} elseif($id) {
			$data['si'] = $this->mitem->getSuratIzin($id);
		}
		$this->load->template('item/surat_izin.php',$data);
	}
	
	public function delete_si($id) {
	
	}
	
	public function category($id=NULL) {
		$data['pageTitle'] = "Category";
		$data['all_cat'] = $this->mitem->getAllCategory();
		$post = $this->input->post();
		if($post) {
			//print_r($post);exit;
			$post_data['category_name'] = $post['category_name'];
			$post_data['category_desc'] = $post['category_desc'];
			if($post['category_id']) {
				$this->mitem->updateCategory($post_data, $post['category_id']);
				redirect('inv/category/'.$post['category_id']);
			} else {
				$last_id = $this->mitem->addCategory($post_data);
				redirect('inv/category/'.$last_id);
			}
		} elseif($id) {
			$data['cat'] = $this->mitem->getCategory($id);
		}
		$this->load->template('item/category.php',$data);
	}
	
	public function delete_cat($id) {
		
	}
	
	public function contractor($id=NULL){
		$data['pageTitle'] = "Contractor";
		$data['all_cont'] = $this->minv->getAllContractor();
		$post = $this->input->post();
		if($post) {
			//print_r($post);exit;
			$post_data['contractor_name'] = $post['contractor_name'];
			$post_data['contractor_desc'] = $post['contractor_desc'];
	
			if($post['contractor_id']) {
				$this->minv->updateContractor($post_data, $post['contractor_id']);
				redirect('inv/contractor/'.$post['contractor_id']);
			} else {
				$last_id = $this->minv->addContractor($post_data);
				redirect('inv/contractor/'.$last_id);
			}
		} elseif($id) {
			$data['cont'] = $this->minv->getContractor($id);
		}
		$this->load->template('contractor.php',$data);
	}
	
	public function delete_contractor($id) {
		
	}
	
	public function get_si() {
		$si = $this->mitem->getAllSIId();
		print_r($si);
	}
	
	public function info(){
		echo CI_VERSION;
		phpinfo();
	}
	
}
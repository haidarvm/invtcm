<?php

class Stock extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->model('mitem');
		$this->load->model('mstock');
		$this->mitem = new MItem();
		$this->mstock = new MStock();
	}
	
	public function index(){
		$this->out_multi();
	}
	
	public function multi_date(){
		$data['pageTitle'] = "Stock Out Multiple";
		$post = $this->input->post();
		$si = $this->uri->segment(3);
		if(!empty($si)) {
			$data['si'] = $si;
		} else {
			$si = $this->mitem->getActiveSI();
			//die($si->surat_izin_id);
			//$si = NULL;
			$data['si'] = $si->surat_izin_id;
			$si = $data['si'];
		}
		$month_year = !empty($this->uri->segment(4)) ? $this->uri->segment(4) :  date('m-y');
		$month = explode("-", $month_year);
		// get Date 30 days before today
		$data['all_si'] = $this->mitem->getAllSuratIzin();
		$data['si_name'] = $this->mitem->getSuratIzin($si);
		$data['all_date'] = getAllDate($month[0], $month[1]);
		$data['all_contractor'] = $this->mstock->getAllContractor();
		$data['month'] = $month[0];
		$data['year'] = $month[1];
		$data['month_year'] = $month_year;
		$item_id = !empty($this->uri->segment(5)) ? $this->uri->segment(5) : "";
		if($post) {
			$this->firephp->log($post);
// 			print_r($post);exit;
			foreach($data['all_date'] as $date) {
				foreach($data['all_contractor'] as $contractor) {
					if (!empty($post['qty_return'] && !empty($post['qty_return'][$date][$contractor->contractor_id]))) {
// 						print_r($post['qty_return']['2014-09-01'][1]);
// 						exit;
						$post_data_return['item_id'] = $item_id;
						echo $date . ' qty return'.$post['qty_return'][$date][$contractor->contractor_id].'<br>';
						$post_data_return['date_return'] =  $date;
						$post_data_return['surat_izin_id'] = $post['surat_izin_id'];
						$post_data_return['qty_return'] = $post['qty_return'][$date][$contractor->contractor_id];
						$post_data_return['contractor_id'] = $contractor->contractor_id;
						$this->mstock->insertStock('return',$post_data_return);
					}
					if (!empty($post['qty_out'] && !empty($post['qty_out'][$date][$contractor->contractor_id]))) {
// 						print_r($post['qty_out']['2014-09-01'][1]);
// 						exit;
						$post_data_out['item_id'] = $item_id;
						echo $date . ' qty out'.$post['qty_out'][$date][$contractor->contractor_id].'<br>';
						$post_data_out['date_out'] =  $date;
						$post_data_out['surat_izin_id'] = $post['surat_izin_id'];
						$post_data_out['qty_out'] = $post['qty_out'][$date][$contractor->contractor_id];
						$post_data_out['contractor_id'] = $contractor->contractor_id;
						$this->mstock->insertStock('out',$post_data_out);
					} 
				}
			}
// 						exit;
			redirect('stock/multi_date/'.$post['surat_izin_id'].'/'.$post['month_year'].'/'.$post['item_id']);
		}
		$data['item'] = !empty($item_id) ? $this->mitem->getItem($item_id) : "";
		$data['all_item'] = $this->mitem->getAllItem();
		$this->load->template('stock/multi_date',$data);
	}
	
	public function multi_date_item(){
		$data['title'] =  "Choose item, default SI, default Month";
		echo $this->mstock->getStock('out', 20, 31, 2, '2014-09-03');
		$this->load->template('stock/item_choose',$data);
	}
	
	public function multi_all(){
		$data['pageTitle'] = "Stock Out Multiple";
		$data['all_si'] = $this->mitem->getAllSuratIzin();
		$data['all_contractors'] = $this->mstock->getAllContractor();
		$post = $this->input->post();
		$si = $this->uri->segment(3);
		if(!empty($si)) {
			$data['si'] = $si;
		} else {
			$si = $this->mitem->getActiveSI();
			//$si = NULL;
			$data['si'] = $si->surat_izin_id;
			$si = $data['si'];
		}
		if($post) {
			$this->firephp->log($post);
			//print_r($post);//exit;
// 			print_r($post);exit;
			foreach($post['item_id'] as $item_id) {
				if(!empty($post['qty_out']) && !empty($post['qty_out'][$item_id])) {
					//echo 'masuk';exit;
					$post_data_out['surat_izin_id'] = $post['surat_izin_id'];
					$post_data_out['contractor_id'] = $post['contractor_id'];
					$post_data_out['item_id'] = $item_id;
					//echo $item_id . ' qty '.$post['qty_in'][$item_id].'<br>';
					$post_data_out['date_out'] = date("Y-m-d", strtotime($post['date']));
					//$post_data['surat_izin_id'] = $post['surat_izin_id'];
					$post_data_out['qty_out'] = $post['qty_out'][$item_id];
					$this->mstock->insertStock('out',$post_data_out);
					//exit;
				}
				if(!empty($post['qty_return']) && !empty($post['qty_return'][$item_id])) {
					//echo 'masuk';exit;
					$post_data_return['surat_izin_id'] = $post['surat_izin_id'];
					$post_data_return['contractor_id'] = $post['contractor_id'];
					$post_data_return['item_id'] = $item_id;
					//echo $item_id . ' qty '.$post['qty_in'][$item_id].'<br>';
					$post_data_return['date_return'] = date("Y-m-d", strtotime($post['date']));
					//$post_data['surat_izin_id'] = $post['surat_izin_id'];
					$post_data_return['qty_return'] = $post['qty_return'][$item_id];
					$this->mstock->insertStock('return',$post_data_return);
					//exit;
				}
			}
			redirect('stock/multi_all/'.$post['surat_izin_id']);
		}
		$data['all_item'] = $this->mstock->getAllStockSIQty($si);
		$this->load->template('stock/multi_all',$data);
	}
	
	public function in_multi() {
		$data['pageTitle'] = "Stock In Multiple";
		$data['all_si'] = $this->mitem->getAllSuratIzin();
		$post = $this->input->post();
		$si = $this->uri->segment(3);
		if(!empty($si)) {
			$data['si'] = $si;
		} else {
			$si = $this->mitem->getActiveSI();
			//die($si->surat_izin_id);
			//$si = NULL;
			$data['si'] = $si->surat_izin_id;
			$si = $data['si'];
		}
		if($post) {
			$this->firephp->log($post);
			// 			print_r($post);//exit;
			foreach($post['item_id'] as $item_id) {
				if(!empty($post['qty_in'][$item_id])) {
					$post_data['item_id'] = $item_id;
					//echo $item_id . ' qty '.$post['qty_in'][$item_id].'<br>';
					$post_data['date_in'] = date("Y-m-d", strtotime($post['date_in']));
					$post_data['surat_izin_id'] = $post['surat_izin_id'];
					$post_data['qty_in'] = $post['qty_in'][$item_id];
					$this->mstock->insertStock('in',$post_data);
					//$this->mstock->editStockInMulti($post_data,$item_id,$post['surat_izin_id']);
				}
			}
			// 			exit;
			redirect('stock/in_multi/'.$post['surat_izin_id']);
		}
		$data['all_item'] = $this->mstock->getAllStockSIQty($si);
		$this->load->template('stock/in_multi',$data);
	}
	
	public function out_multi() {
		$data['pageTitle'] = "Stock Out Multiple";
		$data['all_si'] = $this->mitem->getAllSuratIzin();
		$data['all_contractors'] = $this->mstock->getAllContractor();
		$post = $this->input->post();
		$si = $this->uri->segment(3);
		if(!empty($si)) {
			$data['si'] = $si;
		} else {
			$si = $this->mitem->getActiveSI();
			//$si = NULL;
			$data['si'] = $si->surat_izin_id;
			$si = $data['si'];
		}
		if($post) {
			$this->firephp->log($post);
			//print_r($post);//exit;
			foreach($post['item_id'] as $item_id) {
				//print_r($post['qty_out']);exit;
				if(!empty($post['qty_out'][$item_id])) {
					//echo 'masuk';exit;
					$post_data['item_id'] = $item_id;
					$post_data['surat_izin_id'] = $post['surat_izin_id'];
					$post_data['contractor_id'] = $post['contractor_id'];
					//echo $item_id . ' qty '.$post['qty_in'][$item_id].'<br>';
					$post_data['date_out'] = date("Y-m-d", strtotime($post['date_out']));
					//$post_data['surat_izin_id'] = $post['surat_izin_id'];
					$post_data['qty_out'] = $post['qty_out'][$item_id];
					$this->mstock->insertStock('out',$post_data);
					//exit;
				}
			}
			redirect('stock/out_multi/'.$post['surat_izin_id']);
		}
		$data['all_item'] = $this->mstock->getAllStockSIQty($si);
		$this->load->template('stock/out_multi',$data);
	}
	
	public function return_multi() {
		$data['pageTitle'] = "Stock Out Multiple";
		$data['all_si'] = $this->mitem->getAllSuratIzin();
		$data['all_contractors'] = $this->mstock->getAllContractor();
		$post = $this->input->post();
		$si = $this->uri->segment(3);
		if(!empty($si)) {
			$data['si'] = $si;
		} else {
			$si = $this->mitem->getActiveSI();
			//$si = NULL;
			$data['si'] = $si->surat_izin_id;
			$si = $data['si'];
		}
		if($post) {
			$this->firephp->log($post);
			//print_r($post);//exit;
			foreach($post['item_id'] as $item_id) {
				//print_r($post['qty_out']);exit;
				if(!empty($post['qty_return'][$item_id])) {
					//echo 'masuk';exit;
					$post_data['item_id'] = $item_id;
					$post_data['surat_izin_id'] = $post['surat_izin_id'];
					$post_data['contractor_id'] = $post['contractor_id'];
					//echo $item_id . ' qty '.$post['qty_in'][$item_id].'<br>';
					$post_data['date_return'] = date("Y-m-d", strtotime($post['date_return']));
					//$post_data['surat_izin_id'] = $post['surat_izin_id'];
					$post_data['qty_return'] = $post['qty_return'][$item_id];
					$this->mstock->insertStock('return',$post_data);
					//exit;
				}
			}
			redirect('stock/return_multi/'.$post['surat_izin_id']);
		}
		$data['all_item'] = $this->mstock->getAllStockSIQty($si);
		$this->load->template('stock/return_multi',$data);
	}
	
	public function in($id=NULL){
		$data['pageTitle'] = "Stock In";
		$data['all_item'] = $this->mitem->getAllItem();
		$data['all_si'] = $this->mitem->getAllSuratIzin();
		$data['all_stock'] = $this->mstock->getAllStockSIQty();
		$post = $this->input->post();
		if($post) {
			//print_r($post);exit;
			$post_data['item_id'] = $post['item_id'];
			$post_data['date_in'] = date("Y-m-d", strtotime($post['date_in']));
			$post_data['surat_izin_id'] = $post['surat_izin_id'];
			$post_data['qty_in'] = $post['qty_in'];
			if($post['stock_in_id']) {
				$this->mitem->updateStockIn($post_data, $post['item_id']);
				redirect('stock/in/'.$post['item_id']);
			} else {
				$last_id = $this->mstock->addStockIn($post_data);
				redirect('stock/in/'.$last_id);
			}
		} elseif($id) {
			$data['stock'] = $this->mstock->getStock($id);
		}
		$this->load->template('stock/in',$data);
	}
	
	public function out($id=NULL) {
		$data['pageTitle'] = "Stock Out";
		$data['all_item'] = $this->mitem->getAllItem();
		$data['all_si'] = $this->mitem->getAllSuratIzin();
		$data['all_stock'] = $this->mstock->getAllStock();
		$post = $this->input->post();
		if($post) {
			//print_r($post);exit;
			$post_data['item_id'] = $post['item_id'];
			$post_data['date_in'] = date("Y-m-d", strtotime($post['date_in']));
			$post_data['surat_izin_id'] = $post['surat_izin_id'];
			$contractor = !empty($post_data['contractor_id']) ? $post_data['contractor_id'] : "";
			$post_data['qty_in'] = $post['qty_in'];
			if($post['stock_in_id']) {
				$this->mitem->updateStockIn($post_data, $post['item_id']);
				redirect('stock/in/'.$post['item_id']);
			} else {
				$last_id = $this->mstock->addStockIn($post_data);
				redirect('stock/in/'.$last_id);
			}
		} elseif($id) {
			$data['stock'] = $this->mstock->getStock($id);
		}
		$this->load->template('stock/out',$data);
	}
	
	public function returns(){
		echo 'test';
	}
	
	public function delete_stock($id) {
		$this->mstock->deleteStock('in', $id);
		$this->mstock->deleteStock('out', $id);
		$this->mstock->deleteStock('return', $id);
	}
	
	public function insert_stock(){
		$items = $this->mitem->getAllItem();
		$sis = $this->mitem->getAllSuratIzin();
		foreach ($sis as $si) {
			foreach($items as $item) {
// 				echo $si->surat_izin.' item '. $item->item_name.'</br>';
				$data['surat_izin_id'] = $si->surat_izin_id;
				$data['item_id'] = $item->item_id;
				$data['qty_in'] = 0;
// 				$data['qty_out'] = 0;
				$data['date_in'] = date('Y-m-d');
// 				$this->mstock->insertMultiStockOut($data);
				$this->mstock->insertStock('in',$data);
			}
		}
	}
	
	
	
}
<?php
class Report extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->model('mitem');
		$this->load->model('mreport');
		$this->mitem = new MItem();
		$this->mreport = new MReport();
	}
	
	public function index(){
		$this->daily();
	}
	
	/**
	 * List All Report
	 */
	
	public function list_view(){
		
	}
	
	public function daily($args = NULL) {
		$data['pageTitle'] = "Daily Report";
		$data['daily'] = $this->mreport->getAllDaily();
		$this->load->template('report/daily',$data);
	}
	
	/**
	 * Triwulan
	 */
	public function quarter() {
		$data['pageTitle'] = "Monthly";
		$si = $this->uri->segment(3);
		if(!empty($si)) {
			$si = $this->uri->segment(3);
		} elseif ($si == 0){
			$sis = $this->mitem->getActiveSI();
			$si = $sis->surat_izin_id;
		} else {
			$si = null;
		} 
		$month_year = !empty($this->uri->segment(4)) ? $this->uri->segment(4) :  date('m-y');
		$month = explode("-", $month_year);
		$data['month'] = $month[0]; 
		$data['year'] = $month[1];
// 		echo getMonthYearBetween($month_year,-3);die;
		$data['month_ago'] = getMonthAgo(1);
// 		echo getQurater('08-14');
// 		echo getFirstQuarterMonth(getQurater('08-14'));	exit;
		$data['surat_izin'] = $this->mreport->getSuratIzin($si);
		$data['contractor_all'] = $this->mreport->getAllContractor();
		for($i=0;$i>=-2;$i--) {
			$data['month_year_ago'][abs($i)] = getMonthYearBetween($month_year,$i);
// 			echo getMonthYearBetween('03-14','-5');
			$data['monthly'][abs($i)] = $this->mreport->pivotMonthlyStockUse(getMonthYearBetween($month_year,$i), $si);
		}
		$this->load->template('report/quarter',$data);
	}
	
	
	public function semester($args = NULL) {
		$data['pageTitle'] = "Semester";
		$si = $this->uri->segment(3);
		if(!empty($si)) {
			$si = $this->uri->segment(3);
		} elseif ($si == 0){
			$sis = $this->mitem->getActiveSI();
			$si = $sis->surat_izin_id;
		} else {
			$si = null;
		}
		$month_year = !empty($this->uri->segment(4)) ? $this->uri->segment(4) :  date('m-y');
		$month = explode("-", $month_year);
		$data['month'] = $month[0];
		$data['year'] = $month[1];
		// 		echo getMonthYearBetween($month_year,-3);die;
		$data['month_ago'] = getMonthAgo(1);
		$data['surat_izin'] = $this->mreport->getSuratIzin($si);
		$data['contractor_all'] = $this->mreport->getAllContractor();
		for($i=0;$i>=-5;$i--) {
			$data['month_year_ago'][abs($i)] = getMonthYearBetween($month_year,$i);
			// 			echo getMonthYearBetween('03-14','-5');
			$data['monthly'][abs($i)] = $this->mreport->pivotMonthlyStockUse(getMonthYearBetween($month_year,$i), $si);
		}
		$this->load->template('report/semester',$data);
	}
	
	/**
	 * Control Surat Izin
	 */
	public function all_date_in(){
		$data['pageTitle'] = "All Stock In";
// 		$all_date = $this->mreport->getAllDateIn();
// 		foreach($all_date as $row) {
// 			$date_in[] =$row->date_in;
// 		}
		$si = $this->uri->segment(3);
		if(!empty($si)) {
			$si = $this->uri->segment(3);
		} elseif ($si == 0){
			$sis = $this->mitem->getActiveSI();
			$si = $sis->surat_izin_id;
		} else {
			$si = null;
		} 
		$month_year = !empty($this->uri->segment(4)) ? $this->uri->segment(4) :  date('m-y');
		$month = explode("-", $month_year);
		//$this->firephp->log('surat_izin_id ='.$si);
		$data['date_in'] = getAllDate($month[0], $month[1]);
// 		$data['date_in'] = $date_in;
		$data['surat_izin'] = $this->mreport->getSuratIzin($si);
		$data['pivot'] = $this->mreport->pivotDailyStock('in',$data['date_in'], $si, $month_year);
		$this->load->template('report/pivot_in',$data);
	}

	public function contractor_all(){
		$data['pageTitle'] = "All Stock In";
		$all_date = $this->mreport->getAllDateOut();
// 		foreach($all_date as $row) {
// 			$date_out[] =$row->date_out;
// 		}
// 		$si = !empty($this->uri->segment(3)) ? $this->uri->segment(3) : '';
		$si = $this->uri->segment(3);
		if(!empty($si)) {
			$si = $this->uri->segment(3);
		} elseif ($si == 0){
			$sis = $this->mitem->getActiveSI();
			$si = $sis->surat_izin_id;
		} else {
			$si = null;
		}
		$month_year = !empty($this->uri->segment(4)) ? $this->uri->segment(4) :  date('m-y');
		$contractor = !empty($this->uri->segment(5)) ? $this->uri->segment(5) : '';
		$data['contractor_name'] = !empty($this->uri->segment(5)) ? $this->mreport->getContractor($contractor) : '';
		// //$this->firephp->log('contractor_id ='.$contractor);
		$month = explode("-", $month_year);
		$data['date_out'] = getAllDate($month[0], $month[1]);
		$data['month'] = $month[0]; 
		$data['year'] = $month[1];
		$data['surat_izin_id'] = $si;
		$data['contractor_all'] = $this->mreport->getAllContractor();
		$data['contractor_id'] = $contractor;
		$data['pivot_use'] = $this->mreport->pivotDailyStockUse($data['date_out'], $si, $month_year, $contractor);
		$data['pivot_out'] = $this->mreport->pivotDailyStock('out',$data['date_out'], $si, $month_year, $contractor);
		$data['pivot_return'] = $this->mreport->pivotDailyStock('return',$data['date_out'], $si, $month_year, $contractor);
		$this->load->template('report/contractor_all',$data);
	}
	
	public function category_monthly(){
		$si = !empty($this->uri->segment(3)) ? $this->uri->segment(3) : '';
		$month = !empty($this->uri->segment(4)) ? $this->uri->segment(4) : '';
		$data['pageTitle'] = "Laporan Perincian Bulanan";
		$this->load->template('report/category_monthly',$data);
	}
	
	/**
	 * Rincian Harian
	 */
	public function category_daily() {
		$data['pageTitle'] = "Category Report Daily";
		$si = $this->uri->segment(3);
		if(!empty($si)) {
			$si = $this->uri->segment(3);
		} elseif ($si == 0){
			$sis = $this->mitem->getActiveSI();
			$si = $sis->surat_izin_id;
		} else {
			$si = null;
		}
		$month_year = !empty($this->uri->segment(4)) ? $this->uri->segment(4) :  date('m-y');
		$month = explode("-", $month_year);
		$data['all_date'] = getAllDate($month[0], $month[1]);
		$data['month'] = $month[0];
		$data['year'] = $month[1];
		$data['all_cat'] = $this->mreport->getAllCategories();
		$data['pivot_stock_out'] = $this->mreport->pivotDailyCatStock("out",$data['all_date'], $si,$month_year);
		$data['pivot_stock_return'] = $this->mreport->pivotDailyCatStock("return",$data['all_date'], $si,$month_year);
		$data['pivot_stock_use'] = $this->mreport->pivotDailyCatStockUse($data['all_date'], $si,$month_year);
		$this->load->template('report/category_daily', $data);
	}
	
	public function get_month(){
		$all_date = $this->mreport->getAllDateIn();
		$date = $all_date[0]->date_in;
		echo getYearMonth($date);
	}
	
	public function get_month_ago($ago){
		$month_ago = getMonthAgo($ago);
		//echo date("Y-m-d");
		echo $month_ago;
	}
	
	
}
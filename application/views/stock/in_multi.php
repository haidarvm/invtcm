<script>
$(function(){
    // bind change event to select
    $('#si_change').bind('change', function () {
        si = $(this).val(); // get selected value
        url = site_url + 'stock/in_multi/' + si;
        if (si) { // require a URL
            //alert(url);
            window.location = url; // redirect
        }
        return false;
    });
});
</script>
			<!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                       <?php echo !empty($cat) ? 'Edit' : 'Add' ?> Stock In <i class="fa fa-send"></i>
                        <small>Preview</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="#">Others</a></li>
                        <li class="active">Add Stock</li>
                    </ol>
                </section>
                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-10">
                            <!-- general form elements -->
                            <div class="box box-primary">
	                            <div class="box-header">
	                            	<h3 class="box-title">Surat Izin
	                            	<select id="si_change" name="si">
	                            		<option></option>
	                            		<?php foreach ($all_si as $sis) {?>
	                            		<option value="<?php echo $sis->surat_izin_id?>"
	                            		<?php if(!empty($si)) {
												echo $si == $sis->surat_izin_id ? 'selected':'';
										}?>><?php echo $sis->surat_izin?></option>
	                            		<?php } ?>
	                            	</select></h3>
	                            </div>
                                <!-- form start -->
                                <form role="form" method="POST" enctype="multipart/form-data" 
                                action="<?php echo site_url()?>stock/in_multi">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <div class="row">
                                            	<div class="col-xs-4">
                                            		<label for="name"><i class="fa fa-calendar"></i> Date In</label>
                                        			<input type="text" size="4" class="form-control" name="date_in" required id="date-picker" placeholder="YYYY-MM-DD" >
                                        		</div>
                                        	</div>
                                        </div>
                                        <table id="stock-out-multi" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Name</th>
                                                <th>Item Code</th>
                                                <th>Unit</th>
                                                <th>Surat Izin</th>
                                                <th>Total Qty</th>
                                                <th>Total Qty Out</th>
                                                <th>Total Qty Return</th>
                                                <th>On Hand</th>
                                                <th>Total Use</th>
                                                <th>Qty In</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php 
                                        $i = 1;
                                        foreach($all_item->result() as $items) {?>
                                            <tr>
                                                <td><?php echo $i++;?></td>
                                                <td><input type="hidden" name="item_id[<?php echo $items->item_id?>]" value="<?php echo $items->item_id?>"><?php echo $items->item_name;?></td>
                                                <td><?php echo $items->item_code;?></td>
                                                <td><?php echo $items->qty_unit;?></td>
                                                <td><?php echo empty($si) ? '<input type="hidden" name="surat_izin_id" value="'.$items->surat_izin_id.'">' : '';?><?php echo $items->surat_izin;?></td>
                                                <td><?php echo $items->sum_qty_in;?></td>
                                                <td><?php echo $items->sum_qty_out;?></td>
                                                <td><?php echo $items->sum_qty_return;?></td>
                                                <td><?php echo $items->qty_av;?></td>
                                                <td><?php echo $items->sum_qty_use;?></td>
                                                <td><input type="text" class="form-control" size="12" name="qty_in[<?php echo $items->item_id?>]"></td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                    </div><!-- /.box-body -->
									<?php echo !empty($si) ? '<input type="hidden" name="surat_izin_id" value="'.$si.'" >' : '';?>
                                    <div class="box-footer">
                                    	<button type="submit" class="btn btn-primary">
                                        	<i class="fa fa-check-square-o"></i> Submit
                                        </button>
                                    </div>
                                </form>
                            </div><!-- /.box -->
                        </div><!--/.col (left) -->
                        <!-- right column -->
                    </div>   <!-- /.row -->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
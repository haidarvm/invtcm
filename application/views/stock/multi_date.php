<script>
$(document).ready(function() {
   
    $('#chage_item').click(function() {
    	var mm_yy = $('#datepicker-mm-yy').val();
    	var surat_izin_id = $('#si_change').val();
    	var item_id = $('#item_change').val();
    	var month_year = mm_yy.split("-"); 
    	console.log('month_year'+ month_year);
    	console.log(item_id);
    	if(mm_yy) {
	        url = "<?php echo site_url();?>stock/multi_date/" + surat_izin_id + '/' + month_year[0] + "-" + month_year[1] + '/'+ item_id;
	        window.location = url; // redirect
    	}
        return false;
	});
    
} );
</script>
			<!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                       <?php echo !empty($cat) ? 'Edit' : 'Add' ?> Stock In <i class="fa fa-cubes"></i>
                        <small>Preview</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="#">Others</a></li>
                        <li class="active">Return Stock</li>
                    </ol>
                </section>
                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-10">
                            <!-- general form elements -->
                            <div class="box box-primary">
	                            <div class="box-header">
	                            	<h3 class="box-title">Surat Izin 
	                            	</h3>
	                            </div>
                                <!-- form start -->
                                   <div class="box-body">
                                        <div class="form-group">
                                            <div class="row">
                                            	<div class="col-xs-5">
                                            	<h3>Item Details</h3>
                                            	<table class="table table-bordered table-striped">
                                       				<tbody>
                                            		<tr>
                                            			<td>SURAT IZIN</td>
                                            			<td><b><?php echo $si_name->surat_izin?></b></td>
                                            		</tr>
				                            			<td>PERIODE</td>
				                            			<td><?php echo date("F", mktime(0, 0, 0, $month, 10)).'-'.$year;?>
                                            		<tr>
                                            			<td>ITEM CODE</td>
                                            			<td><b><?php echo !empty($item) ? $item->item_code : ""?></b></td>
                                            		</tr>
                                            		<tr>
                                            			<td>DESCRIPTION</td>
                                            			<td><b><?php echo !empty($item) ? $item->item_name : ""?></b></td>
                                            		</tr>
                                            		<tr>
                                            			<td>UOM</td>
                                            			<td><?php echo !empty($item) ? strtoupper($item->qty_unit) : ""?></td>
                                            		</tr>
                                            		<tr>
                                            			<td>LOCATOR</td>
                                            			<td>GUDANG 4</td>
                                            		</tr>
                                            		<tr>
                                            			<td>ROP:</td>
                                            			<td></td>
                                            		</tr>
                                            		<tr>
                                            			<td>EOQ:</td>
                                            			<td></td>
                                            		</tr>
                                        			</tbody>
                                            	</table>
                                        		</div>
                                        		<div class="col-xs-6">
	                                            	<h3>Choose Item and Month</h3>
	                                            	<table class="table table-bordered table-striped">
	                                       				<tbody>
	                                            		<tr>
	                                            			<td>SURAT IZIN</td>
	                                            			<td><select id="si_change" name="si">
						                            		<option></option>
						                            		<?php foreach ($all_si as $sis) {?>
						                            		<option value="<?php echo $sis->surat_izin_id?>"
						                            		<?php if(!empty($si)) {
																	echo $si == $sis->surat_izin_id ? 'selected':'';
															}?>><?php echo $sis->surat_izin?></option>
						                            		<?php } ?>
						                            		</select></td>
	                                            		</tr>
					                            			<td>Month Year</td>
					                            			<td><input type="text" size="4" class="form-control" name="mm-yy" class="date_change" required id="datepicker-mm-yy" placeholder="mm-yy" ></td>
	                                            		<tr>
	                                            			<td>ITEM Name</td>
	                                            			<td>
	                                            			<select id="item_change" name="item_id">
	                                        				<?php foreach($all_item as $items) {?>
	                                        				<option value="<?php echo $items->item_id?>"><?php echo $items->item_name?></option>
	                                        				<?php } ?>
	                                            			</select></td>
	                                            		</tr>
	                                            		<tr>
	                                            			<td></td>
	                                            			<td>
	                                            			<button id="chage_item" class="btn btn-primary">
			                                        			<i class="fa fa-check-square-o"></i> Submit
			                                        		</button>
	                                            			</td>
	                                            		</tr>
	                                        			</tbody>
	                                            	</table>
			                                    	
                                        		</div>
                                        	</div>
                                        </div>
                                        <?php 
                                        if(!empty($item)) {
                                        ?>
                                        <form role="form" id="in-multi" method="POST" enctype="multipart/form-data" 
                                		action="<?php echo site_url()?>stock/multi_date/<?php echo $si ?>/<?php echo $month_year?>/<?php echo  $item->item_id;?>">
                                
                                        <table id="stock-out-multi" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Date</th>
                                                <th>Reg No.</th>
                                                <th>In</th>
                                                <th>Total In</th>
                                                <th>Out</th>
                                                <th>Total Out</th>
											<!--<th>Return</th> -->
                                                <th>OHS</th>
                                                <th>SIGN</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php 
                                        $i = 1;
	                                    foreach($all_date as $date) {
                                        	foreach($all_contractor as $contractor) {
										?>
	                                            <tr>
	                                                <td><?php echo $i++;?></td>
	                                                <td><?php echo indoDate($date);?></td>
	                                                <td><?php ?></td>
	                                                <td>
		                                                <input type="text" class="form-control" size="3" name="qty_return[<?php echo $date?>][<?php echo $contractor->contractor_id?>]" value="">
	                                                </td>
	                                                <td>
		                                                <input type="text" class="form-control" size="3" disabled="disabled" name="sum_qty_return[<?php echo $date?>][<?php echo $contractor->contractor_id?>]" value="<?php echo $this->mstock->getStock('return', $item->item_id, $si, $contractor->contractor_id, $date);?>">
	                                                </td>
	                                                <td>
	                                                	<input type="text" class="form-control" size="3" name="qty_out[<?php echo $date?>][<?php echo $contractor->contractor_id?>]" value="">
	                                                </td>
	                                                <td>
	                                                	<input type="text" class="form-control" size="3" disabled="disabled" name="sum_qty_out[<?php echo $date?>][<?php echo $contractor->contractor_id?>]" value="<?php echo $this->mstock->getStock('out', $item->item_id, $si, $contractor->contractor_id, $date);?>">
	                                                </td>
	                                                <td><input type="text" class="form-control" size="3" name="" value=""></td>
	                                                <td><?php echo $contractor->contractor_name;?></td>
	                                            </tr>
	                                        <?php 
												} 
	                                        } ?>
                                        </tbody>
                                    </table>
                                    </div><!-- /.box-body -->
									<?php echo !empty($si) ? '<input type="hidden" name="surat_izin_id" value="'.$si.'" >' : '';?>
									<input type="hidden" name="item_id" value="<?php echo $item->item_id?>">
									<input type="hidden" name="month_year" value="<?php echo $month_year?>">
                                    <div class="box-footer">
                                    	<button type="submit" class="btn btn-primary">
                                        	<i class="fa fa-check-square-o"></i> Submit
                                        </button>
                                    </div>
                                </form>
                                <?php } ?>
                            </div><!-- /.box -->
                        </div><!--/.col (left) -->
                        <!-- right column -->
                    </div>   <!-- /.row -->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
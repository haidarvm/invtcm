            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                       <?php echo isset($cat) ? 'Edit' : 'Add' ?> Stock
                        <small>Preview</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="#">Others</a></li>
                        <li class="active">Add Stock</li>
                    </ol>
                </section>
                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-6">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <!-- form start -->
                                <form role="form" method="POST" enctype="multipart/form-data" 
                                action="<?php echo site_url()?>stock/in">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="name">Item Name</label>
                                            <select name="item_id" class="form-control">
                                           	<?php foreach ($all_item as $items) {?>
                                            	<option value="<?php echo $items->item_id?>"
                                            	<?php if(isset($stock_in)) {
												echo $items->category_id == $stock_in->item_id ? 'selected':'';
												}?>
                                            	><?php echo $items->item_name?></option>
                                            <?php } ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                            	<div class="col-xs-4">
                                            		<label for="name">Date In</label>
		                                            <input type="text" class="form-control" name="date_in" id="date-picker" date-format="dd-mm-yy"
		                                             value="<?php echo isset($stock_in) ? datepickerf($stock_in->date_in) : '';?>" >
	                                        	</div>
	                                        	<div class="col-xs-4">
                                            		<label for="name">QTY</label>
		                                            <input type="text" class="form-control" name="qty_in" id="qty_in" 
		                                             value="<?php echo isset($stock_in) ? $stock_in->qty_in : '';?>" >
	                                        	</div>
	                                        	<div class="col-xs-4">
                                            		<label for="name">Surat Izin</label>
		                                            <select name="surat_izin_id"  class="form-control">
                                            			<option value=""></option>
		                                            	<?php foreach ($all_si as $si) {?>
		                                            	<option value="<?php echo $si->surat_izin_id?>" 
		                                            	<?php if(isset($item)) {
														echo $si->surat_izin_id == $item->surat_izin_id ? 'selected':'';
														}?>><?php echo $si->surat_izin?></option>
		                                            	<?php } ?>
                                            		</select>
	                                        	</div>
	                                        </div>	
                                        </div>
                                    </div><!-- /.box-body -->
									<?php echo isset($cat) ? '<input type="hidden" name="category_id" value="'.$cat->category_id.'" >' : '';?>
                                    <div class="box-footer">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </form>
                            </div><!-- /.box -->
                        </div><!--/.col (left) -->
                        <div class="col-md-6">
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">List Stock
                                    <?php echo isset($stock) ? '<button onclick="location.href=\''. site_url().'stock/in/\'" class="btn btn-sm btn-primary">Add Stock</button>' : '' ?> 
                                    </h3>
                                </div>
                                <div class="box-body">
									<?php
									foreach ($all_stock as $stocks) {
										echo "<p><a href='" . site_url() . "stock/in/" . $stocks->stock_in_id . "'>" .$stocks->item_name . " QTY IN ". $stocks->qty_in . "</a></p>";
									}
									?>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                         </div><!-- right column -->
                    </div>   <!-- /.row -->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
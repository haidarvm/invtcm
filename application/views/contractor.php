            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                       <?php echo isset($cont) ? 'Edit' : 'Add' ?> Contractor
                        <small>Preview</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="#">Others</a></li>
                        <li class="active">Add Contractor</li>
                    </ol>
                </section>
                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-6">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <!-- form start -->
                                <form role="form" method="POST" enctype="multipart/form-data" 
                                action="<?php echo site_url()?>inv/contractor">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="name">Contractor Name</label>
                                            <input type="text" class="form-control" name="contractor_name" id="contractor_name"
                                             value="<?php echo isset($cont) ? $cont->contractor_name : '';?>" >
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Description</label>
                                            <textarea name="contractor_desc" class="form-control" ><?php  echo isset($cont) ? $cont->contractor_desc : '';?></textarea>
                                        </div>
                                    </div><!-- /.box-body -->
									<?php echo isset($cont) ? '<input type="hidden" name="contractor_id" value="'.$cont->contractor_id.'" >' : '';?>
                                    <div class="box-footer">
                                    	<button type="submit" class="btn btn-primary">
                                        	<i class="fa fa-check-square-o"></i> Submit
                                        </button>
                                    </div>
                                </form>
                            </div><!-- /.box -->
                        </div><!--/.col (left) -->
                        <div class="col-md-6">
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">List Contractor
                                    <?php echo isset($cont) ? '<button onclick="location.href=\''. site_url().'inv/contractor/\'" class="btn btn-sm btn-primary">Add Contractor</button>' : '' ?> 
                                    </h3>
                                </div>
                                <div class="box-body">
									<?php
									foreach ($all_cont->result() as $conts) {
										echo "<p><a href='" . site_url() . "inv/contractor/" . $conts->contractor_id . "'>" . $conts->contractor_name . "</a></p>";
									}
									?>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                         </div><!-- right column -->
                    </div>   <!-- /.row -->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
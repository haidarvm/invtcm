            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <?php echo !empty($si) ? 'Edit' : 'Add' ?> Surat Izin <?php echo !empty($item) ? '<i class="fa fa-edit"></i>' : '<i class="fa fa-legal"></i>';?>
                        <small>Preview</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="#">Others</a></li>
                        <li class="active">Add Surat Izin</li>
                    </ol>
                </section>
                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-6">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <!-- form start -->
                                <form role="form" method="POST" enctype="multipart/form-data" 
                                action="<?php echo site_url()?>inv/surat_izin">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="name">Surat Izin</label>
                                            <input type="text" class="form-control" name="surat_izin" id="surat_izin"
                                             value="<?php echo !empty($si) ? $si->surat_izin : '';?>" >
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Description</label>
                                            <textarea name="surat_izin_desc" class="form-control" ><?php  echo !empty($si) ? $si->surat_izin_desc : '';?></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="name">Date Range</label>
                                            <div class="row">
                                            	<div class="col-xs-4">
		                                            <input type="text" class="form-control" name="date_min" id="date-picker" 
		                                             value="<?php echo !empty($si) ? datepickerf($si->date_min) : date('Y-m-d');?>" >
		                                        </div>
		                                        <div class="col-xs-4">
		                                            <input type="text" class="form-control" name="date_max" id="date-picker2"
		                                             value="<?php echo !empty($si) ? datepickerf($si->date_max) : getDateBetween(date('Y-m-d'), +6);?>" >
	                                             </div>
                                             </div>
                                        </div>
                                    </div><!-- /.box-body -->
									<?php echo !empty($si) ? '<input type="hidden" name="surat_izin_id" value="'.$si->surat_izin_id.'" >' : '';?>
                                    <div class="box-footer">
                                        <button type="submit" class="btn btn-primary">
                                        <i class="fa fa-check-square-o"></i>
                                        Submit</button>
                                    </div>
                                </form>
                            </div><!-- /.box -->
                        </div><!--/.col (left) -->
                         <div class="col-md-6">
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">List Surat Izin
                                    <?php echo !empty($si) ? '<button onclick="location.href=\''. site_url().'inv/surat_izin/\'" class="btn btn-sm btn-primary">Add Surat Izin</button>' : '' ?> 
                                    </h3>
                                </div>
                                <div class="box-body">
									<?php
									foreach ($all_si as $sis) {
										echo "<p><a href='" . site_url() . "inv/surat_izin/" . $sis->surat_izin_id . "'>" . $sis->surat_izin . "</a></p>";
									}
									?>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                         </div><!-- right column -->
                    </div>   <!-- /.row -->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
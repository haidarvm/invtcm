            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                       <?php echo !empty($item) ? 'Edit' : 'Add' ?> Item <?php echo !empty($item) ? '<i class="fa fa-edit"></i>' : '<i class="fa fa-download"></i>';?>
                        <small>Preview</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="#">Item</a></li>
                        <li class="active">Add New Item</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-6">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <!-- form start -->
                                <form role="form" method="POST" enctype="multipart/form-data" 
                                action="<?php echo site_url()?>inv/item">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="name">Name</label>
                                            <input type="text" class="form-control" name="item_name" id="item_name" value="<?php echo isset($item) ? $item->item_name : '';?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="name">Category</label>
                                            <select name="category_id" class="form-control">
                                            	<option value=""></option>
                                           	<?php foreach ($all_cat->result() as $category) {?>
                                            	<option value="<?php echo $category->category_id?>"
                                            	<?php if(isset($item)) {
												echo $category->category_id == $item->category_id ? 'selected':'';
												}?>
                                            	><?php echo $category->category_name?></option>
                                            <?php } ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="name">Item Code</label>
                                            <input type="text" class="form-control" name="item_code" id="item_code" value="<?php echo isset($item) ? $item->item_code : '';?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="name">Satuan</label>
                                            <select name="qty_unit_id" class="form-control">
                                            	<option value=""></option>
                                            <?php foreach ($all_qty_unit->result() as $qty_units) {?>
                                            	<option value="<?php echo $qty_units->qty_unit_id?>"
                                            	<?php if(isset($item)) {
												echo $qty_units->qty_unit_id == $item->qty_unit_id ? 'selected':'';
												}?>
                                            	><?php echo strtoupper($qty_units->qty_unit)?></option>
                                            <?php } ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="item_desc">Description</label>
                                            <textarea name="item_desc" class="form-control" ><?php echo isset($item) ? $item->item_desc : '';?></textarea>
                                        </div>
                                    </div><!-- /.box-body -->
									<?php echo isset($item) ? '<input type="hidden" name="item_id" value="'.$item->item_id.'" >' : '';?>
                                    <div class="box-footer">
                                        <button type="submit" class="btn btn-primary">
                                        	<i class="fa fa-check-square-o"></i> Submit
                                        </button>
                                    </div>
                                </form>
                            </div><!-- /.box -->

                           
                        </div><!--/.col (left) -->
                        <div class="col-md-6">
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">List Item
                                    <?php echo isset($item) ? '<button onclick="location.href=\''. site_url().'inv/item/\'" class="btn btn-sm btn-primary">Add Item</button>' : '' ?> 
                                    </h3>
                                </div>
                                <div class="box-body">
									<?php
									foreach ($all_item as $items) {
										echo "<p><a href='" . site_url() . "inv/item/" . $items->item_id . "'>" . $items->item_name . "</a> <a href=\"".site_url()."inv/delete_item/".$items->item_id."\" class=\"btn btn-danger btn-xs\">Delete</a></p>";
									}
									?>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                         </div><!-- right column -->
                    </div>   <!-- /.row -->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
</div><!-- ./wrapper -->
        <!-- Bootstrap -->
        <script src="<?php echo base_url();?>assets/js/invtcm.js" type="text/javascript"></script>        
        <script src="<?php echo base_url();?>assets/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>assets/js/jquery.validate.min.js"></script>
        <!-- DATA TABES SCRIPT -->
        <script src="<?php echo base_url();?>assets/js/jquery.dataTables.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/js/dataTables.fixedColumns.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="<?php echo base_url();?>assets/js/AdminLTE/app.js" type="text/javascript"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="<?php echo base_url();?>assets/js/bootstrap-datepicker.min.js" type="text/javascript"></script>        
    </body>
</html>
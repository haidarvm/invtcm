<?php 
$collapse = '';
if($this->uri->segment(1) == 'report') {
	$collapse = ' collapse-left';
}
?>
<!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas <?php echo $collapse;?>">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <!-- search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li>
                            <a href="<?php echo base_url();?>assets/index.html">
                                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                            </a>
                        </li>
                        <li class="treeview active">
                            <a href="#">
                                <i class="fa fa-bar-chart-o"></i>
                                <span>Report</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="<?php echo site_url();?>report/all_date_in/"><i class="fa fa-calendar"></i> Control SI</a></li>
                                <li><a href="<?php echo site_url();?>report/contractor_all/"><i class="fa fa-calendar"></i> ISSUE</a></li>
                                <li><a href="<?php echo site_url();?>report/category_daily"><i class="fa fa-clock-o"></i> Daily</a></li>
                                <li><a href="<?php echo site_url();?>report/category_daily"><i class="fa fa-clock-o"></i> Rekapitulasi</a></li>
                                <li><a href="<?php echo site_url();?>report/quarter"><i class="fa fa-calendar-o"></i> Triwulan</a></li>
                                <li><a href="<?php echo site_url();?>inv/list_item"><i class="fa fa-history"></i> Semester</a></li>
                            </ul>
                        </li>
                        <li class="treeview active">
                            <a href="#">
                                <i class="fa fa-th"></i> <span>Item</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                             <ul class="treeview-menu">
                                <li><a href="<?php echo site_url();?>inv/item"><i class="fa fa-cubes"></i> List Item</a></li>
                                <li><a href="<?php echo site_url();?>inv/item"><i class="fa fa-stack-overflow"></i> Add Item <small class="badge pull-right bg-red">3</small></a></li>
                                <li><a href="<?php echo site_url();?>stock/multi_date"><i class="fa fa-reply"></i> Stock Multi Date</a></li>
                                <li><a href="<?php echo site_url();?>stock/multi_all"><i class="fa fa-reply"></i> Stock Multi All</a></li>
                                <li><a href="<?php echo site_url();?>stock/in_multi"><i class="fa fa-reply"></i> Stock In</a></li>
                                <li><a href="<?php echo site_url();?>stock/out_multi"><i class="fa fa-share"></i> Stock Out</a></li>
                                <li><a href="<?php echo site_url();?>stock/return_multi"><i class="fa fa-refresh"></i>Stock Return</a></li>
                            </ul>
                        </li>
                        <li class="treeview active">
                            <a href="#">
                                <i class="fa fa-group"></i> <span>Contractor</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="<?php echo site_url();?>report/contractor_all/0/<?php echo date('m-y');?>/1"><i class="fa fa-building-o"></i> PAMA</a></li>
                                <li><a href="<?php echo site_url();?>report/contractor_all/0/<?php echo date('m-y');?>/2"><i class="fa fa-building-o"></i> BAS</a></li>
                                <li><a href="<?php echo site_url();?>report/contractor_all/0/<?php echo date('m-y');?>/3"><i class="fa fa-building-o"></i> MAP</a></li>
                                <li><a href="<?php echo site_url();?>report/contractor_all/0/<?php echo date('m-y');?>/4"><i class="fa fa-building-o"></i> RIUNG</a></li>
                            </ul>
                        </li>
<!--                         <li> -->
<!--                             <a href="../calendar.html"> -->
<!--                                 <i class="fa fa-calendar"></i> <span>Calendar</span> -->
<!--                                 <small class="badge pull-right bg-red">3</small> -->
<!--                             </a> -->
<!--                         </li> -->
                        <li class="treeview active">
                            <a href="#">
                                <i class="fa fa-folder"></i> <span>Others</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="<?php echo site_url()?>inv/surat_izin"><i class="fa fa-gavel"></i> Surat Izin <small class="badge pull-right bg-yellow">5</small></a></li>
                                <li><a href="<?php echo site_url()?>inv/contractor"><i class="fa fa-plus-square"></i> Add Contractor</a></li>
                                <li><a href="<?php echo site_url()?>inv/category"><i class="fa fa-folder-open"></i> Category <small class="badge pull-right bg-green">6</small></a></li>
                                <li><a href="<?php echo site_url()?>user"><i class="fa fa-user"></i> User <small class="badge pull-right bg-blue">2</small></a></li>
                                <li><a href="<?php echo site_url()?>inv/qty_unit"><i class="fa fa-database"></i> QTY Unit</a></li>
                            </ul>
                        </li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>
<script>
$(document).ready(function() {
    var table = $('#example').DataTable( {
    	scrollY:        "300px",
        scrollX:        true,
        scrollCollapse: true,
        paging:         false,
      
    } );
 
    new $.fn.dataTable.FixedColumns( table, {
        leftColumns: 3
    } );
} );
</script>
<style>
/* Ensure that the demo table scrolls */
    div.dataTables_wrapper {
        width: 1200px;
        margin: 0 auto;
    }
</style>

			<!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side strech">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Rekapitulasi Pemakaian <?php echo !empty($contractor_name) ? $contractor_name->contractor_name : "";?>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="#">Tables</a></li>
                        <li class="active">Data tables</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Pemakaian (Use)</h3>                                    
                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive">
		                                    <table id="example" class=" row-border order-column display" cellspacing="0" width="100%">
		                                        <thead>
		                                            <tr>
		                                                <th>No</th>
		                                                <th>Name</th>
		                                                <th>Item Code</th>
		                                                <th>Satuan</th>
		                                                <?php foreach($date_out as $date) {?>
		                                                <th><?php echo $date;?></th>
		                                                <?php } ?>
		                                                <th>Total</th>
		                                            </tr>
		                                        </thead>
		                                        <tbody>
		                                        <?php 
		                                        $i = 1;
		                                        foreach($pivot_use as $row) {?>
		                                            <tr  style="height: 30px;">
		                                                <td><?php echo $i++;?></td>
		                                                <td><?php echo $row->item_name;?></td>
		                                                <td><?php echo $row->item_code;?></td>
		                                                <td><?php echo $row->qty_unit;?></td>
		                                                <?php foreach($date_out as $date_qty) {?>
		                                                <td><?php echo $row->$date_qty;?></td>
		                                                <?php } ?>
		                                                <td><?php echo !empty($row->Total) ? $row->Total : "" ?>
		                                                <script type="text/javascript">
														/* <![CDATA[ */
														(function(){try{var s,a,i,j,r,c,l,b=document.getElementsByTagName("script");l=b[b.length-1].previousSibling;a=l.getAttribute('data-cfemail');if(a){s='';r=parseInt(a.substr(0,2),16);for(j=2;a.length-j;j+=2){c=parseInt(a.substr(j,2),16)^r;s+=String.fromCharCode(c);}s=document.createTextNode(s);l.parentNode.replaceChild(s,l);}}catch(e){}})();
														/* ]]> */
														</script>
		                                                </td>
		                                            </tr>
		                                        <?php 
												
												}
												$total_i = $i;
												?>
		                                        <tr>
		                                        	<td><?php echo $total_i+1;?></td>
		                                        	<td>Pengambilan</td>
		                                        	<td></td>
		                                        	<td></td>
		                                        	 <?php 
		                                        	 foreach($date_out as $date_qty) {?>
		                                                <td><?php echo "";?></td>
		                                                <?php } ?>
	                                                <td>
	                                                <script type="text/javascript">
													/* <![CDATA[ */
													(function(){try{var s,a,i,j,r,c,l,b=document.getElementsByTagName("script");l=b[b.length-1].previousSibling;a=l.getAttribute('data-cfemail');if(a){s='';r=parseInt(a.substr(0,2),16);for(j=2;a.length-j;j+=2){c=parseInt(a.substr(j,2),16)^r;s+=String.fromCharCode(c);}s=document.createTextNode(s);l.parentNode.replaceChild(s,l);}}catch(e){}})();
													/* ]]> */
													</script>
	                                                </td>
		                                        </tr>    
		                                        </tbody>
		                                    </table>
		                                    <h3>Pengambilan (Take)</h3>
		                                    <table id="example1" class="table table-bordered table-striped">
		                                        <thead>
		                                            <tr>
		                                                <th>No</th>
		                                                <th>Name</th>
		                                                <th>Item Code</th>
		                                                <th>Satuan</th>
		                                                <?php foreach($date_out as $date) {?>
		                                                <th><?php echo $date;?></th>
		                                                <?php } ?>
		                                                <th>Total</th>
		                                            </tr>
		                                        </thead>
		                                        <tbody>
		                                        <?php 
		                                        $i = 1;
		                                        foreach($pivot_out as $row) {?>
		                                            <tr>
		                                                <td><?php echo $i++;?></td>
		                                                <td><?php echo $row->item_name;?></td>
		                                                <td><?php echo $row->item_code;?></td>
		                                                <td><?php echo $row->qty_unit;?></td>
		                                                <?php foreach($date_out as $date_qty) {?>
		                                                <td><?php echo $row->$date_qty;?></td>
		                                                <?php } ?>
		                                                <td><?php echo !empty($row->Total) ? $row->Total : "" ?></td>
		                                            </tr>
		                                        <?php } ?>    
		                                        </tbody>
		                                    </table>
		                                    <h3>Pengembalian (Return)</h3>
		                                    <table id="example1" class="table table-bordered table-striped">
		                                        <thead>
		                                            <tr>
		                                                <th>No</th>
		                                                <th>Name</th>
		                                                <th>Item Code</th>
		                                                <th>Satuan</th>
		                                                <?php foreach($date_out as $date) {?>
		                                                <th><?php echo $date;?></th>
		                                                <?php } ?>
		                                                <th>Total</th>
		                                            </tr>
		                                        </thead>
		                                        <tbody>
		                                        <?php 
		                                        $i = 1;
		                                        foreach($pivot_return as $row) {?>
		                                            <tr>
		                                                <td><?php echo $i++;?></td>
		                                                <td><?php echo $row->item_name;?></td>
		                                                <td><?php echo $row->item_code;?></td>
		                                                <td><?php echo $row->qty_unit;?></td>
		                                                <?php foreach($date_out as $date_qty) {?>
		                                                <td><?php echo $row->$date_qty;?></td>
		                                                <?php } ?>
		                                                <td><?php echo !empty($row->Total) ? $row->Total : "" ?></td>
		                                            </tr>
		                                        <?php } ?>    
		                                        </tbody>
		                                    </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
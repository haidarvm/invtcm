<script>
$(document).ready(function() {
    $('.submit').click(function() {
    	var mm_yy = $('#datepicker-mm-yy').val();
    	var contractor = $('#contractor').val();
    	console.log('month_year2'+ mm_yy);
    	var surat_izin_id = 2;
    	var month_year = mm_yy.split("-"); 
    	console.log('month_year'+ month_year);
    	if(mm_yy) {
	        url = "<?php echo site_url();?>report/contractor_all/" + surat_izin_id + '/' + month_year[0] + "-" + month_year[1] + '/'+ contractor;
	        window.location = url; // redirect
    	}
        return false;
	});
} );
</script>
			<!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side strech">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Rekapitulasi Pemakaian <?php echo !empty($contractor_name) ? $contractor_name->contractor_name : "";?>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="#">Tables</a></li>
                        <li class="active">Data tables</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">REKAPITULASI PEMAKAIAN 
	                                    <select name="contractor" id="contractor">
		                                    <option value="">ALL</option>
	                                    <?php foreach($contractor_all as $contractor) { ?>
		                                    <option value="<?php echo $contractor->contractor_id;?>"
		                                    <?php if(!empty($contractor_id)) {
											echo $contractor_id == $contractor->contractor_id ? 'selected':'';
											}?>><?php echo $contractor->contractor_name;?></option>
		                                <?php }?>
	                                    </select>
                                    PERIODE <?php echo date("F", mktime(0, 0, 0, $month, 10)).'-'.$year;?>
                                    </h3>
                                    <div class="col-xs-4">
                                      		<input type="text" size="4" class="form-control" name="mm-yy" class="date_change" required id="datepicker-mm-yy" placeholder="YYYY-MM" >
                                      		<input type="button" value="submit" class="submit">
                                    </div>
                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive">
                                <?php 

                                $tri = new MultipleIterator();
                                $item_name = array();
                                $qty_unit = array();
                                foreach($monthly[abs(1)] as $row) {
									array_push($item_name,$row->item_name);
									array_push($qty_unit,$row->qty_unit);
                                }
                                	$tri->attachIterator(new ArrayIterator($item_name), 'item_name');
                                	$tri->attachIterator(new ArrayIterator($qty_unit), 'qty_unit');
		                                        $j=1;
		                                        $s=0;
		                                        $same_i = "";
		                                        $stock_awal = array();
		                                        $total_in = array();
		                                        $total_use = array();
		                                        $stock_akhir = array();
		                                        for($i=-2;$i<=0;$i++) {
			                                        foreach($monthly[abs($i)] as $row) {
														$stock_awal[$i][$row->item_id] = $row->stock_awal;
														$total_in[$i][$row->item_id] = $row->total_in;
														$total_use[$i][$row->item_id] = $row->total_use;
														$stock_akhir[$i][$row->item_id] = $row->stock_akhir;
			                                        }
												$tri->attachIterator(new ArrayIterator($stock_awal[$i]), 'stock_awal'.$i);
												$tri->attachIterator(new ArrayIterator($total_in[$i]), 'total_in'.$i);
												$tri->attachIterator(new ArrayIterator($total_use[$i]), 'total_use'.$i);
												$tri->attachIterator(new ArrayIterator($stock_akhir[$i]), 'stock_akhir'.$i);
												}	
													?>
		                                    <table class="table table-bordered table-striped">
		                                        <thead>
		                                            <tr>
		                                                <th rowspan="2">NO</th>
		                                                <th rowspan="2">JENIS BAHAN PELEDAK</th>
		                                                <th rowspan="2">SATUAN</th>
		                                                <?php for($i=-2;$i<=0;$i++) {?>
		                                        		<th colspan="4"><?php echo getMonthNameAgo($i)?></th>
		                                        		<?php } ?>
		                                            </tr>
		                                            <tr>
		                                            <?php for($i=-2;$i<=0;$i++) {?>
		                                                <th>STOCK AWAL</th>
		                                                <th>PENERIMAAN</th>
		                                                <th>PEMAKAIAN</th>
		                                                <th>STOCK AKHIR</th>
		                                            <?php } ?>
		                                            </tr>
		                                        </thead>
		                                        <tbody>
		                                       
		                                        <?php 
		                                        $j = 1;
		                                        foreach($tri as $row) {
		                                        ?>
		                                            <tr>
		                                                <td><?php echo $j++;?></td>
		                                                <td><?php echo $row[0];?></td>
		                                                <td><?php echo $row[1];?></td>
		                                                <?php for($i=2;$i<=13;$i++) {?>
		                                                <td><?php echo $row[$i];?>	</td>
		                                                <?php } ?>
		                                                <script type="text/javascript">
														/* <![CDATA[ */
														(function(){try{var s,a,i,j,r,c,l,b=document.getElementsByTagName("script");l=b[b.length-1].previousSibling;a=l.getAttribute('data-cfemail');if(a){s='';r=parseInt(a.substr(0,2),16);for(j=2;a.length-j;j+=2){c=parseInt(a.substr(j,2),16)^r;s+=String.fromCharCode(c);}s=document.createTextNode(s);l.parentNode.replaceChild(s,l);}}catch(e){}})();
														/* ]]> */
														</script>
		                                                </td>
													</tr>        
												<?php 
												}
												?>
		                                        </tbody>
		                                    </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
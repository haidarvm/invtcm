<script>
$(document).ready(function() {
    var table = $('#example').DataTable( {
    	scrollY:        "600px",
        scrollX:        true,
        scrollCollapse: true,
        paging:         false,
      
    } );
 
    new $.fn.dataTable.FixedColumns( table, {
        leftColumns: 3
    } );

    $('.submit').click(function() {
    	var mm_yy = $('#datepicker-mm-yy').val();
    	var contractor = $('#contractor').val();
    	console.log('month_year2'+ mm_yy);
    	var surat_izin_id = 2;
    	var month_year = mm_yy.split("-"); 
    	console.log('month_year'+ month_year);
    	if(mm_yy) {
	        url = "<?php echo site_url();?>report/contractor_all/" + surat_izin_id + '/' + month_year[0] + "-" + month_year[1] + '/'+ contractor;
	        window.location = url; // redirect
    	}
        return false;
	});
    
} );
</script>
<style>
/* Ensure that the demo table scrolls */
    div.dataTables_wrapper {
        width: 1200px;
        margin: 0 auto;
    }
    .rekap_out {
       color : blue;
    }
    .rekap_return {
    	color : red;
    }
</style>

			<!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side strech">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Rekapitulasi Pemakaian <?php echo !empty($contractor_name) ? $contractor_name->contractor_name : "";?>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="#">Tables</a></li>
                        <li class="active">Data tables</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">REKAPITULASI PEMAKAIAN
	                                    <select name="contractor" id="contractor">
		                                    <option value="">ALL</option>
	                                    <?php foreach($contractor_all as $contractor) { ?>
		                                    <option value="<?php echo $contractor->contractor_id;?>"
		                                    <?php if(!empty($contractor_id)) {
											echo $contractor_id == $contractor->contractor_id ? 'selected':'';
											}?>><?php echo $contractor->contractor_name;?></option>
		                                <?php }?>
	                                    </select>
                                    PERIODE <?php echo date("F", mktime(0, 0, 0, $month, 10)).'-'.$year;?>
                                    </h3>
                                    <div class="col-xs-4">
                                      		<input type="text" size="4" class="form-control" name="mm-yy" class="date_change" required id="datepicker-mm-yy" placeholder="YYYY-MM" >
                                      		<input type="button" value="submit" class="submit">
                                    </div>
                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive">
		                                    <table id="example" class=" row-border order-column display" cellspacing="0" width="100%">
		                                        <thead>
		                                            <tr>
		                                                <th>No</th>
		                                                <th>Name</th>
		                                                <th>Item Code</th>
		                                                <th>Satuan</th>
		                                                <?php foreach($date_out as $date) {?>
		                                                <th><?php echo $date;?></th>
		                                                <?php } ?>
		                                                <th>Total</th>
		                                            </tr>
		                                        </thead>
		                                        <tbody>
		                                        <?php 
		                                        $i = 1;
		                                        foreach($pivot_use as $row) {?>
		                                            <tr  style="height: 30px;">
		                                                <td><?php echo $i++;?></td>
		                                                <td><?php echo $row->item_name;?></td>
		                                                <td><?php echo $row->item_code;?></td>
		                                                <td><?php echo $row->qty_unit;?></td>
		                                                <?php foreach($date_out as $date_qty) {?>
		                                                <td><?php echo $row->$date_qty;?></td>
		                                                <?php } ?>
		                                                <td><?php echo !empty($row->total_use) ? $row->total_use : "" ?>
		                                                <script type="text/javascript">
														/* <![CDATA[ */
														(function(){try{var s,a,i,j,r,c,l,b=document.getElementsByTagName("script");l=b[b.length-1].previousSibling;a=l.getAttribute('data-cfemail');if(a){s='';r=parseInt(a.substr(0,2),16);for(j=2;a.length-j;j+=2){c=parseInt(a.substr(j,2),16)^r;s+=String.fromCharCode(c);}s=document.createTextNode(s);l.parentNode.replaceChild(s,l);}}catch(e){}})();
														/* ]]> */
														</script>
		                                                </td>
		                                            </tr>
		                                        <?php 
												
												}
												$total_with_out = $i + 1;
												?>
		                                        <tr class="rekap_out">
		                                        	<td><?php echo $total_with_out;?></td>
		                                        	<td>Pengambilan</td>
		                                        	<td></td>
		                                        	<td></td>
		                                        	 <?php 
		                                        	 foreach($date_out as $date_qty) {?>
		                                                <td><?php echo "";?></td>
		                                                <?php } ?>
	                                                <td>
	                                                <script type="text/javascript">
													/* <![CDATA[ */
													(function(){try{var s,a,i,j,r,c,l,b=document.getElementsByTagName("script");l=b[b.length-1].previousSibling;a=l.getAttribute('data-cfemail');if(a){s='';r=parseInt(a.substr(0,2),16);for(j=2;a.length-j;j+=2){c=parseInt(a.substr(j,2),16)^r;s+=String.fromCharCode(c);}s=document.createTextNode(s);l.parentNode.replaceChild(s,l);}}catch(e){}})();
													/* ]]> */
													</script>
	                                                </td>
		                                        </tr>
		                                        <?php 
		                                        foreach($pivot_out as $row) {?>
		                                            <tr class="rekap_out">
		                                                <td><?php echo $total_with_out++;?></td>
		                                                <td><?php echo $row->item_name;?></td>
		                                                <td><?php echo $row->item_code;?></td>
		                                                <td><?php echo $row->qty_unit;?></td>
		                                                <?php foreach($date_out as $date_qty) {?>
		                                                <td><?php echo $row->$date_qty;?></td>
		                                                <?php } ?>
		                                                <td><?php echo !empty($row->Total) ? $row->Total : "" ?>
		                                                <script type="text/javascript">
														/* <![CDATA[ */
														(function(){try{var s,a,i,j,r,c,l,b=document.getElementsByTagName("script");l=b[b.length-1].previousSibling;a=l.getAttribute('data-cfemail');if(a){s='';r=parseInt(a.substr(0,2),16);for(j=2;a.length-j;j+=2){c=parseInt(a.substr(j,2),16)^r;s+=String.fromCharCode(c);}s=document.createTextNode(s);l.parentNode.replaceChild(s,l);}}catch(e){}})();
														/* ]]> */
														</script>
		                                                </td>
		                                            </tr>
		                                        <?php } 
		                                        $total_with_return = $total_with_out + 1;
		                                        ?>
		                                        <tr class="rekap_return">
		                                        	<td><?php echo $total_with_return;?></td>
		                                        	<td>Pengembalian</td>
		                                        	<td></td>
		                                        	<td></td>
		                                        	 <?php 
		                                        	 foreach($date_out as $date_qty) {?>
		                                                <td><?php echo "";?></td>
		                                                <?php } ?>
	                                                <td>
	                                                <script type="text/javascript">
													/* <![CDATA[ */
													(function(){try{var s,a,i,j,r,c,l,b=document.getElementsByTagName("script");l=b[b.length-1].previousSibling;a=l.getAttribute('data-cfemail');if(a){s='';r=parseInt(a.substr(0,2),16);for(j=2;a.length-j;j+=2){c=parseInt(a.substr(j,2),16)^r;s+=String.fromCharCode(c);}s=document.createTextNode(s);l.parentNode.replaceChild(s,l);}}catch(e){}})();
													/* ]]> */
													</script>
	                                                </td>
		                                        </tr>
		                                        <?php 
		                                        foreach($pivot_return as $row) {?>
		                                            <tr class="rekap_return">
		                                                <td><?php echo $total_with_return++;?></td>
		                                                <td><?php echo $row->item_name;?></td>
		                                                <td><?php echo $row->item_code;?></td>
		                                                <td><?php echo $row->qty_unit;?></td>
		                                                <?php foreach($date_out as $date_qty) {?>
		                                                <td><?php echo $row->$date_qty;?></td>
		                                                <?php } ?>
		                                                <td><?php echo !empty($row->Total) ? $row->Total : "" ?></td>
		                                            </tr>
		                                        <?php } ?>         
		                                        </tbody>
		                                    </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
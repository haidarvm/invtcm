            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side strech">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Rincian Harian
                        <small>advanced tables</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="#">Tables</a></li>
                        <li class="active">Data tables</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                	<div class="row">
                		<div class="col-xs-4">
                            <input type="text" size="4" class="form-control" name="mm-yy" class="date_change" required id="datepicker-mm-yy" placeholder="YYYY-MM" >
                            <input type="button" value="submit" class="submit">
                        </div>
                	</div>
                    <div class="row">
                    <!-- Stock IN -->
                        <div class="col-lg-4">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Pengambilan</h3>                                    
                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive">
                                	<div class="overflow">
	                                    <table class="table table-bordered table-striped">
	                                        <thead>
	                                            <tr>
	                                                <th rowspan="3">No</th>
	                                                <th rowspan="3">Tanggal</th>
	                                                <th colspan="3" align="center" valign="middle">Stock Awal</th>
	                                                <th rowspan="3">Keterangan</th>
	                                            </tr>
	                                            <tr>
	                                            	<?php foreach ($all_cat as $cats) { ?>
	                                            	<th><?php echo $cats->category_name?></th>
	                                            	<?php } ?>
	                                            </tr>
	                                            <tr>
	                                            	<th>KG</th>
	                                            	<th>PCS</th>
	                                            	<th>PCS</th>
	                                            </tr>
	                                        </thead>
	                                        <tbody>
	                                        <?php 

	                                        $sto = new MultipleIterator();
	                                        $output = array_slice($all_cat, 2);
	                                        $sto->attachIterator(new ArrayIterator($all_date), 'date');
	                                        
	                                        foreach($pivot_stock_out as $row) {
												$date_qty = array();
												$total_qty = array();
												foreach($all_cat as $cat) {
													$cat_name = $cat->category_name;
													$total_qty[] = $row->$cat_name;
// 													$date_qty[$cat->category_id] = $row->$cat_name;
													foreach($all_date as $dates) {
														$safeDate = safeDateAdd($dates, $cat->category_id);
															$date_qty[$cat->category_id][safeDate($dates)] = $row->$safeDate;
															//echo $row->$safeDate;
													}
													$sto->attachIterator(new ArrayIterator($date_qty[$cat->category_id]), $cat->category_id);
												}
											}
											?>
                                                <tr>
                                                	<td colspan="2">Stock Awal</td>
                                                	<td>-</td>
                                                	<td>-</td>
                                                	<td>-</td>
                                                	<td>-</td>
                                                </tr>
                                                <tr>
                                                	<td colspan="2">Pemasukan</td>
                                                	<td>-</td>
                                                	<td>-</td>
                                                	<td>-</td>
                                                	<td>-</td>
                                                </tr>
                                        		<?php 
                                        		$i = 1;
                                                foreach($sto as $date) {
	                                                //print_r($date[1]);?>
	                                                
	                                                <tr>
		                                                <td><?php echo $i++;?></td>
		                                                <td><?php echo $date[0];?></td>
		                                                <?php 
		                                                $d = 1;
		                                                foreach($all_cat as $cat) { ?>
		                                                <td><?php echo $date[$d++];?></td>
		                                                <?php } ?>
		                                                <td><?php //echo $row->$cat_name?></td>
		                                            </tr>
		                                        <?php } ?>
		                                        <tr>
	                                                	<td colspan="2">Jumlah Pengambilan</td>
	                                                	<?php foreach($total_qty as $total) {?>
	                                                	<td><?php echo $total?></td>
	                                                	<?php } ?>
	                                                	<td>-</td>
	                                            </tr>
	                                        </tbody>
	                                    </table>
                                    </div>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div><!-- Stock IN -->
                        
                        <!-- Stock Return -->
                        <div class="col-lg-4">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Pengembalian</h3>                                    
                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive">
                                	<div class="overflow">
	                                    <table class="table table-bordered table-striped">
	                                        <thead>
	                                            <tr>
	                                                <th rowspan="3">No</th>
	                                                <th rowspan="3">Tanggal</th>
	                                                <th colspan="3" align="center" valign="middle">Stock Awal</th>
	                                                <th rowspan="3">Keterangan</th>
	                                            </tr>
	                                            <tr>
	                                            	<?php foreach ($all_cat as $cats) { ?>
	                                            	<th><?php echo $cats->category_name?></th>
	                                            	<?php } ?>
	                                            </tr>
	                                            <tr>
	                                            	<th>KG</th>
	                                            	<th>PCS</th>
	                                            	<th>PCS</th>
	                                            </tr>
	                                        </thead>
	                                        <tbody>
	                                         <?php 

	                                        $str = new MultipleIterator();
	                                        $output = array_slice($all_cat, 2);
	                                        $str->attachIterator(new ArrayIterator($all_date), 'date');
	                                        
	                                        foreach($pivot_stock_return as $row) {
												$date_qty = array();
												$total_qty = array();
												foreach($all_cat as $cat) {
													$cat_name = $cat->category_name;
													$total_qty[] = $row->$cat_name;
// 													$date_qty[$cat->category_id] = $row->$cat_name;
													foreach($all_date as $dates) {
														$safeDate = safeDateAdd($dates, $cat->category_id);
															$date_qty[$cat->category_id][safeDate($dates)] = $row->$safeDate;
															//echo $row->$safeDate;
													}
													$str->attachIterator(new ArrayIterator($date_qty[$cat->category_id]), $cat->category_id);
												}
											}
											?>
                                                <tr>
                                                	<td colspan="6">Pengemabalian</td>
                                                </tr>
												<tr>
                                                	<td colspan="6">&nbsp;</td>
                                                </tr>
                                        		<?php 
                                        		$i = 1;
                                                foreach($str as $date) {
	                                                //print_r($date[1]);?>
	                                                
	                                                <tr>
		                                                <td><?php echo $i++;?></td>
		                                                <td><?php echo $date[0];?></td>
		                                                <?php 
		                                                $d = 1;
		                                                foreach($all_cat as $cat) { ?>
		                                                <td><?php echo $date[$d++];?></td>
		                                                <?php } ?>
		                                                <td><?php //echo $row->$cat_name?></td>
		                                            </tr>
		                                        <?php } ?>
		                                        <tr>
	                                                	<td colspan="2">Jumlah Pengembalian</td>
	                                                	<?php foreach($total_qty as $total) {?>
	                                                	<td><?php echo $total?></td>
	                                                	<?php } ?>
	                                                	<td>-</td>
	                                            </tr>
	                                        </tbody>
	                                    </table>
	                            	</div>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div><!-- Stock IN -->
                        
                        <!-- Stock Out -->
                        <div class="col-lg-4">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Pemakaian</h3>                                    
                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive">
                                	<div class="overflow">
	                                    <table class="table table-bordered table-striped">
	                                        <thead>
	                                            <tr>
	                                                <th rowspan="3">No</th>
	                                                <th rowspan="3">Tanggal</th>
	                                                <th colspan="3" align="center" valign="middle">Stock Awal</th>
	                                                <th rowspan="3">Keterangan</th>
	                                            </tr>
	                                            <tr>
	                                            	<?php foreach ($all_cat as $cats) { ?>
	                                            	<th><?php echo $cats->category_name?></th>
	                                            	<?php } ?>
	                                            </tr>
	                                            <tr>
	                                            	<th>KG</th>
	                                            	<th>PCS</th>
	                                            	<th>PCS</th>
	                                            </tr>
	                                        </thead>
	                                        <tbody>
	                                         <?php 

	                                        $stu = new MultipleIterator();
	                                        $output = array_slice($all_cat, 2);
	                                        $stu->attachIterator(new ArrayIterator($all_date), 'date');
	                                        
	                                        foreach($pivot_stock_use as $row) {
												$date_qty = array();
												$total_qty = array();
												foreach($all_cat as $cat) {
													$cat_name = $cat->category_name;
													$total_qty[] = $row->$cat_name;
// 													$date_qty[$cat->category_id] = $row->$cat_name;
													foreach($all_date as $dates) {
														$safeDate = safeDateAdd($dates, $cat->category_id);
															$date_qty[$cat->category_id][safeDate($dates)] = $row->$safeDate;
															//echo $row->$safeDate;
													}
													$stu->attachIterator(new ArrayIterator($date_qty[$cat->category_id]), $cat->category_id);
												}
											}
											?>
	                                                <tr>
	                                                	<td colspan="6">Pemakaian</td>
	                                                </tr>
	                                                 <tr>
	                                                	<td colspan="6">&nbsp;</td>
	                                                </tr>
                                        		<?php 
                                        		$i = 1;
                                                foreach($stu as $date) {
	                                                //print_r($date[1]);?>
	                                                
	                                                <tr>
		                                                <td><?php echo $i++;?></td>
		                                                <td><?php echo $date[0];?></td>
		                                                <?php 
		                                                $d = 1;
		                                                foreach($all_cat as $cat) { ?>
		                                                <td><?php echo $date[$d++];?></td>
		                                                <?php } ?>
		                                                <td><?php //echo $row->$cat_name?></td>
		                                            </tr>
		                                        <?php } ?>
		                                        <tr>
	                                                	<td>Jumlah</td>
	                                                	<td></td>
	                                                	<?php foreach($total_qty as $total) {?>
	                                                	<td><?php echo $total?></td>
	                                                	<?php } ?>
	                                                	<td>-</td>
	                                            </tr> 
	                                        </tbody>
	                                    </table>
	                            	</div>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div><!-- Stock IN -->
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
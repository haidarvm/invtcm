            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side strech">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Surat Izin <?php echo $surat_izin->surat_izin;?>
                        <small>advanced tables</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="#">Tables</a></li>
                        <li class="active">Data tables</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title"></h3>                                    
                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive">
                                	<div class="overflow">
	                                    <table id="example1" class="table table-bordered table-striped">
	                                        <thead>
	                                            <tr>
	                                                <th>No</th>
	                                                <th>Name</th>
	                                                <th>Item Code</th>
	                                                <th>Satuan</th>
	                                                <?php foreach($date_in as $date) {?>
	                                                <th><?php echo $date;?></th>
	                                                <?php } ?>
	                                                <th>Total</th>
	                                            </tr>
	                                        </thead>
	                                        <tbody>
	                                        <?php 
	                                        $i = 1;
	                                        foreach($pivot as $row) {?>
	                                            <tr>
	                                                <td><?php echo $i++;?></td>
	                                                <td><?php echo $row->item_name;?></td>
	                                                <td><?php echo $row->item_code;?></td>
	                                                <td><?php echo $row->qty_unit;?></td>
	                                                <?php foreach($date_in as $date_qty) {?>
	                                                <td><?php echo $row->$date_qty;?></td>
	                                                <?php } ?>
	                                                <td><?php echo !empty($row->Total) ? $row->Total : "" ?></td>
	                                            </tr>
	                                        <?php } ?>    
	                                        </tbody>
	                                    </table>
                                    </div>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->